import { browser, by, element } from 'protractor';

export class EditProfilePage {


    navigateTo() {
        return browser.get('/edit-profile');
    }

    getTitleText() {
        return element(by.css('h1')).getText();
    }

    getSignInButton() {
        return element(by.name('edit'));
    }

    getNameInputField() {
        return element(by.name("name"));
    }

    getSurnameInputField() {
        return element(by.name("surname"));
    }

    getOldPasswordnputField() {
        return element(by.name("oldPassword"));
    }

    getPasswordInputField() {
        return element(by.name("password"));
    }

    getConfirmPasswordInputField() {
        return element(by.name("confirmPassword"));
    }

    getFileInputField() {
        return element(by.name("file"));
    }

    getTitleOffSweetAlertModal() {
        return element(by.className('swal2-title')).getText();
    }

    getMessageOfSweetAlertModal() {
        return element(by.className('swal2-html-container')).getText();
    }

    getOkButtonOnSweetAlertModal() {
        return element(by.className('swal2-confirm swal2-styled'));
    }

    setName(name: string) {
        const elem = this.getNameInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(name);
        });
    }

    setSurname(surname: string) {
        const elem = this.getSurnameInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(surname);
        });
    }

    setOldPassword(oldPassword: string) {
        const elem = this.getOldPasswordnputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(oldPassword);
        });
    }

    setPassword(password: string) {
        const elem = this.getPasswordInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(password);
        });
    }

    setConfirmPassword(confirmPassword: string) {
        const elem = this.getConfirmPasswordInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(confirmPassword);
        });
    }

    setFile(file: any) {
        const elem = this.getFileInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(file);
        });
    }
}
