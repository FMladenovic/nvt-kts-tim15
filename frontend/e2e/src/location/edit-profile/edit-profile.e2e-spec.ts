import { browser } from 'protractor';
import { EditProfilePage } from './edit-profile-po/edit-profile.po';
import { SignInPage } from '../../sign-in/sign-in-po/sign-in.po';

describe('Edit profile end to end tests', () => {
    let page: EditProfilePage;
    let signInPage: SignInPage;


    beforeEach(() => {
        page = new EditProfilePage();
        signInPage = new SignInPage();
    });

    it('should login as admin to have permission', () => {
        signInPage.navigateTo();
        signInPage.setUsername("user");
        signInPage.setPassword("user");
        signInPage.getSignInButton().click();

        expect(browser.getCurrentUrl()).toBe("http://localhost:4200/");
    });

    it('should display edit profile page', () => {
        page.navigateTo();
        expect(page.getTitleText()).toEqual('Edit your profile');
    });

    it('should show exception because old password is not provided', () => {
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Oops...");
        expect(page.getMessageOfSweetAlertModal()).toBe("Old password must be provided!");
        page.getOkButtonOnSweetAlertModal().click();
    });

    it('should show exception because new password is equal to old password', () => {
        page.setOldPassword("user");
        page.setPassword("user");
        page.setConfirmPassword("user")
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Oops...");
        expect(page.getMessageOfSweetAlertModal()).toBe("New password must be different than old one!");

        page.getOkButtonOnSweetAlertModal().click();
    });

    it('should show exception because password and confirm password are not the same', () => {
        page.setOldPassword("user");
        page.setPassword("password");
        page.setConfirmPassword("different pass")
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Oops...");
        expect(page.getMessageOfSweetAlertModal()).toBe("Confirm password must be equal to new password!");

        page.getOkButtonOnSweetAlertModal().click();
    });

    it('should edit profile and go to view profile', () => {

        page.setName("name");
        page.setSurname("surname");
        page.setOldPassword("user");
        page.setPassword("password");
        page.setConfirmPassword("password");
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Good job!");
        page.getOkButtonOnSweetAlertModal().click();
        expect(page.getMessageOfSweetAlertModal()).toBe("You have edited your profile!");
        expect(browser.getCurrentUrl()).toBe("http://localhost:4200/profile");
    });
});