import { browser, by, element } from 'protractor';
import { LocationPage } from './location.po';

export class LocationDetailsTab {
    public name: string;

    getTabDetailsByName(name: string) {
        return element(by.id(name));
    }

    getSaveButton() {
        return this.getTabDetailsByName(this.name).element(by.css('input[type="submit"]'));
    }

    getNameInput() {
        return this.getTabDetailsByName(this.name).element(by.id('name'));
    }

    getConfigurationSelect() {
        return this.getTabDetailsByName(this.name).element(by.css('select'));
    }

    getActiveInput() {
        return this.getTabDetailsByName(this.name).element(by.css('input[type="checkbox"]'));
    }

    selectConfiguration(optionNum: number) {
        this.getTabDetailsByName(this.name).all(by.tagName('option'))
        .then( options => options[optionNum].click());
    }

    inputName(name: string) {
        const elem = this.getNameInput();
        elem.clear();
        elem.sendKeys(name);
        this.name = name;
    }

}
