import { browser, by, element } from 'protractor';

export class LocationPage {

    navigateTo() {
        return browser.get('/location');
    }

    getLocationLink() {
        return element(by.name("locations"));
    }

    getTitleText() {
        return element(by.css('h1')).getText();
    }

    getNewLocationButton() {
        return element(by.cssContainingText('button', 'New Location'));
    }

    countTabs() {
        const list =  element(by.css('body')).all(by.tagName('p-accordiontab'));
        return list.count();
    }

    getTabByName(name: string) {
        return element(by.cssContainingText('p-accordionTab', name));
    }

    getSweetAlertModal() {
        return element(by.className('swal2-popup'));
    }

    okOnSweetAlertModal() {
        return this.getSweetAlertModal().element(by.cssContainingText('button', 'OK'));
    }

    getSweetAlertModalTitle() {
        return this.getSweetAlertModal().element(by.css('h2')).getText();
    }
}
