import { browser, by, element } from 'protractor';

export class LocationAddModal {



    getAddLocationModal() {
        return element(by.className('modal'));
    }

    getTitleText() {
        return this.getAddLocationModal().element(by.css('h4')).getText();
    }

    getSaveButton() {
        return this.getAddLocationModal().element(by.css('input[type="submit"]'));
    }

    getNameInput() {
        return this.getAddLocationModal().element(by.id('name'));
    }

    getConfigurationSelect() {
        return this.getAddLocationModal().element(by.id('configuration'));
    }

    getActiveInput() {
        return this.getAddLocationModal().element(by.id('active'));
    }

    selectConfiguration(optionNum: number) {
        this.getAddLocationModal().all(by.tagName('option'))
        .then( options => options[optionNum].click());
    }

    inputName(name: string) {
        this.getNameInput().sendKeys(name);
    }

}
