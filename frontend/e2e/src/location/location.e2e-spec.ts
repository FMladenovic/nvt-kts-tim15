import { LocationPage } from './location-po/location.po';
import { browser, logging, element } from 'protractor';
import { LocationAddModal } from './location-po/location-add.po';
import { LocationDetailsTab } from './location-po/location-details.po';
import { SignInPage } from '../sign-in/sign-in-po/sign-in.po';

describe('Location end to end tests', () => {
  let page: LocationPage;
  let modal: LocationAddModal;
  let details: LocationDetailsTab;
  let signInPage: SignInPage;

  beforeEach(() => {
    page = new LocationPage();
    modal = new LocationAddModal();
    details = new LocationDetailsTab();
    signInPage = new SignInPage();
  });

  it('should login as admin to have permission', () => {
    signInPage.navigateTo();
    signInPage.setUsername("admin");
    signInPage.setPassword("admin");
    signInPage.getSignInButton().click();

    expect(browser.getCurrentUrl()).toBe("http://localhost:4200/");
  });

  it('should display location page', () => {
    page.getLocationLink().click();
    expect(page.getTitleText()).toEqual('Locations');
  });

  it('should open modal with `New location` title on `New Location` button click', () => {
    page.getNewLocationButton().click();
    expect(modal.getTitleText()).toEqual('New Location');
  });

  it('should save new location on save click', () => {

    modal.inputName('Blabla');
    modal.selectConfiguration(0);
    modal.getSaveButton().click();

    expect(page.getSweetAlertModalTitle()).toBe('Good job!');
    page.okOnSweetAlertModal().click();
    expect(page.getTabByName('Blabla')).toBeTruthy();
  });

  it('should show exception because of collision', () => {
    page.getNewLocationButton().click();

    modal.inputName('Blabla');
    modal.selectConfiguration(0);
    modal.getSaveButton().click();

    expect(page.getSweetAlertModalTitle()).toBe('Oops...');
    page.okOnSweetAlertModal().click();
  });

  it('should show exception because location doesn`t contain name', () => {
    page.getNewLocationButton().click();

    modal.inputName('');
    modal.selectConfiguration(0);
    modal.getSaveButton().click();

    expect(page.getSweetAlertModalTitle()).toBe('Oops...');
    page.okOnSweetAlertModal().click();
  });

  it('should show exception because location doesn`t contain configuration', () => {
    page.getNewLocationButton().click();

    modal.inputName('Some name');
    modal.getSaveButton().click();

    expect(page.getSweetAlertModalTitle()).toBe('Oops...');
    page.okOnSweetAlertModal().click();
  });

  it('should show exception because location doesn`t contain configuration', () => {
    page.getNewLocationButton().click();

    modal.inputName('Some name');
    modal.getSaveButton().click();

    expect(page.getSweetAlertModalTitle()).toBe('Oops...');
    page.okOnSweetAlertModal().click();
  });

  it('should save new location an viewer should show two tabs', () => {
    page.getNewLocationButton().click();

    modal.inputName('Some name');
    modal.selectConfiguration(0);
    modal.getSaveButton().click();
    expect(page.getSweetAlertModalTitle()).toBe('Good job!');
    page.okOnSweetAlertModal().click();
    expect(page.countTabs()).toBe(2);
  });

  it('should change title and configuration of `Blabla` location', () => {
    const name = 'Blabla';
    const changeName = 'Something else';
    page.getTabByName(name).click();
    details.name = name;
    browser.sleep(500); // to be done with animation
    details.inputName(changeName);
    details.selectConfiguration(0);
    details.getSaveButton().click();
    expect(page.getSweetAlertModalTitle()).toBe('Good job!');
    page.okOnSweetAlertModal().click();
    expect(page.getTabByName(changeName)).toBeTruthy();
    page.getTabByName(changeName).click();
  });

  it('should show exception because of collision', () => {
    const name = 'Something else';
    const changeName = 'Some name';
    page.getTabByName(name).click();
    details.name = name;
    browser.sleep(500); // to be done with animation
    details.inputName(changeName);
    details.getSaveButton().click();
    expect(page.getSweetAlertModalTitle()).toBe('Oops...');
    page.okOnSweetAlertModal().click();
  });

});
