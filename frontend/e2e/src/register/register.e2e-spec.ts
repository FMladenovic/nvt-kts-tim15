import { browser, logging, element } from 'protractor';
import { RegisterPage } from './register-po/register.po';

describe('Register end to end tests', () => {
    let page: RegisterPage;

    beforeEach(() => {
        page = new RegisterPage();
    });

    it('should display register page', () => {
        page.navigateTo();
        expect(page.getTitleText()).toEqual('Register');
    });

    it('should show exception because fields are invalid', () => {
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Oops...");
        expect(page.getMessageOfSweetAlertModal()).toBe("All fields are mandatory! Please insert data.");
        page.getOkButtonOnSweetAlertModal().click();
    });

    it('should show exception when enter only one value', () => {
        page.setName("name");
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Oops...");
        expect(page.getMessageOfSweetAlertModal()).toBe("All fields are mandatory! Please insert data.");
        page.getOkButtonOnSweetAlertModal().click();
    });

    it('should show exception because passwords are not the same', () => {

        page.setName("name");
        page.setSurname("surname");
        page.setUsername("username");
        page.setEmail("email");
        page.setPassword("password");
        page.setConfirmPassword("different pass")
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Oops...");
        expect(page.getMessageOfSweetAlertModal()).toBe("Confirm password must be equal to password!");

        page.getOkButtonOnSweetAlertModal().click();
    });

    it('should return exception because user with that username already exists', () => {

        page.setName("name");
        page.setSurname("surname");
        page.setUsername("admin");
        page.setEmail("example@examle1.sss");
        page.setPassword("password");
        page.setConfirmPassword("password")
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Oops...");
        page.getOkButtonOnSweetAlertModal().click();
    });

    it('should return exception because user with that email already exists', () => {

        page.setName("name");
        page.setSurname("surname");
        page.setUsername("new username");
        page.setEmail("admin@ex.ex");
        page.setPassword("password");
        page.setConfirmPassword("password")
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Oops...");
        page.getOkButtonOnSweetAlertModal().click();
    });

    it('should return exception because email is not formed well', () => {

        page.setName("name");
        page.setSurname("surname");
        page.setUsername("username email");
        page.setEmail("email");
        page.setPassword("password");
        page.setConfirmPassword("password")
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Oops...");
        page.getOkButtonOnSweetAlertModal().click();
    });

    it('should register user', () => {

        page.setName("name");
        page.setSurname("surname");
        page.setUsername("username");
        page.setEmail("example@examle.sss");
        page.setPassword("password");
        page.setConfirmPassword("password");
        // page.setFile("path");
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Good job!");
        expect(page.getMessageOfSweetAlertModal()).toBe("You have created account. Please check your email to verify account!");

        page.getOkButtonOnSweetAlertModal().click();
        expect(browser.getCurrentUrl()).toBe("http://localhost:4200/sign-in");
    });

});