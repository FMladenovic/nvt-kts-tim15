import { browser, by, element } from 'protractor';

export class RegisterPage {


    navigateTo() {
        return browser.get('/register');
    }

    getTitleText() {
        return element(by.css('h1')).getText();
    }

    getSignInButton() {
        return element(by.name('register'));
    }

    getNameInputField() {
        return element(by.name("name"));
    }

    getSurnameInputField() {
        return element(by.name("surname"));
    }

    getUsernameInputField() {
        return element(by.name("username"));
    }

    getEmailInputField() {
        return element(by.name("email"));
    }

    getPasswordInputField() {
        return element(by.name("password"));
    }

    getConfirmPasswordInputField() {
        return element(by.name("confirmPassword"));
    }

    getFileInputField() {
        return element(by.css('input[type="file"]'));
    }

    getTitleOffSweetAlertModal() {
        return element(by.className('swal2-title')).getText();
    }

    getMessageOfSweetAlertModal() {
        return element(by.className('swal2-html-container')).getText();
    }

    getOkButtonOnSweetAlertModal() {
        return element(by.className('swal2-confirm swal2-styled'));
    }

    setName(name: string) {
        const elem = this.getNameInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(name);
        });
    }

    setSurname(surname: string) {
        const elem = this.getSurnameInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(surname);
        });
    }

    setUsername(username: string) {
        const elem = this.getUsernameInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(username);
        });
    }

    setEmail(email: string) {
        const elem = this.getEmailInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(email);
        });
    }


    setPassword(password: string) {
        const elem = this.getPasswordInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(password);
        });
    }

    setConfirmPassword(confirmPassword: string) {
        const elem = this.getConfirmPasswordInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(confirmPassword);
        });
    }

    setFile(file: string) {
        const elem = this.getFileInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(file);
        });
    }
}
