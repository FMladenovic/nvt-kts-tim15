
import { browser } from 'protractor';
import { SignInPage } from './sign-in-po/sign-in.po';

describe('Sign in end to end tests', () => {
    let page: SignInPage;

    beforeEach(() => {
        page = new SignInPage();
    });

    it('should display sign in page', () => {
        page.navigateTo();
        expect(page.getTitleText()).toEqual('Sign in');
    });

    it('should show exception because password is invalid', () => {
        page.setUsername("username");
        page.setPassword("");
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Oops...");
        expect(page.getMessageOfSweetAlertModal()).toBe("Password must be provided!");
        page.getOkButtonOnSweetAlertModal().click();

    });

    it('should show exception because username is invalid', () => {
        page.navigateTo();
        page.setUsername("");
        page.setPassword("password");
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Oops...");
        expect(page.getMessageOfSweetAlertModal()).toBe("Username must be provided!");

        page.getOkButtonOnSweetAlertModal().click();
    });

    it('should show exception because there is no user with provided credentials', () => {
        page.setUsername("wrong username");
        page.setPassword("wrong password");
        page.getSignInButton().click();

        expect(page.getTitleOffSweetAlertModal()).toBe("Oops...");
        expect(page.getMessageOfSweetAlertModal()).toBe("Wrong username and/or password!");
        page.getOkButtonOnSweetAlertModal().click();
    });

    it('should sign in user', () => {
        // browser.refresh();
        page.setUsername("admin");
        page.setPassword("admin");
        page.getSignInButton().click();

        expect(browser.getCurrentUrl()).toBe("http://localhost:4200/");
    });
});