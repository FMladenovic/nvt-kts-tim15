import { browser, by, element } from 'protractor';

export class SignInPage {


    navigateTo() {
        return browser.get('/sign-in');
    }

    getTitleText() {
        return element(by.css('h1')).getText();
    }

    getSignInButton() {
        return element(by.name('signIn'));
    }

    getUsernameInputField() {
        return element(by.name("username"));
    }

    getPasswordInputField() {
        return element(by.name("password"));
    }

    getTitleOffSweetAlertModal() {
        return element(by.className('swal2-title')).getText();
    }

    getMessageOfSweetAlertModal() {
        return element(by.className('swal2-html-container')).getText();
    }

    getOkButtonOnSweetAlertModal() {
        return element(by.className('swal2-confirm swal2-styled'));
    }

    setUsername(username: string) {
        const elem = this.getUsernameInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(username);
        });
    }

    setPassword(password: string) {
        const elem = this.getPasswordInputField();
        elem.click();
        elem.clear().then(function () {
            elem.sendKeys(password);
        });
    }
}
