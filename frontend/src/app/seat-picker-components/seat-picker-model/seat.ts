

export class Seat {
    name: string;
    status: string;

    price: number;
    color: string;

    ticketId: string;

    constructor(name: string, ticketId: string) {
        this.name = name;
        this.status = !(ticketId === undefined) ? 'available' : 'booked';
        this.ticketId = ticketId;
    }
}
