export class Category {
    constructor(
        public name: string,
        public price: number,
        public color: string,
        public seats: string[],
        public parterCapacities: number[]
        ) {}
}
