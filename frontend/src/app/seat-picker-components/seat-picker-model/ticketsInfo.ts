import { Categories } from './categories';

export class TicketsInfo {
    constructor(
        public categories: Categories,
        public ticketsIds: Map<string, string>,
        public  availableParterTicket: number[],
        public ticketsLimit: number
    ) {}
}
