import { Category } from './category';

export class Categories {
    constructor(
        public id: string,
        public layout: string[],
        public parterCapacities: number[],
        public categories: Category[]
        ) {
    }
}
