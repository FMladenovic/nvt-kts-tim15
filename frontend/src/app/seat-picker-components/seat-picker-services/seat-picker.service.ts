import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { TicketsInfo } from '../seat-picker-model/ticketsInfo';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Seat } from '../seat-picker-model/seat';
import { Reservation } from '../seat-picker-model/reservation';


@Injectable({
  providedIn: 'root'
})
export class SeatPickerService {

  public id;
  public term;
  public ticketsInfo: TicketsInfo;

  private _configuration = new BehaviorSubject<Map<number, Seat[]>>(new Map());
  private configurationHolder: Map<number, Seat[]> = new Map();
  configuration = this._configuration.asObservable();

  private _ticketsIds = new BehaviorSubject<Map<string, string>>(null);
  private ticketsIdsHolder: Map<string, string> = null;
  ticketsIds = this._ticketsIds.asObservable();

  private _parterCapacities = new BehaviorSubject<Map<string, number>>(new Map());
  private parterCapacitiesHolder: Map<string, number> = new Map();
  parterCapacities = this._parterCapacities.asObservable();

  private _selectedSeats = new BehaviorSubject<Seat[]>([]);
  private selectedSeatsHolder: Seat[] = [];
  selectedSeats = this._selectedSeats.asObservable();

  private _totalPrice = new BehaviorSubject<number>(0);
  private totalPriceHolder = 0;
  totalPrice = this._totalPrice.asObservable();

  private _ticketsLimit = new BehaviorSubject<number>(null);
  private ticketsLimitHolder: number = null;
  ticketsLimit = this._ticketsLimit.asObservable();


  constructor(
    private httpClient: HttpClient,
    public route: ActivatedRoute,
    private router: Router
    ) {

  }

  private isInteger(value) {
    return /^\d+$/.test(value);
  }

  onConfirm() {
    const extractedIds = [];

    const parterTickets = [];
    const ticketIds = [];

    const n = this.ticketsInfo.categories.parterCapacities.length;

    for (let i = 0; i < n; i++) {
      parterTickets[i] = 0;
    }

    this.selectedSeatsHolder.forEach(
      seat => {
        extractedIds.push(seat.ticketId);
      }
    );

    for (let i = 0; i < extractedIds.length; i++) {
      if (!this.isInteger(extractedIds[i])) {
        ticketIds.push(extractedIds[i]);
      } else {
        for (let j = 0; j < parterTickets.length; j++) {
          if (extractedIds[i] == j) {
            parterTickets[j]++;
            break;
          }
        }
      }
    }
    const toReserve = new Reservation(parterTickets, ticketIds);
    this.reserve(toReserve);
  }


  reserve(reservation: Reservation) {
    this.httpClient
    .put<Reservation>('http://localhost:8081/api/tickets/' + this.id + '?term=' + this.term, reservation)
    .toPromise()
    .then(data => {
      Swal.fire(
        'Good job!',
        'You have reserved tickets',
        'success'
      );
      this.router.navigateByUrl('/'); // change if needed
    })
    .catch( error => {
        Swal.fire(
            'Oops...',
            error.message,
            'error'
        );
      }
    );
  }

  onXClick(seat: Seat) {
    if (seat.name.includes('P')) {
      let n = this.parterCapacitiesHolder.get(seat.name);
      this.parterCapacitiesHolder.delete(seat.name);
      n = n + 1;
      this.parterCapacitiesHolder.set(seat.name, n);
      seat.status = 'parter';

      this._parterCapacities.next(this.parterCapacitiesHolder);
    } else {
      seat.status = 'available';
    }

    const price = this.totalPriceHolder - seat.price;
    this.totalPriceHolder = Math.round(price * 100) / 100;
    this._totalPrice.next(this.totalPriceHolder);

    for ( let i = 0; i < this.selectedSeatsHolder.length; i++) {
      if ( this.selectedSeatsHolder[i].name === seat.name) {
        this.selectedSeatsHolder.splice(i, 1);
        this._selectedSeats.next(this.selectedSeatsHolder);
        return;
      }
    }
  }

  onCancel() {
    this.selectedSeatsHolder.forEach((s) => {
      if (s.name.includes('P')) {
        let n = this.parterCapacitiesHolder.get(s.name);
        this.parterCapacitiesHolder.delete(s.name);
        n = n + 1;
        this.parterCapacitiesHolder.set(s.name, n);
        s.status = 'parter';
      } else {
        s.status = 'available';
      }
    });
    this.selectedSeatsHolder = [];
    this.totalPriceHolder = 0;

    this._selectedSeats.next(this.selectedSeatsHolder);
    this._totalPrice.next(this.totalPriceHolder);

  }

  onSeatClick(seat: Seat) {

    if (seat.name.includes('P')) {

      if (this.parterCapacitiesHolder.get(seat.name) === 0 || this.selectedSeatsHolder.length === this.ticketsLimitHolder) {
          return;
      }

      let n = this.parterCapacitiesHolder.get(seat.name);
      this.parterCapacitiesHolder.delete(seat.name);
      n = n - 1;
      this.parterCapacitiesHolder.set(seat.name, n);

      this.selectedSeatsHolder.push(seat);

      const price = this.totalPriceHolder + seat.price;
      this.totalPriceHolder = Math.round(price * 100) / 100;
      seat.status = 'parter';

      this._selectedSeats.next(this.selectedSeatsHolder);
      this._parterCapacities.next(this.parterCapacitiesHolder);
      this._totalPrice.next(this.totalPriceHolder);
    } else {
      if (seat.status === 'available') {
        if (this.selectedSeatsHolder.length === this.ticketsLimitHolder) {
          return;
        }
        seat.status = 'selected';
        const price = this.totalPriceHolder + seat.price;
        this.totalPriceHolder = Math.round(price * 100) / 100;
        this.selectedSeatsHolder.push(seat);

        this._selectedSeats.next(this.selectedSeatsHolder);
        this._totalPrice.next(this.totalPriceHolder);
      } else if (seat.status === 'selected') {
        seat.status = 'available';
        const price = this.totalPriceHolder - seat.price;
        this.totalPriceHolder = Math.round(price * 100) / 100;
        for ( let i = 0; i < this.selectedSeatsHolder.length; i++) {
          if ( this.selectedSeatsHolder[i].name === seat.name) {
            this.selectedSeatsHolder.splice(i, 1);
          }
        }
        this._selectedSeats.next(this.selectedSeatsHolder);
        this._parterCapacities.next(this.parterCapacitiesHolder);
        this._totalPrice.next(this.totalPriceHolder);
      }
    }
  }


  getTicketsInfo() {
    this.httpClient
      .get<TicketsInfo>('http://localhost:8081/api/tickets/' + this.id + '?term=' + this.term)
      .toPromise()
      .then(data => {
          this.ticketsInfo = data;

          this.ticketsIdsHolder = data.ticketsIds;
          this._ticketsIds.next(this.ticketsIdsHolder);

          this.ticketsLimitHolder = data.ticketsLimit;
          this._ticketsLimit.next(this.ticketsLimitHolder);

          this.configurationFromCategories(data);
      })
      .catch( error =>
          Swal.fire(
              'Oops...',
              error.message,
              'error'
          )
      );
  }

  configurationFromCategories(ticketsInfo: TicketsInfo) {
    const categories = ticketsInfo.categories.categories;
    const configuration = new Map<number, Seat[]>();
    const parterCapacities = ticketsInfo.availableParterTicket;

    let aCounter = 0;
    let bCounter = 0;
    let cCounter = 0;
    let dCounter = 0;

    const categoryNumber = categories.length;

    for (let i = 0; i < categoryNumber; i++) {

      const category = categories[i];
      const rows = category.seats.length;

      for (let j = 0; j < rows; j++) {
        const row = [];
        const letters = category.seats[j].split('');
        const columns = letters.length;
        for (let k = 0; k < columns; k++) {
          const letter = letters[k];
          let seat = null;
          let name = '';
          let id = null;

          if (letter === 'a') {
            aCounter++;
            name = 'A' + aCounter;
            id = this.ticketsIdsHolder[name];
            seat = new Seat(name, id);
            seat.price = category.price;
            seat.color = category.color;

          }
          if (letter === 'b') {
            bCounter++;
            name = 'B' + bCounter;
            id = this.ticketsIdsHolder[name];
            seat = new Seat(name, id);
            seat.price = category.price;
            seat.color = category.color;

          }
          if (letter === 'c') {
            cCounter++;
            name = 'C' + cCounter;
            id = this.ticketsIdsHolder[name];
            seat = new Seat(name, id);
            seat.price = category.price;
            seat.color = category.color;
          }
          if (letter === 'd') {
            dCounter++;
            name = 'D' + dCounter;
            id = this.ticketsIdsHolder[name];
            seat = new Seat(name, id);
            seat.price = category.price;
            seat.color = category.color;

          }
          if (letter === 'p') {
            let number = 0;
            let toSave = 0;
            for (let no = 1; no <= categories[i].parterCapacities.length; no++) {
              if (categories[i].parterCapacities[no - 1] != null) {
                toSave = parterCapacities[no - 1];
                number = no;
                categories[i].parterCapacities[no - 1] = null;
                break;
              }
            }
            let name = 'P' + number;
            seat = new Seat(name, (number - 1) + '');
            seat.status = 'parter';

            this.parterCapacitiesHolder.set(name, toSave);
            this._parterCapacities.next(this.parterCapacitiesHolder);

            seat.price = category.price;
            seat.color = category.color;
          }

          if (i === 0) {
            row.push(seat);
          }

          if (i !== 0 && seat != null) {
            if (configuration.get(j)[k] == null) {
              configuration.get(j)[k] = seat;
            }
          }
        }
        if (i === 0) {
          configuration.set(j, row);
        }
      }
    }
    this.configurationHolder = configuration;
    this._configuration.next(this.configurationHolder);
  }

}
