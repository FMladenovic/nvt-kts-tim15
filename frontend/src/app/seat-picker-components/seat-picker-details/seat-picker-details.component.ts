import { Component, OnInit } from '@angular/core';
import { Seat } from '../seat-picker-model/seat';
import { SeatPickerService } from '../seat-picker-services/seat-picker.service';

@Component({
  selector: 'app-seat-picker-details',
  templateUrl: './seat-picker-details.component.html',
  styleUrls: ['./seat-picker-details.component.css']
})
export class SeatPickerDetailsComponent implements OnInit {

  parterCapacities = new Map<string, number>();
  selectedSeats: Seat[] = [];
  totalPrice = 0;
  ticketsLimit: number;

  constructor(
    private seatPickerService: SeatPickerService,
  ) {}

  ngOnInit() {
    this.seatPickerService.parterCapacities.subscribe(
      data => this.parterCapacities = data
    );

    this.seatPickerService.selectedSeats.subscribe(
      data => this.selectedSeats = data
    );
    this.seatPickerService.totalPrice.subscribe(
      data => this.totalPrice = data
    );

    this.seatPickerService.ticketsLimit.subscribe(
      data => this.ticketsLimit = data
    );
  }

  onXClick(event, seat: Seat) {
    this.seatPickerService.onXClick(seat);
  }

}
