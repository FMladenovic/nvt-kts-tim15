import { Component, OnInit } from '@angular/core';
import { SeatPickerService } from '../seat-picker-services/seat-picker.service';

import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-seat-picker',
  templateUrl: './seat-picker.component.html',
  styleUrls: ['./seat-picker.component.css']
})
export class SeatPickerComponent implements OnInit {

  constructor(
    private seatPickerService: SeatPickerService,
    public route: ActivatedRoute
  ) {}

  ngOnInit() {

    this.seatPickerService.term = this.route.snapshot.queryParamMap.get('term');
    this.route.paramMap.subscribe( paramMap => {
      this.seatPickerService.id = paramMap.get('id');
      this.seatPickerService.getTicketsInfo();
    });

  }

  onConfirm() {
    this.seatPickerService.onConfirm();
  }

  onCancel() {
    this.seatPickerService.onCancel();
  }

}
