import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SeatPickerService } from '../seat-picker-services/seat-picker.service';
import { Seat } from '../seat-picker-model/seat';

@Component({
  selector: 'app-seat-picker-view',
  templateUrl: './seat-picker-view.component.html',
  styleUrls: ['./seat-picker-view.component.css']
})
export class SeatPickerViewComponent implements OnInit {


  configuration = new Map();

  constructor(
    private seatPickerService: SeatPickerService,
    public route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.seatPickerService.configuration.subscribe(
      data => this.configuration = data
    );

  }
  onSeatClick(event, seat: Seat) {
    this.seatPickerService.onSeatClick(seat);
  }

}
