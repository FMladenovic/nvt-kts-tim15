import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

import Swal from 'sweetalert2';

import { Categories } from '../categories-model/categories';
import { Category } from '../categories-model/category';
import { Seat } from '../categories-model/seat';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  private _categories = new BehaviorSubject<Categories>(new Categories([], []));
  private categoriesHolder: Categories = new Categories([], []);
  categories = this._categories.asObservable();

  setCategories(categories: Categories) {
    this.categoriesHolder = categories;
    this._categories.next(this.categoriesHolder);
  }

  addCategory(category: Category) {
    this.categoriesHolder.categories.push(category);
    this._categories.next(this.categoriesHolder);
  }


  private _currentCategory = new BehaviorSubject<Category>(null);
  private categoryHolder: Category = null;
  currentCategory = this._currentCategory.asObservable();

  setCurrentCategory(category: Category) {
    this.categoryHolder = category;
    this._currentCategory.next(this.categoryHolder);
  }

  removeCategory(category: Category) {
    const index = this.categoriesHolder.categories.indexOf(category);
    if (index !== -1) {
      this.categoriesHolder.categories.splice(index, 1);
      this._categories.next(this.categoriesHolder);
      this.configurationFromCategories(this.categoriesHolder);
    }
  }

  removeCategories() {
    this.categoriesHolder.categories = [];
    this._categories.next(this.categoriesHolder);
    this.configurationFromCategories(this.categoriesHolder);
  }



  private _configuration = new BehaviorSubject<Map<number, Seat[]>>(new Map<number, Seat[]>());
  private configurationHolder: Map<number, Seat[]> = new Map<number, Seat[]>();
  configuration = this._configuration.asObservable();


  configurationFromCategories(categories: Categories) {

    const configuration = new Map<number, Seat[]>();
    let pCounter = 0;

    const layOut = categories.layout;

    for (let i = 0; i < layOut.length; i++) {
      const letters = layOut[i].split('');
      const row = [];
      for (let j = 0; j < letters.length; j++) {
        const letter = letters[j];
        if (!(letter === '_')) {
          const seat = new Seat(letter.toUpperCase(), i, j);

          categories.categories.forEach( category => {
            if (!(category.seats[i][j] === '_')) {
              seat.category = category;
            }
          });

          if (letter === 'p') {
            pCounter++;
            seat.status = 'parter';
            seat.name = 'P' + pCounter;
          }
          row.push(seat);
        } else {
          row.push( null );
        }
      }
      configuration.set(i, row);
    }
    this.configurationHolder = configuration;
    this._configuration.next( this.configurationHolder);
  }

  private _text = new BehaviorSubject<string>('');
  private textHolder = '';
  text = this._text.asObservable();

  setText(text: string) {
    this.textHolder = text;
    this._text.next(this.textHolder);
  }



  constructor(private httpClient: HttpClient) { }

  validation() {
    const categories = this.categoriesHolder.categories;
    let checkName = true;
    let checkPrice = true;
    let checkColor = true;
    let checkCategories = true;

    if (categories.length === 0) {
      checkCategories = false;
    }

    categories.forEach( category => {
      if (category.name === '' || category.name === null) {
        checkName = false;
        return;
      }
      if (category.price <= 0 ) {
        checkPrice = false;
        return;
      }
      if (category.color === '#ffffff') {
        checkColor = false;
        return;
      }
    });

    if (!checkName) {
      Swal.fire(
        'Oops...',
        'Every category must have name.',
        'error'
      );
    } else if (!checkPrice) {
      Swal.fire(
        'Oops...',
        'Every category must have price.',
        'error'
      );
    } else if (!checkColor) {
      Swal.fire(
        'Oops...',
        'Every category must have color.',
        'error'
      );
    } else if (!checkCategories) {
      Swal.fire(
        'Oops...',
        'You should have at least one category.',
        'error'
      );
    }

    return checkName && checkPrice && checkColor && checkCategories;

  }

  postCategories() {
    if (this.validation()) {
      this.httpClient.post<{}>('http://localhost:8081/api/categories/', this.categoriesHolder, {observe: 'response'})
      .toPromise()
      .then( resp => Swal.fire( // ovo ce se izbaciti kada se komponenta doda u drugu
        'Good job!',
        'You have created categories',
        'success'
      ))
      .catch( resp => { Swal.fire(
          'Oops...',
          resp.message,
          'error'
        );
      });
    }
  }



}
