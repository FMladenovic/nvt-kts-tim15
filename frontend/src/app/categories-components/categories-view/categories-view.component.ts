import { Component, OnInit } from '@angular/core';

import {Seat} from '../categories-model/seat';
import {Category} from '../categories-model/category';
import { CategoriesService } from '../categories-services/categories.service';
import { Categories } from '../categories-model/categories';


@Component({
  selector: 'app-categories-view',
  templateUrl: './categories-view.component.html',
  styleUrls: ['./categories-view.component.css']


})
export class CategoriesViewComponent implements OnInit {

  categories: Categories;
  currentCategory: Category;
  configuration = new Map<number, Seat[]>();

  isSingleClick = true;

  constructor(private categoriesService: CategoriesService) {
  }

  ngOnInit() {
    this.categoriesService.categories.subscribe( data => this.categories = data);
    this.categoriesService.currentCategory.subscribe( data => this.currentCategory = data);
    this.categoriesService.configuration.subscribe( data => this.configuration = data);

    this.categoriesService.configurationFromCategories(this.categories);
  }

  replaceAt(index: number, line: string, char: string) {
    return line.substr(0, index) + char + line.substr(index + char.length);
  }
  method1CallForClick(event, seat: Seat) {
    this.isSingleClick = true;
    setTimeout(() => {
              if (this.isSingleClick) {
                  this.onSeatClick(event, seat, 1);
              }
          }, 200);
  }
  method2CallForDblClick(event, seat: Seat) {
          this.isSingleClick = false;
          this.onSeatClick(event, seat, 10);
  }
  onSeatClick(event, seat: Seat, increment: number) {
    if (this.currentCategory) {
      if ( !(this.currentCategory.color === '#ffffff') ) {

        let validation = true;

        this.categories.categories.forEach( category => {
          if (seat.name.includes('P')) {
            if (!(category.seats[seat.row][seat.column] === '_') && !(category === this.currentCategory)) {
              validation = false;
            }
          } else {
            if (!(category.seats[seat.row][seat.column] === '_')) {
              validation = false;
            }
          }
        });


        if (seat.name.includes('P')) {
          this.currentCategory.seats.forEach(row => {
            if (row.includes('a')) {
              validation = false;
            }
            if (row.includes('b')) {
              validation = false;
            }
            if (row.includes('c')) {
              validation = false;
            }
            if (row.includes('d')) {
              validation = false;
            }
          });
        } else {
          this.currentCategory.seats.forEach(row => {
            if (row.includes('p')) {
             validation = false;
            }
          });
        }

        if (seat.name.includes('P')  && validation) {
          const num = parseInt(seat.name.split('')[1]);
          if (!this.currentCategory.parterCapacities) {
            this.currentCategory.parterCapacities = [];
            this.currentCategory.parterCapacities[num - 1] = 1;
          } else {
            if (!this.currentCategory.parterCapacities[num - 1]) {
              this.currentCategory.parterCapacities[num - 1] = 1;
            } else {
              const testNum = this.currentCategory.parterCapacities[num - 1] + increment ;
              if (testNum > this.categories.parterCapacities[num - 1]) {
                validation = false;
              } else {
                this.currentCategory.parterCapacities[num - 1] = testNum;
              }
            }
          }
        }

        if (validation) {
          const index = this.categories.categories.indexOf(this.currentCategory);
          const newLine = this.replaceAt(seat.column, this.categories.categories[index].seats[seat.row], seat.name.toLocaleLowerCase());
          this.categories.categories[index].seats[seat.row] = newLine;
          this.categoriesService.setCategories(this.categories);
          this.categoriesService.configurationFromCategories(this.categories);
        }
      }
    }
  }




}
