import { Component, OnInit, Input } from '@angular/core';
import { Category } from '../categories-model/category';
import { CategoriesService } from '../categories-services/categories.service';

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.css']
})
export class CategoryDetailsComponent implements OnInit {

  @Input() category: Category;
  parterCapacities: number[];

  constructor(private categoriesService: CategoriesService) {
    this.categoriesService.categories.subscribe(data => this.parterCapacities = data.parterCapacities);
  }

  ngOnInit() {
  }

  onUpdate() {
    this.categoriesService.setCurrentCategory(this.category);
  }

  onDelete() {
    this.categoriesService.removeCategory(this.category);
  }

  write() {
    if (this.category.parterCapacities != null) {
      let text = '';
      for ( let i = 0; i < this.category.parterCapacities.length; i++) {
        if (this.category.parterCapacities[i]) {
          text = text + 'P' + (i + 1) + ':' + ' ' + this.category.parterCapacities[i] + '/' + this.parterCapacities[i] + '    ';
        }
      }
      return text;    } else {
      return '';
    }
  }
}
