import { Component, OnInit, Input } from '@angular/core';

import { CategoriesService } from '../categories-services/categories.service';
import { Configuration } from 'src/app/categories-components/categories-model/configuration';
import { Categories } from '../categories-model/categories';
import { Category } from '../categories-model/category';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
// posto mora uraditi get svih lokacija da bi event odabrao jednu dovoljno je da prosledi konfiguraciju te lokacije kao parametar komponente
  /*@Input()*/ configuration: Configuration;
  categories: Categories;

  currentCategory: Category;


  constructor(private categoriesService: CategoriesService) { }

  ngOnInit() {
    // let cat = new Categories(this.configuration.layout, this.configuration.parterCapacities);
    const parterCapacities = [200, 100, 200];
    const layOut = [
      'a_____' + 'bbbbbbbbbbbbbbbbbbbbb' + '_____c',
      'aa____' + 'bbbbbbbbbbbbbbbbbbbbb' + '____cc',
      'aaa___' + '_bbbbbbbbbbbbbbbbbbb_' + '___ccc',
      'aaaa__' + '__bbbbbbbbbbbbbbbbb__' + '__cccc',
      'aaaaa_' + '___bbbbbbbbbbbbbbb___' + '_ccccc',
      'aaaaaa' + '____bbbbbbbbbbbbb____' + 'cccccc',
      'aaaaaa' + '_____________________' + 'cccccc',
      'aaaaaa' + '____p_____p_____p____' + 'cccccc',
      'aaaaaa' + '_____________________' + 'cccccc',
      'aaaaaa' + '____ddddddddddddd____' + 'cccccc',
      'aaaaa_' + '___ddddddddddddddd___' + '_ccccc',
      'aaaa__' + '__ddddddddddddddddd__' + '__cccc',
      'aaa___' + '_ddddddddddddddddddd_' + '___ccc',
      'aa____' + 'ddddddddddddddddddddd' + '____cc',
      'a_____' + 'ddddddddddddddddddddd' + '_____c',
    ];

    const cat = new Categories(layOut, parterCapacities);

    this.categoriesService.setCategories(cat);
    this.categoriesService.categories.subscribe(data => this.categories = data);
    this.categoriesService.currentCategory.subscribe(data => this.currentCategory = data);
  }

  onCategoryClick(event, category: Category) {
    this.categoriesService.setCurrentCategory(category);
  }

  onAddCategory() {
    const newCategoryLayout = [];
    this.categories.layout.forEach( row => newCategoryLayout.push('_'.repeat(row.length)) );
    this.categoriesService.addCategory(new Category(null, null, newCategoryLayout, null, '#ffffff'));
  }

  onSave() {
    this.categoriesService.postCategories();
  }

  onCancel() {
    this.categoriesService.removeCategories();
  }

}
