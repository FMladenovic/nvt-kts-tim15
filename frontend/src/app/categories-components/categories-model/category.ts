export class Category {

    name: string;
    price: number;
    color: string;
    seats: string[];
    parterCapacities: number[];

    constructor( name: string, price: number, seats: string[], parterCapacities: number[], color: string) {
        this.name = name;
        this.price = price;
        this.seats = seats;
        this.parterCapacities = parterCapacities;
        this.color = color;
    }
}
