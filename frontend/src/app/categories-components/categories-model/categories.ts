import { Category } from './category';

export class Categories {

    public categories: Category[];

    constructor(
        public layout: string[],
        public parterCapacities: number[],
        ) {
            this.categories = [];
    }
}
