export class Configuration {

    constructor(
        public id: number,
        public name: string,
        public layout: string[],
        public parterCapacities: number[]
        ) {
    }
}
