import { Category } from './category';


export class Seat {

    name: string;
    status: string;

    row: number;
    column: number;

    category: Category; // promeni ovo u Category :D i probaj sa tim da odradis isto ovo
                    // sigurno ce bolje raditi

    constructor(name: string, row: number, column: number) {
        this.name = name;
        this.status = 'available';
        this.row = row;
        this.column = column;
    }
}
