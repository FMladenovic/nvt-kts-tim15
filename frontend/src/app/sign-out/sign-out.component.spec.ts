import { ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MessageService } from '../message-service/message.service';
import { SignOutComponent } from './sign-out.component';
import { HomeComponent } from '../home/home.component';

@Injectable()
class MockMessageService extends MessageService {

    constructor() {
        super();
    }
}

describe('SignOutComponent', () => {
    let component: SignOutComponent;
    let fixture: ComponentFixture<SignOutComponent>;
    let service: any;

    const appRoutes: Routes = [
        { path: '', component: HomeComponent }
    ];

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterModule.forRoot(appRoutes)
            ],
            declarations: [
                SignOutComponent, HomeComponent
            ],
            providers: [
                { provide: MessageService, useClass: MockMessageService }
            ]
        });

        fixture = TestBed.createComponent(SignOutComponent);
        component = fixture.componentInstance;
        service = TestBed.get(MessageService);
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });

    it('should inject same service',
        inject([MessageService], (injectService: MessageService) => {
            expect(injectService).toBe(service);
        })
    );

    it('should call sendMessage()', () => {
        const spy = spyOn(service, 'sendMessage').and.callFake(() => console.log('sendMessage'));
        component.ngOnInit();
        expect(spy).toHaveBeenCalledTimes(1);
    });
});
