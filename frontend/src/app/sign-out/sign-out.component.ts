import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from '../message-service/message.service';

@Component({
  selector: 'app-sign-out',
  templateUrl: './sign-out.component.html',
  styleUrls: ['./sign-out.component.css']
})
export class SignOutComponent implements OnInit {

  constructor(private router: Router, private messageService: MessageService) { }

  ngOnInit() {
    localStorage.clear();
    //delete role of logged user
    this.messageService.sendMessage("");
    this.router.navigate(['']);
  }

}
