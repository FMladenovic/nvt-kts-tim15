import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LocationComponent } from './location-components/location/location.component';
import { PageNotFoundComponent } from './error-components/page-not-found/page-not-found.component';
import { CategoriesComponent } from './categories-components/categories/categories.component';
import { SeatPickerComponent } from './seat-picker-components/seat-picker/seat-picker.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms'
import { EditProfileComponent } from './profile/edit-profile/edit-profile.component';
import { ViewProfileComponent } from './profile/view-profile/view-profile.component';
import { SignOutComponent } from './sign-out/sign-out.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'sign-in', component: SignInComponent },
  { path: 'sign-out', component: SignOutComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ViewProfileComponent },
  { path: 'edit-profile', component: EditProfileComponent },
  { path: 'location', component: LocationComponent },
  { path: 'configuration', component: CategoriesComponent },
  { path: 'seat-picker/:id', component: SeatPickerComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), ReactiveFormsModule, FormsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
