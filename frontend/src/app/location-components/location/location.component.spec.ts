import { ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { LocationComponent } from './location.component';
import { LocationAddComponent } from '../location-add/location-add.component';
import { LocationDetailsComponent } from '../location-details/location-details.component';
import { FormsModule } from '@angular/forms';
import { AccordionModule } from 'primeng/accordion';
import { ModalTriggerComponent } from 'src/app/modal/modal-trigger/modal-trigger.component';
import { LocationService } from '../location-services/location.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Configuration } from '../location-model/configuration';

@Injectable()
class MockLocationService extends LocationService {

  constructor(public http: HttpClient) {
    super(http);
  }
}

describe('LocationComponent', () => {
  let component: LocationComponent;
  let fixture: ComponentFixture<LocationComponent>;
  let service: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        AccordionModule,
        HttpClientModule
      ],
      declarations: [
        LocationComponent,
        LocationAddComponent,
        LocationDetailsComponent,
        ModalTriggerComponent
       ],
       providers: [
        {provide: LocationService, useClass: MockLocationService}
      ]
    });



    fixture = TestBed.createComponent(LocationComponent);
    component = fixture.componentInstance;
    service = TestBed.get(LocationService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('The same injected service',
    inject([LocationService], (injectService: LocationService) => {
      expect(injectService).toBe(service);
    })
  );

  it('method for get entities is called', () => {
    const spy = spyOn(service, 'getConfigurationsAndLocations').and.callFake(() => console.log('getConfigurationsAndLocations'));
    component.ngOnInit();
    expect(spy).toHaveBeenCalledTimes(1);
  });

});
