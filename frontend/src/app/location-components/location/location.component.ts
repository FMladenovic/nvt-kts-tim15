import { Component, OnInit } from '@angular/core';

import { LocationService } from '../location-services/location.service';
import { Location } from '../location-model/location';
import { Configuration } from '../location-model/configuration';


@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css'],
})
export class LocationComponent implements OnInit {

  locations: Location[];
  configurations: Configuration[];

  constructor(private locationService: LocationService) {}

  ngOnInit() {
    this.locationService.getConfigurationsAndLocations();

    this.locationService.configurations
    .subscribe(data => this.configurations = data);
    this.locationService.locations
    .subscribe(data => this.locations = data);
  }

}
