import { TestBed } from '@angular/core/testing';
import { LocationService } from './location.service';
import { HttpClientModule } from '@angular/common/http';
import { Location } from '../location-model/location';
import { Configuration } from '../location-model/configuration';


describe('LocationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule],
    providers: [LocationService]
  }));

  it('should be created', () => {
    const service: LocationService = TestBed.get(LocationService);
    expect(service).toBeTruthy();
  });

  it('validation should return false if location name is null or ``', () => {
    const service: LocationService = TestBed.get(LocationService);
    const newLocation = new Location('123', '', true, 1);
    let invalid = service.validate(newLocation);
    expect(invalid).toBe(false);
    newLocation.name = null;
    invalid = service.validate(newLocation);
    expect(invalid).toBe(false);
  });

  it('validation should return true if location name is valid', () => {
    const service: LocationService = TestBed.get(LocationService);
    const newLocation = new Location('123', 'Some name', true, 1);
    const valid = service.validate(newLocation);
    expect(valid).toBe(true);
  });

  it('validation should return true if location contains configurationId', () => {
    const service: LocationService = TestBed.get(LocationService);
    const newLocation = new Location('123', 'Some name', true, 1);
    const valid = service.validate(newLocation);
    expect(valid).toBe(true);
  });

  it('validation should return false if location doesn`t contain configurationId', () => {
    const service: LocationService = TestBed.get(LocationService);
    const newLocation = new Location('123', 'Some name', true, null);
    const invalid = service.validate(newLocation);
    expect(invalid).toBe(false);
  });

  it('set should connect locations and configurations', () => {
    const service: LocationService = TestBed.get(LocationService);
    let locations: Location[] = [];
    const location1 = new Location('123', 'Name1', true, 1);
    const location2 = new Location('124', 'Name2', true, 1);
    const location3 = new Location('125', 'Name3', true, 2);
    const location4 = new Location('126', 'Name4', true, 2);
    locations.push(location1, location2, location3, location4);

    const configurations: Configuration[] = [];
    const config1 = new Configuration(1, 'Config1', [], []);
    const config2 = new Configuration(2, 'Config2', [], []);
    configurations.push(config1, config2);

    locations = service.set(locations, configurations);

    locations.forEach(location => expect(location.configuration).toBeTruthy());
  });

  it('set should connect location and configuration', () => {
    const service: LocationService = TestBed.get(LocationService);
    let location = new Location('123', 'Name1', true, 1);
    const configurations: Configuration[] = [];
    const config1 = new Configuration(1, 'Config1', [], []);
    const config2 = new Configuration(2, 'Config2', [], []);
    configurations.push(config1, config2);

    location = service.setConfigurationToLocation(location, configurations);
    expect(location.configuration).toBeTruthy();
  });

});
