import {Location} from '../location-model/location';
import { Configuration } from '../location-model/configuration';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject } from 'rxjs';
import { Categories } from 'src/app/categories-components/categories-model/categories';
import Swal from 'sweetalert2';

@Injectable()
export class LocationService {

    private _locations = new BehaviorSubject<Location[]>([]);
    private locationsHolder: Location[] = [];
    locations = this._locations.asObservable();

    private _configurations = new BehaviorSubject<Configuration[]>([]);
    private configurationsHolder: Configuration[] = [];
    configurations = this._configurations.asObservable();

    constructor(private httpClient: HttpClient) {
    }


    set(locations: Location[], configurations: Configuration[]) {
        locations.forEach(location => {
            configurations.forEach( configuration => {
                if (location.configurationId === configuration.id) {
                    location.configuration = configuration;
                }
            });
        });
        return locations;
    }

    setConfigurationToLocation(location: Location, configurations: Configuration[]) {
        configurations.forEach( configuration => {
            if (location.configurationId === configuration.id) {
                location.configuration = configuration;
            }
        });
        return location;
    }

    validate(location: Location) {
        const checkName = !(location.name === '' || location.name === null);
        const checkConfigurationId = !(location.configurationId === null);

        if (! checkName) {
            Swal.fire(
                'Oops...',
                'Location name must exist!',
                'error'
            );
        }
        if (! checkConfigurationId) {
            Swal.fire(
                'Oops...',
                'Configuration must be chosen!',
                'error'
            );
        }

        return checkName && checkConfigurationId;
    }

    getLocations() {
        this.httpClient
            .get<Location[]>('http://localhost:8081/api/locations/')
            .toPromise()
            .then(data => {
                const loc = this.set(data, this.configurationsHolder);
                this.locationsHolder = loc;
                this._locations.next(this.locationsHolder);
            })
            .catch( error =>
                Swal.fire(
                    'Oops...',
                    error.message,
                    'error'
                )
            );
    }

    getLocation(id: number) {
        this.httpClient
            .get<Location>('http://localhost:8081/api/locations/' + id)
            .toPromise()
            .then(data => {
                const location = this.setConfigurationToLocation(data, this.configurationsHolder);
                this.locationsHolder.push(location);
                this._locations.next(this.locationsHolder);
            })
            .catch( error =>
                Swal.fire(
                    'Oops...',
                    error.error.message,
                    'error'
                )
            );
    }

    getConfigurationsAndLocations() {
        this.httpClient
            .get<Configuration[]>('http://localhost:8081/api/configurations/')
            .toPromise()
            .then(data => {
                this.configurationsHolder = data;
                this._configurations.next(this.configurationsHolder);
            })
            .then(() => {
                this.getLocations();
            })
            .catch( error =>
                Swal.fire(
                    'Oops...',
                    error.message,
                    'error'
                )
            );
    }

    postLocation(newLocation: Location) {
        if (this.validate(newLocation)) {
            this.httpClient
                .post<{}>('http://localhost:8081/api/locations/', {
                    name: newLocation.name,
                    active: newLocation.active,
                    configurationId: newLocation.configurationId
                }, {observe: 'response'})
                .toPromise()
                .then( response => {
                    this.getLocationByPath(response.headers.get('Location'));
                    Swal.fire(
                        'Good job!',
                        'You have created location',
                        'success'
                    );
                })
                .catch( error => {
                    Swal.fire(
                        'Oops...',
                        error.error.message,
                        'error'
                    );
                });
        }
    }

    getLocationByPath(path: string) {
        this.httpClient
            .get<Location>('http://localhost:8081' + path)
            .toPromise()
            .then(data => {
                const location = this.setConfigurationToLocation(data, this.configurationsHolder);
                this.locationsHolder.push(location);
                this._locations.next(this.locationsHolder);
            })
            .catch( error =>
                Swal.fire(
                    'Oops...',
                    error.error.message,
                    'error'
                )
            );
    }

    put(newLocation: Location) {
        if (this.validate(newLocation)) {
            this.httpClient
                .put<Location>('http://localhost:8081/api/locations/' + newLocation.id, newLocation)
                .toPromise()
                .then( response => {
                    Swal.fire(
                        'Good job!',
                        'You have updated location',
                        'success'
                    );
                })
                .catch( error => {
                    Swal.fire(
                        'Oops...',
                        error.error.message,
                        'error'
                    );
                });
        }
    }




}
