export class Configuration {

    public id: number;
    public name: string;
    public layout: string[];
    public parterCapacities: number[];

    constructor(id: number, name: string, layout: string[], parterCapacities: number[]) {
        this.id = id;
        this.name = name;
        this.layout = layout;
        this.parterCapacities = parterCapacities;
    }
}
