import { Configuration } from './configuration';

export class Location {
    public configuration: Configuration;
    constructor(
        public id: string,
        public name: string,
        public active: boolean,
        public configurationId: number
        ) {}
}
