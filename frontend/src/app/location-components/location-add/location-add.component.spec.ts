import { ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { LocationAddComponent } from './location-add.component';
import { Location } from '../location-model/location';
import { HttpClientModule, HttpClient  } from '@angular/common/http';

import { LocationService } from '../location-services/location.service';
import { FormsModule } from '@angular/forms';
import { Injectable } from '@angular/core';

@Injectable()
class MockLocationService extends LocationService {
  constructor(public http: HttpClient) {
    super(http);
  }
}

describe('LocationAddComponent', () => {
  let component: LocationAddComponent;
  let fixture: ComponentFixture<LocationAddComponent>;
  let service: MockLocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientModule
      ],
      declarations: [
        LocationAddComponent
       ],
       providers: [
        {provide: LocationService, useClass: MockLocationService}
      ]
    });


    fixture = TestBed.createComponent(LocationAddComponent);
    component = fixture.componentInstance;
    service = TestBed.get(LocationService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('the same injected service',
    inject([LocationService], (injectService: LocationService) => {
      expect(injectService).toBe(service);
    })
  );

  it('`onSave()` should call the `postLocation` method from LocationService', () => {
    const newLocation = new Location(null, 'nesto', true, 1);
    const spy = spyOn(service, 'postLocation').and.callFake(() => console.log('post'));
    component.location = newLocation;
    component.onSave();
    expect(spy).toHaveBeenCalledWith(component.location);
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('`onSave()` should trigger `turnOff` event', () => {
    const newLocation = new Location(null, 'nesto', true, 1);
    const spy = spyOn(service, 'postLocation').and.callFake(() => console.log('post'));
    component.location = newLocation;
    let check = true;
    component.turnOff.subscribe(data => check = data);
    component.onSave();
    expect(check).toBe(false);
  });

});
