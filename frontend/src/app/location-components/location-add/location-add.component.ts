import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Configuration } from '../location-model/configuration';
import { Location } from '../location-model/location';

import { LocationService } from '../location-services/location.service';

@Component({
  selector: 'app-location-add',
  templateUrl: './location-add.component.html',
  styleUrls: ['./location-add.component.css']
})
export class LocationAddComponent implements OnInit {

  location: Location = new Location('', '', true, null);
  configurations: Configuration[];

  @Output() turnOff = new EventEmitter<boolean>();

  constructor(
    private locationService: LocationService
    ) {}

  ngOnInit() {
    this.locationService.configurations
    .subscribe(data => this.configurations = data);
  }

  onSave() {
    this.locationService.postLocation(this.location);
    this.turnOff.emit(false);
  }
}
