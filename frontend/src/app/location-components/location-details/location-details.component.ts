import { Component, OnInit, Input } from '@angular/core';
import {Location} from '../location-model/location';
import { Configuration } from '../location-model/configuration';
import { LocationService } from '../location-services/location.service';


@Component({
  selector: 'app-location-details',
  templateUrl: './location-details.component.html',
  styleUrls: ['./location-details.component.css']
})
export class LocationDetailsComponent implements OnInit {

  @Input() location: Location;
  configurations: Configuration[];

  constructor(private locationService: LocationService) {}

  ngOnInit() {
    this.locationService.configurations
    .subscribe(data => this.configurations = data);
  }

  onSave() {
    this.locationService.put(this.location);
  }
}
