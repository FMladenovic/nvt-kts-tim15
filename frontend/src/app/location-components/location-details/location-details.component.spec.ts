import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { LocationDetailsComponent } from './location-details.component';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Location } from '../location-model/location';
import { LocationService } from '../location-services/location.service';

@Injectable()
class MockLocationService extends LocationService {
  constructor(public http: HttpClient) {
    super(http);
  }
}

describe('LocationDetailsComponent', () => {
  let component: LocationDetailsComponent;
  let fixture: ComponentFixture<LocationDetailsComponent>;
  let service: LocationService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientModule
      ],
      declarations: [
        LocationDetailsComponent
       ],
       providers: [
        {provide: LocationService, useClass: MockLocationService}
      ]
    });

    fixture = TestBed.createComponent(LocationDetailsComponent);
    component = fixture.componentInstance;
    service = TestBed.get(LocationService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('the same injected service',
  inject([LocationService], (injectService: LocationService) => {
    expect(injectService).toBe(service);
  })
);

  it('`onSave()` should call the `put` method from LocationService', () => {
    const newLocation = new Location('123', 'nesto', true, 1);
    const spy = spyOn(service, 'put').and.callFake(() => console.log('put'));
    component.location = newLocation;
    component.onSave();
    expect(spy).toHaveBeenCalledWith(component.location);
    expect(spy).toHaveBeenCalledTimes(1);
  });

});
