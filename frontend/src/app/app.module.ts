import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
// import { TooltipModule } from 'ngx-bootstrap';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'primeng/accordion';
import { TooltipModule } from 'primeng/tooltip';
import { ColorPickerModule } from 'primeng/colorpicker';



import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NavBarComponent } from './header/nav-bar/nav-bar.component';
import { LogoComponent } from './header/logo/logo.component';
import { HomeComponent } from './home/home.component';
import { LocationComponent } from './location-components/location/location.component';
import { PageNotFoundComponent } from './error-components/page-not-found/page-not-found.component';
import { LocationDetailsComponent } from './location-components/location-details/location-details.component';
import { ModalComponent } from './modal/modal/modal.component';
import { ModalTriggerComponent } from './modal/modal-trigger/modal-trigger.component';
import { LocationAddComponent } from './location-components/location-add/location-add.component';
import { CategoriesViewComponent } from './categories-components/categories-view/categories-view.component';
import { CategoriesComponent } from './categories-components/categories/categories.component';
import { CategoryDetailsComponent } from './categories-components/category-details/category-details.component';

import { CategoriesService } from './categories-components/categories-services/categories.service';
import { LocationService } from './location-components/location-services/location.service';
import { SeatPickerComponent } from './seat-picker-components/seat-picker/seat-picker.component';
import { SeatPickerViewComponent } from './seat-picker-components/seat-picker-view/seat-picker-view.component';
import { SeatPickerDetailsComponent } from './seat-picker-components/seat-picker-details/seat-picker-details.component';
import { SeatPickerService } from './seat-picker-components/seat-picker-services/seat-picker.service';
import { SignInComponent } from './sign-in/sign-in.component';
import { RegisterComponent } from './register/register.component';
import { EditProfileComponent } from './profile/edit-profile/edit-profile.component';
import { ViewProfileComponent } from './profile/view-profile/view-profile.component';
import { RegisterService } from './register/register-service/register.service';
import { ProfileLogoComponent } from './header/profile-logo/profile-logo.component';
import { TokenInterceptorService } from './sign-in/service/token-interceptor.service';
import { AuthorizationService } from './sign-in/service/authorization.service';
import { SignInService } from './sign-in/service/sign-in.service';
import { SignOutComponent } from './sign-out/sign-out.component';
import { MessageService } from './message-service/message.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavBarComponent,
    LogoComponent,
    HomeComponent,
    LocationComponent,
    PageNotFoundComponent,
    LocationDetailsComponent,
    ModalComponent,
    ModalTriggerComponent,
    LocationAddComponent,
    CategoriesViewComponent,
    CategoriesComponent,
    CategoryDetailsComponent,
    SeatPickerComponent,
    SeatPickerViewComponent,
    SeatPickerDetailsComponent,
    SignInComponent,
    RegisterComponent,
    EditProfileComponent,
    ViewProfileComponent,
    ProfileLogoComponent,
    SignOutComponent
  ],
  imports: [
    AccordionModule,
    ColorPickerModule,
    TooltipModule,
    BrowserAnimationsModule,
    HttpClientModule,

    FormsModule,

    NgbModule,
    BrowserModule,
    AppRoutingModule
  ],
  entryComponents: [
    ModalComponent
  ],
  providers: [
    CategoriesService,
    LocationService,
    SeatPickerService,
    RegisterService,
    AuthorizationService,
    MessageService,
    SignInService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
