import { Component, OnInit, Input} from '@angular/core';

import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {ModalComponent} from '../modal/modal.component';


@Component({
  selector: 'app-modal-trigger',
  templateUrl: './modal-trigger.component.html',
  styleUrls: ['./modal-trigger.component.css']
})
export class ModalTriggerComponent implements OnInit {

  closeResult: string;
  modalOptions: NgbModalOptions;

  @Input() modalTitle = 'My modal';
  @Input() modalType: string;
  @Input() modalProps: {};

  constructor(
    private modalService: NgbModal,
  ) {
    this.modalOptions = {
      backdrop: 'static',
      backdropClass: 'customBackdrop'
    };
  }

  ngOnInit() {
  }

  open() {
    const modalRef = this.modalService.open(ModalComponent);
    modalRef.componentInstance.title = this.modalTitle;
    modalRef.componentInstance.type = this.modalType;
    modalRef.componentInstance.props = this.modalProps;

  }

}
