import { Component } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})
export class AppComponent {
  title = 'frontend';

  constructor(private sanitizer: DomSanitizer) {}

  getVideoTag() {
    return this.sanitizer.bypassSecurityTrustHtml(
    `<video autoplay muted loop >
        <source src="../assets/background/smokeBackground0.mp4" type="video/mp4">
      </video>`
    );
  }

}

