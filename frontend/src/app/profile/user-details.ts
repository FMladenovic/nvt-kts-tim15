export class UserDetails {
    constructor(
        public name: string,
        public surname: string,
        public email: string,
        public username: string,
        public imagePath: string
    ) { }
}
