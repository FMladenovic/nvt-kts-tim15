import { ComponentFixture, TestBed, inject, async } from '@angular/core/testing';

import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule, Routes, Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { EditProfileService } from './edit-profile-service/edit-profile.service';
import { EditProfileComponent } from './edit-profile.component';
import { ViewProfileComponent } from '../view-profile/view-profile.component';


@Injectable()
class MockEditProfileService extends EditProfileService {

    constructor(httpClient: HttpClient, router: Router) {
        super(httpClient, router);
    }
}

describe('EditProfileComponent', () => {
    let component: EditProfileComponent;
    let fixture: ComponentFixture<EditProfileComponent>;
    let service: any;

    const appRoutes: Routes = [
        { path: 'profile', component: ViewProfileComponent }
    ];

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                HttpClientModule,
                RouterModule.forRoot(appRoutes)
            ],
            declarations: [
                ViewProfileComponent,
                EditProfileComponent
            ]
            ,
            providers: [
                { provide: EditProfileService, useClass: MockEditProfileService }
            ],
        })

        fixture = TestBed.createComponent(EditProfileComponent);
        component = fixture.componentInstance;
        service = TestBed.get(EditProfileService);

    });

    // it('should be created', () => {
    //     expect(component).toBeTruthy();
    // });

    // it('should inject same service',
    //     inject([EditProfileService], (injectService: EditProfileService) => {
    //         expect(injectService).toBe(service);
    //     })
    // );

    // it('should call method for update profile', () => {
    // const spy = spyOn(service, 'updateUser').and.callFake(() => console.log('updateUser'));
    // var updateUser: UpdateUser = new UpdateUser("name", "surname", "old pass", "new pass", "new pass");
    // component.updateUser = updateUser;
    // component.update();
    // expect(spy).toHaveBeenCalledTimes(1);
    // });

    // it('should be defined onFileSelected method', () => {
    //     expect(component.onFileSelected).toBeDefined();
    // });

    // it('should be defined getUser method', () => {
    //     expect(component.getUser).toBeDefined();
    // });
});