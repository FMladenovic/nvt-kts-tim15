import { Component, OnInit } from '@angular/core';
import { UpdateUser } from '../update-user';
import { EditProfileService } from './edit-profile-service/edit-profile.service';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
import { UserDetails } from '../user-details';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {

  updateUser: UpdateUser = new UpdateUser("", "", "", "", "");
  file: File = null;
  loggedUser: UserDetails = new UserDetails("", "", "", "", "");

  constructor(private editProfileService: EditProfileService,
    private http: HttpClient) {
    this.getUser();
  }

  ngOnInit() {
  }

  update() {
    const formData = new FormData();
    formData.append('file', this.file);
    formData.append('user', new Blob([JSON.stringify(this.updateUser)], {
      type: "application/json"
    }));

    this.editProfileService.updateUser(formData, this.updateUser);
  }

  onFileSelected(event) {
    this.file = event.target.files[0];
  }

  getUser() {
    var response: any;
    this.http.get("http://localhost:8081/api/user").subscribe(res => {
      response = res;
      this.loggedUser = response;
    }), err => {
      Swal.fire(
        'Oops...',
        err.message,
        'error'
      )
    };
  }

}
