import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class EditProfileService {

  constructor(private http: HttpClient, private router: Router) {
  }

  public updateUser(uploadForm, updateUser) {
    if (this.validatePasswords(updateUser)) {
      this.http.patch('http://localhost:8081/api/user', uploadForm)
        .toPromise().then(res => {
          Swal.fire(
            'Good job!',
            'You have edited your profile!',
            'success'
          );

          this.router.navigate(['profile']);

          return res;
        }).catch(error => {
          Swal.fire(
            'Oops...',
            error.message,
            'error'
          );
        });
    }
  }

  validatePasswords(updateUser: any) {

    console.log(JSON.stringify(updateUser));

    var oldPassword = updateUser.oldPassword;
    const blankOldPassword = (oldPassword === '' || oldPassword === null || oldPassword === undefined);

    if (blankOldPassword) {
      Swal.fire(
        'Oops...',
        'Old password must be provided!',
        'error'
      );

      return false;
    }

    var password = updateUser.password;
    const blankPassword = (password === '' || password === null || password === undefined);

    if (!blankPassword && oldPassword === password) {
      Swal.fire(
        'Oops...',
        'New password must be different than old one!',
        'error'
      );

      return false;
    }

    var confirmPassword = updateUser.confirmPassword;

    if (password != confirmPassword) {
      Swal.fire(
        'Oops...',
        'Confirm password must be equal to new password!',
        'error'
      );

      return false;
    }

    if (password.length < 8) {
      Swal.fire(
        'Oops...',
        'Password must has 8 letters min!',
        'error'
      );

      return false;
    }

    return true;
  }
}
