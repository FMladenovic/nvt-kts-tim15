import { TestBed } from '@angular/core/testing';

import { EditProfileService } from './edit-profile.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { ViewProfileComponent } from '../../view-profile/view-profile.component';

describe('EditProfileService', () => {
    const appRoutes: Routes = [
        { path: 'profile', component: ViewProfileComponent }
    ];

    beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientModule, RouterModule.forRoot(appRoutes)],
        declarations: [ViewProfileComponent],
        providers: [EditProfileService]
    }));

    it('should be created', () => {
        const service: EditProfileService = TestBed.get(EditProfileService);
        expect(service).toBeTruthy();
    });

    it('validatePasswords should return false because old password is not provided', () => {
        const service: EditProfileService = TestBed.get(EditProfileService);
        var updateUser = {
            name: "", surname: "", oldPassword: "",
            password: "valid password", confirmPassword: "valid password"
        };
        var validated = service.validatePasswords(updateUser);

        expect(validated).toBe(false);
    });

    it('validatePasswords should return false because new password is equal to old one', () => {
        const service: EditProfileService = TestBed.get(EditProfileService);
        var updateUser = {
            name: "", surname: "", oldPassword: "password",
            password: "password", confirmPassword: "password"
        };
        var validated = service.validatePasswords(updateUser);

        expect(validated).toBe(false);
    });

    it('validatePasswords should return false because confirm password is not equal new one', () => {
        const service: EditProfileService = TestBed.get(EditProfileService);
        var updateUser = {
            name: "", surname: "", oldPassword: "password",
            password: "new password", confirmPassword: "different password"
        };
        var validated = service.validatePasswords(updateUser);

        expect(validated).toBe(false);
    });

    it('validatePasswords should return true if passwords are valid', () => {
        const service: EditProfileService = TestBed.get(EditProfileService);
        var updateUser = {
            name: "", surname: "", oldPassword: "password",
            password: "new password", confirmPassword: "new password"
        };
        var validated = service.validatePasswords(updateUser);

        expect(validated).toBe(true);
    });

});
