import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { UserDetails } from '../user-details';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.css']
})
export class ViewProfileComponent implements OnInit {
  user: UserDetails = new UserDetails("", "", "", "", "");

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    var response: any;
    this.http.get("http://localhost:8081/api/user").subscribe(res => {
      response = res;
      this.user = response;
    }), err => {
      Swal.fire(
        'Oops...',
        err.message,
        'error'
      )
    };
  }
}
