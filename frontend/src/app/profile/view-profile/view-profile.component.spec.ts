import { ComponentFixture, TestBed, inject, async } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { ViewProfileComponent } from '../view-profile/view-profile.component';

describe('ViewProfileComponent', () => {
    let component: ViewProfileComponent;
    let fixture: ComponentFixture<ViewProfileComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [

                HttpClientModule
            ],
            declarations: [
                ViewProfileComponent
            ]
        })

        fixture = TestBed.createComponent(ViewProfileComponent);
        component = fixture.componentInstance;
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });

    it('should be defined getUser method', () => {
        expect(component.getUser).toBeDefined();
    });
});