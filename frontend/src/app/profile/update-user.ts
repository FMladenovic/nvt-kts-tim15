export class UpdateUser {
    constructor(
        public name: string,
        public surname: string,
        public oldPassword: string,
        public password: string,
        public confirmPassword: string
    ) { }
}