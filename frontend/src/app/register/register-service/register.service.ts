import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient, private router: Router) { }

  public register(uploadForm, user) {
    if (this.validateData(user)) {
      this.http.post("http://localhost:8081/api/user/register", uploadForm)
        .toPromise().then(res => {
          Swal.fire(
            'Good job!',
            'You have created account. Please check your email to verify account!',
            'success'
          );
          this.router.navigate(['/sign-in']);
        }).catch(error => {
          Swal.fire(
            'Oops...',
            "Please enter valid email address.",
            'error'
          );
        });
    }
  }

  validateData(user) {

    var name = user.name === "" || user.name === undefined || user.name === null;
    var surname = user.surname === "" || user.surname === undefined || user.surname === null;
    var username = user.username === "" || user.username === undefined || user.username === null;
    var email = user.email === "" || user.email === undefined || user.email === null;
    var password = user.password === "" || user.password === undefined || user.password === null;
    var confirmPassword = user.confirmPassword === "" || user.confirmPassword === undefined || user.confirmPassword === null;

    if (name || surname || username || email || password || confirmPassword) {
      Swal.fire(
        'Oops...',
        'All fields are mandatory! Please insert data.',
        'error'
      );

      return false;
    }

    //min 8
    if (user.password.length < 8 || user.confirmPassword.length < 8) {
      Swal.fire(
        'Oops...',
        'Password must has min 8 letters!',
        'error'
      );

      return false;
    }

    //matching pass
    if (user.password != user.confirmPassword) {
      Swal.fire(
        'Oops...',
        'Confirm password must be equal to password!',
        'error'
      );

      return false;
    }

    return true;
  }
}
