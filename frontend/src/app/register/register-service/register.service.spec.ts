import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { RegisterService } from './register.service';
import { RouterModule, Routes } from '@angular/router';
import { SignInComponent } from 'src/app/sign-in/sign-in.component';
import { FormsModule } from '@angular/forms';


describe('RegisterService', () => {
    const appRoutes: Routes = [
        { path: 'sign-in', component: SignInComponent }
    ];
    beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientModule, RouterModule.forRoot(appRoutes), FormsModule],
        declarations: [SignInComponent],
        providers: [RegisterService]
    }));

    it('should create register service', () => {
        const service: RegisterService = TestBed.get(RegisterService);
        expect(service).toBeTruthy();
    });

    it('method validateData should return false because username is not provided', () => {
        const service: RegisterService = TestBed.get(RegisterService);
        var user = {
            name: "name", surname: "surname", email: "emal@ex.ex",
            username: "", password: "password", confirmPassword: "password"
        };

        var validated = service.validateData(user);
        expect(validated).toBe(false);
    });

    it('method validateData should return false because passwords are not equal', () => {
        const service: RegisterService = TestBed.get(RegisterService);
        var user = {
            name: "name", surname: "surname", email: "emal@ex.ex",
            username: "username", password: "password", confirmPassword: "different password"
        };

        var validated = service.validateData(user);
        expect(validated).toBe(false);
    });

    it('method validateData should return true', () => {
        const service: RegisterService = TestBed.get(RegisterService);
        var user = {
            name: "name", surname: "surname", email: "emal@ex.ex",
            username: "username", password: "password", confirmPassword: "password"
        };

        var validated = service.validateData(user);
        expect(validated).toBe(true);
    });
});