import { Component, OnInit } from '@angular/core';
import { User } from './register-service/user';
import { RegisterService } from './register-service/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  user: User = new User("", "", "", "", "", "");
  file: File = null;

  constructor(private registerService: RegisterService) {
  }

  ngOnInit() {
  }

  public register() {

    const formData = new FormData();
    formData.append('file', this.file);
    formData.append('user', new Blob([JSON.stringify(this.user)], {
      type: "application/json"
    }));

    this.registerService.register(formData, this.user);
  }

  public onFileSelected(event) {
    this.file = event.target.files[0];
  }
}
