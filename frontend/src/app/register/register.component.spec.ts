import { ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RegisterService } from './register-service/register.service';
import { RegisterComponent } from './register.component';
import { Router, RouterModule, Routes } from '@angular/router';
import { SignInComponent } from '../sign-in/sign-in.component';

@Injectable()
class MockRegisterService extends RegisterService {

    constructor(http: HttpClient, router: Router) {
        super(http, router);
    }
}

describe('RegisterComponent', () => {
    let component: RegisterComponent;
    let fixture: ComponentFixture<RegisterComponent>;
    let service: any;

    const appRoutes: Routes = [
        { path: 'sign-in', component: SignInComponent }
    ];

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                HttpClientModule
                , RouterModule.forRoot(appRoutes)
            ],
            declarations: [
                RegisterComponent,
                SignInComponent
            ],
            providers: [
                { provide: RegisterService, useClass: MockRegisterService }
            ]
        });

        fixture = TestBed.createComponent(RegisterComponent);
        component = fixture.componentInstance;
        service = TestBed.get(RegisterService);
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });

    it('should inject same service',
        inject([RegisterService], (injectService: RegisterService) => {
            expect(injectService).toBe(service);
        })
    );

    it('should call method for register user', () => {
        const spy = spyOn(service, 'register').and.callFake(() => console.log('register'));
        component.register();
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should be defined onFileSelected method', () => {
        expect(component.onFileSelected).toBeDefined();
    });
});
