import { Component, OnInit } from '@angular/core';
import { SignInService } from './service/sign-in.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  constructor(private signInService: SignInService) {
  }

  ngOnInit() {
  }

  signIn(signInForm: any) {
    this.signInService.signIn(signInForm);
  }
}
