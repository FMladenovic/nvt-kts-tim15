import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor() { }

  getJwtFromLocalStorage() {
    var jwt = localStorage.getItem("jwt");
    return jwt;
  }
}
