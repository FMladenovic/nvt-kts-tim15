import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { AuthorizationService } from './authorization.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private authService: AuthorizationService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): import("rxjs").Observable<HttpEvent<any>> {
    var jwt = this.authService.getJwtFromLocalStorage();
    const modifiedReq = req.clone({
      headers: req.headers.set('Authorization', `${jwt}`),
    });

    return next.handle(modifiedReq);
  }
}
