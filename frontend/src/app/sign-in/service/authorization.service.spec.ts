import { TestBed } from "@angular/core/testing";
import { AuthorizationService } from './authorization.service';


describe('AuthorizationService', () => {

    beforeEach(() => TestBed.configureTestingModule({
        providers: [AuthorizationService]
    }));

    it('should create authorization service', () => {
        const service: AuthorizationService = TestBed.get(AuthorizationService);
        expect(service).toBeTruthy();
    });

    it('should return jwt', () => {
        const service: AuthorizationService = TestBed.get(AuthorizationService);
        localStorage.setItem("jwt", "jwt");
        var jwt = service.getJwtFromLocalStorage();
        expect(jwt).toBe("jwt");
    });
});