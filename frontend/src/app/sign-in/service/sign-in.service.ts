import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as jwtDecode from 'jwt-decode';
import Swal from 'sweetalert2';
import { MessageService } from 'src/app/message-service/message.service';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class SignInService {

  jwt: any;
  token: any;
  decoded: any;

  constructor(private httpClient: HttpClient, private router: Router, private messageService: MessageService) { }


  signIn(signInForm: any) {

    if (this.validateCredentials(signInForm)) {
      this.httpClient.post<{}>('http://localhost:8081/api/user/login', {
        username: signInForm.value.username,
        password: signInForm.value.password
      }).toPromise().then(res => {
        this.token = res;
        this.setToken();
      }).catch(error => {
        Swal.fire(
          'Oops...',
          'Wrong username and/or password!',
          'error'
        );
      });
    }
  }

  validateCredentials(signInForm) {

    const username = (signInForm.value.username === '' || signInForm.value.username === null
      || signInForm.value.username === undefined);
    const password = (signInForm.value.password === '' || signInForm.value.password === null
      || signInForm.value.password === undefined);

    if (username) {
      Swal.fire(
        'Oops...',
        'Username must be provided!',
        'error'
      );
    }
    if (password) {
      Swal.fire(
        'Oops...',
        'Password must be provided!',
        'error'
      );
    }

    return !username && !password;
  }

  setToken() {
    this.jwt = this.token.value;
    var jwtWithoutBearer = this.jwt.replace('Bearer ', '');
    this.decoded = jwtDecode(jwtWithoutBearer);

    localStorage.setItem('jwt', this.jwt);

    this.messageService.sendMessage(this.decoded.user.role);
    this.router.navigate(['']);
  }

}
