import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { SignInService } from './sign-in.service';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from 'src/app/home/home.component';


describe('SignInService', () => {
    const appRoutes: Routes = [
        { path: '', component: HomeComponent }
    ];
    beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientModule, RouterModule.forRoot(appRoutes)],
        declarations: [HomeComponent],
        providers: [SignInService]
    }));

    it('should create sign in service', () => {
        const service: SignInService = TestBed.get(SignInService);
        expect(service).toBeTruthy();
    });

    it('credentials validation should return false if username is not provided', () => {
        const service: SignInService = TestBed.get(SignInService);
        var signIn = { value: { username: "", password: "valid password" } };
        var validated = service.validateCredentials(signIn);
        expect(validated).toBe(false);
    });

    it('credentials validation should return false if password is not provided', () => {
        const service: SignInService = TestBed.get(SignInService);
        var signIn = { value: { username: "valid username", password: "" } };
        var validated = service.validateCredentials(signIn);
        expect(validated).toBe(false);
    });

    it('credentials validation should return false if username and password are not provided', () => {
        const service: SignInService = TestBed.get(SignInService);
        var signIn = { value: { username: "", password: "" } };
        var validated = service.validateCredentials(signIn);
        expect(validated).toBe(false);
    });

    it('credentials validation should true when credentials are valid', () => {
        const service: SignInService = TestBed.get(SignInService);
        var signIn = { value: { username: "username", password: "password" } };
        var validated = service.validateCredentials(signIn);
        expect(validated).toBe(true);
    });

    it('should call validateCredentials', () => {
        const service: SignInService = TestBed.get(SignInService);
        var signIn = { value: { username: "username", password: "password" } };
        service.signIn(signIn);
        expect(service.validateCredentials(signIn)).toBe(true);
    });
});