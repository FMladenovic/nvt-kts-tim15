import { ComponentFixture, TestBed, inject, async } from '@angular/core/testing';

import { SignInComponent } from './sign-in.component';
import { Injectable } from '@angular/core';
import { SignInService } from './service/sign-in.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule, Routes, Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from '../home/home.component';
import { NavBarComponent } from '../header/nav-bar/nav-bar.component';
import { LogoComponent } from '../header/logo/logo.component';
import { MessageService } from '../message-service/message.service';


@Injectable()
class MockSignInService extends SignInService {

    constructor(httpClient: HttpClient, router: Router, messageService: MessageService) {
        super(httpClient, router, messageService);
    }
}

describe('SignInComponent', () => {
    let component: SignInComponent;
    let fixture: ComponentFixture<SignInComponent>;
    let service: any;

    const appRoutes: Routes = [
        { path: '', component: HomeComponent }
    ];

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                HttpClientModule,
                RouterModule.forRoot(appRoutes)
            ],
            declarations: [
                SignInComponent,
                HomeComponent,
                NavBarComponent,
                LogoComponent
            ]
            ,
            providers: [
                { provide: SignInService, useClass: MockSignInService }
            ],
        })

        fixture = TestBed.createComponent(SignInComponent);
        component = fixture.componentInstance;
        service = TestBed.get(SignInService);

    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });

    it('should inject same service',
        inject([SignInService], (injectService: SignInService) => {
            expect(injectService).toBe(service);
        })
    );

    it('should call method for sign in', () => {
        const spy = spyOn(service, 'signIn').and.callFake(() => console.log('signIn'));
        var signIn: any;
        component.signIn(signIn);
        expect(spy).toHaveBeenCalledTimes(1);
    });

});