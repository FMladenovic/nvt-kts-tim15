create table user(
    id char(250) not null,
    username varchar(30) unique,
    password varchar(30),
    email varchar(50) unique,
    name varchar(30),
    surname varchar(30),
    image_path varchar(500),
    is_enabled boolean,
    role varchar(20),
    primary key(id)
);

create table confirmation_token(
    id char(100) not null,
    token varchar(100),
    created_date date,
    user_id char(100),
    primary key(id),
    foreign key(user_id) references user(id)
);

create table events(
    id char(100) not null,
    name varchar(100),
    description varchar(100),
    due_date_for_reservation date,
    tickets_limit int,
    location_id char(100),
    categories_id char(100),
    primary key (id)
);

create table event_terms(
    event_id char(100),
    terms date
);

create table locations(
    id char(100) not null,
    name varchar(100) unique,
    configuration_id int,
    active boolean,
    primary key (id)
);

create table ticket(
    id char(100) not null,
    date_and_time date,
    seat_mark varchar(20),
    price double,
    category_name varchar(50),
    bought boolean,
    version long,
    event_id char(100),
    user_id char(100),
    primary key (id),
    foreign key(event_id) references events(id),
    foreign key(user_id) references user(id)
);