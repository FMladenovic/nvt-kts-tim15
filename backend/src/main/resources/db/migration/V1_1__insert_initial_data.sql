insert into user(id, name, surname, username, password, email, role, image_path, is_enabled)
values ('faeec839-9df4-4e58-83f7-048783a7052d', 'admin', 'admin', 'admin', 'admin', 'admin@ex.ex',
'ROLE_ADMIN', '../../../assets/users/admin.png', 1);

insert into user(id, name, surname, username, password, email, role, image_path, is_enabled)
values ('d55134db-a3ea-40ad-9d50-b6c2354a1e81', 'user', 'user', 'user', 'user', 'user@ex.ex',
'ROLE_USER', '../../../assets/users/user.png', 1);