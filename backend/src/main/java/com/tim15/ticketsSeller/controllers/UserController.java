package com.tim15.ticketsSeller.controllers;

import com.tim15.ticketsSeller.DTOImplementations.JwtResponse;
import com.tim15.ticketsSeller.DTOImplementations.NewUser;
import com.tim15.ticketsSeller.DTOImplementations.UpdateUser;
import com.tim15.ticketsSeller.DTOImplementations.UserCredentials;
import com.tim15.ticketsSeller.DTOImplementations.UserDetails;
import com.tim15.ticketsSeller.security.annotations.LoggedUser;
import com.tim15.ticketsSeller.serviceInterfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public JwtResponse login(@RequestBody UserCredentials userCredentials) {
        return new JwtResponse(userService.login(userCredentials));
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public NewUser registerUser(@RequestPart(value = "user") NewUser newUser,
                                @RequestPart(value = "file", required = false) MultipartFile image) {
        return userService.registerUser(newUser, image);
    }

    @GetMapping("/confirmAccount")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void confirmUserAccount(@RequestParam("token") String confirmationToken) {
        userService.confirmUserAccount(confirmationToken);
    }

    @PatchMapping
    @ResponseStatus(HttpStatus.OK)
    @LoggedUser
    public UpdateUser updateUser(@RequestPart(value = "user") UpdateUser updatedUser,
                                 @RequestPart(value = "file", required = false) MultipartFile image,
                                 Authentication authentication) {
        return userService.updateUser(updatedUser, image, authentication);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @LoggedUser
    public UserDetails getLoggedUser(Authentication authentication) {
        return userService.getLoggedUser(authentication);
    }
}
