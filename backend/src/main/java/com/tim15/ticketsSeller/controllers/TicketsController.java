package com.tim15.ticketsSeller.controllers;

import java.util.List;
import java.util.UUID;

import com.tim15.ticketsSeller.security.annotations.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tim15.ticketsSeller.DTOImplementations.BookTickets;
import com.tim15.ticketsSeller.DTOImplementations.CancelTickets;
import com.tim15.ticketsSeller.DTOImplementations.Reservation;
import com.tim15.ticketsSeller.domain.Ticket;
import com.tim15.ticketsSeller.exception.customExceptions.EventUndefinedException;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidTicketOwnerException;
import com.tim15.ticketsSeller.exception.customExceptions.ReservationPeriodExpiredException;
import com.tim15.ticketsSeller.exception.customExceptions.TermsUndefinedInEventException;
import com.tim15.ticketsSeller.exception.customExceptions.TicketPaidException;
import com.tim15.ticketsSeller.exception.customExceptions.TicketsAlreadyGeneratedException;
import com.tim15.ticketsSeller.exception.customExceptions.TicketsLimitNumberException;
import com.tim15.ticketsSeller.exception.customExceptions.TicketsNotAvailableException;
import com.tim15.ticketsSeller.exception.customExceptions.UnexistingTicketException;
import com.tim15.ticketsSeller.serviceImplementations.TicketService;

@RestController
@RequestMapping("/api/tickets")
@User
public class TicketsController {
	
	@Autowired
	TicketService ticketService;
	
// Ne ovako
//	@RequestMapping(
//			value = "/generateAll",
//			method = RequestMethod.POST,
//			produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<?> generateTicketsForEvent(@RequestBody EventForTickets eventDTO) {
//		try {
//			
//			ticketService.generateTicketsForEvent(eventDTO.getId());
//			
//		}catch(EventUndefinedException ex) {
//			
//			return new ResponseEntity<String>("Event undefined.", HttpStatus.NOT_FOUND);
//			
//		}catch(TicketsAlreadyGeneratedException ex) {
//			
//			return new ResponseEntity<String>("Tickets already exist.", HttpStatus.BAD_REQUEST);
//			
//		}
//		return new ResponseEntity<String>("Tickets successfully generated.", HttpStatus.OK);		
//	}
	
	//http://localhost:8081/api/tickets/69ae1cb4-4b1c-4487-8cb0-ee7f5ae68581 Request koji generise neke tickete
	@RequestMapping(
			value = "/{id}",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> generateTickets(@PathVariable("id") UUID eventId) {
		try {
			
			ticketService.generateTicketsForEvent(eventId);
			
		}catch(EventUndefinedException ex) {
			
			return new ResponseEntity<String>("Event undefined.", HttpStatus.NOT_FOUND); //zbog ovoga smo pravili RestExceptionHandler
																						 //da to ne bismo radili ovde...
		}catch(TicketsAlreadyGeneratedException ex) {
			
			return new ResponseEntity<String>("Tickets already exist.", HttpStatus.BAD_REQUEST);
			
		}
		return new ResponseEntity<String>("Tickets successfully generated.", HttpStatus.OK);		
	}
	
	@RequestMapping(
			value = "/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getTickets(@PathVariable("id") UUID eventId, int term) {
		return new ResponseEntity<>(ticketService.availableTicketsDTO(eventId, term), HttpStatus.OK);
	}
	
	//Ispravi ovo....
	@RequestMapping(
			value = "/book",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> bookTickets(@RequestBody BookTickets bookTicketsDTO){
		
		List<Ticket> reserved;
		
		try {
			
			reserved = ticketService.bookTickets(bookTicketsDTO);
			
		}catch(EventUndefinedException ex) {
			
			return new ResponseEntity<String>("Event undefined.", HttpStatus.NOT_FOUND);
			
		}catch(TermsUndefinedInEventException ex) {
			
			return new ResponseEntity<String>("Requsted terms don't exist for this event.", HttpStatus.NOT_FOUND);
			
		}catch(ReservationPeriodExpiredException ex) {
			
			return new ResponseEntity<String>("Reservation period expired.", HttpStatus.BAD_REQUEST);
			
		}catch(TicketsLimitNumberException ex) {
			
			return new ResponseEntity<String>("Can not reserve more tickets than limit number.", HttpStatus.BAD_REQUEST);
			
		}catch(TicketsNotAvailableException ex) {
			
			return new ResponseEntity<String>("Some tickets aren't available.", HttpStatus.BAD_REQUEST);
			
		}
		
		return new ResponseEntity<List<Ticket>>(reserved, HttpStatus.OK);
		
	}
	
	@RequestMapping(
			value = "/cancel",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> cancelTicket(@RequestBody CancelTickets cancelTicketDTO){
		
		Ticket toCancel;
		
		try {
			
			toCancel = ticketService.cancelTicket(cancelTicketDTO);
			
		}catch(UnexistingTicketException ex) {
			
			return new ResponseEntity<String>("Ticket doesn't exist.", HttpStatus.BAD_REQUEST);
			
		}catch(InvalidTicketOwnerException ex) {
			
			return new ResponseEntity<String>(cancelTicketDTO.getUsername()+" is not a ticket owner.", HttpStatus.BAD_REQUEST);
			
		}catch(TicketPaidException ex) {
			
			return new ResponseEntity<String>(ex.getMessage(), HttpStatus.BAD_REQUEST);
			
		}
		
		return new ResponseEntity<Ticket>(toCancel, HttpStatus.OK);
		
	}
	
	@RequestMapping(
			value = "/{id}",
			method = RequestMethod.PUT
			)
	public ResponseEntity<?> reservation(@PathVariable("id") UUID eventId, int term, @RequestBody Reservation reservationDTO){
		ticketService.reserveTickets(eventId, term, reservationDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
