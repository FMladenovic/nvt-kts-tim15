package com.tim15.ticketsSeller.controllers;

import java.util.NoSuchElementException;
import java.util.UUID;

import com.tim15.ticketsSeller.security.annotations.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.tim15.ticketsSeller.DTOImplementations.NewLocation;
import com.tim15.ticketsSeller.domain.Location;
import com.tim15.ticketsSeller.exception.customExceptions.ForeignKeyException;
import com.tim15.ticketsSeller.exception.customExceptions.UniqueViolationException;
import com.tim15.ticketsSeller.serviceInterfaces.ILocationService;

@RestController
@CrossOrigin
@RequestMapping(value="/api/locations")
@Admin
public class LocationController {
	
	@Autowired
	private ILocationService locationService;
	
	
	@RequestMapping(
			value = "/",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> get() {
		
		return new ResponseEntity<Iterable<Location>>(locationService.getAll(), HttpStatus.OK);
	}
	
	@RequestMapping(
			value = "/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> get(@PathVariable("id") UUID id) {
		Location location = locationService.getById(id);
		if(location == null)
			return new ResponseEntity<String>("Location with given id doesn't exist.",HttpStatus.NOT_FOUND);
		return new ResponseEntity<Location>(location, HttpStatus.OK);
	}
	
	@RequestMapping(
			value = "/",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> post(@RequestBody NewLocation iNewLocation) throws IllegalArgumentException, NoSuchElementException, UniqueViolationException {
		
		UUID id = locationService.post(iNewLocation);
		UriComponents urlLocation = UriComponentsBuilder.newInstance().path("/api/locations/{id}").buildAndExpand(id);
		return ResponseEntity.created(urlLocation.toUri()).build();
	}
	
	@RequestMapping(
			value = "/{id}",
			method = RequestMethod.PUT,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> put(@PathVariable("id") UUID id, 
								@RequestBody NewLocation iNewLocation) throws IllegalArgumentException, NoSuchElementException, UniqueViolationException {
		locationService.put(id, iNewLocation);
		return new ResponseEntity<Location>(HttpStatus.OK);
	}
	
	@RequestMapping(
			value = "/{id}",
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> delete(@PathVariable("id") UUID id) throws ForeignKeyException {
		
		return locationService.delete(id) ? 
				new ResponseEntity<String>("Location is deleted.", HttpStatus.OK) 
				: 
				new ResponseEntity<String>("Location with given id doesn't exist.", HttpStatus.NOT_FOUND);
	}
	

}
