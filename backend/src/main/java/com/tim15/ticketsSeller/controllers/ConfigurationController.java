package com.tim15.ticketsSeller.controllers;

import com.tim15.ticketsSeller.security.annotations.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tim15.ticketsSeller.domain.Configuration;
import com.tim15.ticketsSeller.serviceInterfaces.IConfigurationService;

@RestController
@RequestMapping(value="/api/configurations")
@Admin
public class ConfigurationController {
	
	@Autowired
	private IConfigurationService configurationService;
	
	
	@RequestMapping(
			value = "/",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> get() {
		
		return new ResponseEntity<Iterable<Configuration>>(configurationService.get(), HttpStatus.OK);
	}
	
	@RequestMapping(
			value = "/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> get(@PathVariable("id") int id) {
		Configuration configuration = configurationService.getById(id);
		if(configuration == null)
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<Configuration>(configuration, HttpStatus.OK);
	}
	

	
	
}
