package com.tim15.ticketsSeller.controllers;

import java.io.IOException;
import java.util.UUID;

import com.tim15.ticketsSeller.security.annotations.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.JsonIOException;
import com.tim15.ticketsSeller.DTOImplementations.NewCategories;
import com.tim15.ticketsSeller.domain.Categories;
import com.tim15.ticketsSeller.serviceImplementations.CategoriesService;

@RestController
@RequestMapping(value="/api/categories")
@Admin
public class CategoriesController {
	
	@Autowired
	private CategoriesService categoriesService;
	

	
	@RequestMapping(
			value = "/",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAll(){
		return new ResponseEntity<Iterable<Categories>>(categoriesService.getAll(), HttpStatus.OK);
	}
	
	@RequestMapping(
			value = "/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getById(@PathVariable("id") UUID id){
		Categories categories = categoriesService.getById(id);
		if(categories == null)
			return new ResponseEntity<Categories>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<Categories>(categories, HttpStatus.OK);
	}
	
	@RequestMapping(
			value = "/",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> post(@RequestBody NewCategories categories) throws JsonIOException, IllegalArgumentException, IOException{
		UUID id = categoriesService.post(categories);
		UriComponents urlLocation = UriComponentsBuilder.newInstance().path("/api/categories/{id}").buildAndExpand(id);
		return ResponseEntity.created(urlLocation.toUri()).build();
	}
	
	@RequestMapping(
			value = "/{id}",
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> delete(@PathVariable("id") UUID id) throws JsonIOException, IllegalArgumentException, IOException{
		categoriesService.delete(id);
		return new ResponseEntity<String>("Categories doesnt exist anymore",HttpStatus.OK);
	}
	

}
