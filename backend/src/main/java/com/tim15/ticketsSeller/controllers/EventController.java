package com.tim15.ticketsSeller.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.tim15.ticketsSeller.security.annotations.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.tim15.ticketsSeller.DTOImplementations.NewEvent;
import com.tim15.ticketsSeller.domain.Event;
import com.tim15.ticketsSeller.serviceImplementations.EventService;

@RestController
@RequestMapping(value="/api/events")
@Admin
public class EventController {

	@Autowired
	private EventService eventService;
	

	
	
	@RequestMapping(
			value = "/",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> get() {
		return new ResponseEntity<Iterable<Event>>(eventService.getAll(),  HttpStatus.OK);
	}
	
	
	@RequestMapping(
			value = "/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> get(@PathVariable("id") UUID id) {
		Event event = eventService.getById(id);
		if(event == null)
			return new ResponseEntity<String>("Event is not found",  HttpStatus.NOT_FOUND);
		return new ResponseEntity<Event>(event,  HttpStatus.OK);
	}

	@RequestMapping(
			value = "/",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> post(@RequestBody NewEvent newEvent) {
		
		UUID id = eventService.post(newEvent);
		UriComponents urlLocation = UriComponentsBuilder.newInstance().path("/api/events/{id}").buildAndExpand(id);
		return ResponseEntity.created(urlLocation.toUri()).build();
	}
	
	@RequestMapping(
			value = "/all",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllPageable(Pageable page){
		Page<Event> events = eventService.getAllPageable(page);
		
		List<NewEvent> eventsDTO = new ArrayList<>();
		
		for(Event e: events) {
			eventsDTO.add(new NewEvent(e));
		}
		
		return new ResponseEntity<>(eventsDTO, HttpStatus.OK);
	}
	
	
}
