package com.tim15.ticketsSeller.repositoryInterfaces;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tim15.ticketsSeller.domain.Location;

public interface LocationRepository extends JpaRepository<Location, UUID> {

	Location findOneByName(String name);
	
}
