package com.tim15.ticketsSeller.repositoryInterfaces;

import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tim15.ticketsSeller.domain.Categories;

public interface CategoriesMongoRepository extends MongoRepository<Categories, UUID>{

}
