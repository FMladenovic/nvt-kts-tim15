package com.tim15.ticketsSeller.repositoryInterfaces;

import com.tim15.ticketsSeller.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findByUsername(String username);

    User findByPasswordAndUsernameOrEmail(String password, String username, String email);
}
