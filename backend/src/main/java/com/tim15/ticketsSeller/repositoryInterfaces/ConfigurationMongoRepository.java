package com.tim15.ticketsSeller.repositoryInterfaces;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tim15.ticketsSeller.domain.Configuration;

public interface ConfigurationMongoRepository extends MongoRepository<Configuration, Integer>{

}
