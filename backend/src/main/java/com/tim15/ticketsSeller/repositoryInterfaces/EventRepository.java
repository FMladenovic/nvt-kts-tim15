package com.tim15.ticketsSeller.repositoryInterfaces;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.tim15.ticketsSeller.domain.Event;

public interface EventRepository extends JpaRepository<Event, UUID> {
	
	Page<Event> findAll(Pageable pageable);
	
	List<Event> findByName(String eventName);

}
