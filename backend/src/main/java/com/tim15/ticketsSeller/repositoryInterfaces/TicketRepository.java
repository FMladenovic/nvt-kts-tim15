package com.tim15.ticketsSeller.repositoryInterfaces;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tim15.ticketsSeller.domain.Event;
import com.tim15.ticketsSeller.domain.Ticket;
import com.tim15.ticketsSeller.domain.User;

public interface TicketRepository extends JpaRepository<Ticket, UUID>{
	
	Page<Ticket> findAllByUser(User u, Pageable pageable);
	
	//vraca broj slobodnih karata za trazeni odredjeni datum odredjenog dogadjaja cije mesto pocinje prosledjenom oznakom
	@Query("SELECT COUNT(t) FROM Ticket t WHERE t.event = :event AND t.dateAndTime = :date AND t.user IS NULL AND t.seatMark LIKE CONCAT(UPPER(:segment),'%')")
	Integer countAvailableTicketsByEventAndParterreSegmentAndDate(@Param("event") Event e, @Param("segment") String parterreSegment, @Param("date") Date date);
	
	@Query("SELECT t FROM Ticket t WHERE t.event = :event AND t.dateAndTime = :date AND t.user IS NULL AND t.seatMark LIKE CONCAT(UPPER(:segment),'%')")
	Page<Ticket> getAvailableTicketsByEventAndParterreSegmentAndDate(@Param("event") Event e, @Param("segment") String parterreSegment, @Param("date") Date date, Pageable pageable);
	
	@Query("SELECT t FROM Ticket t WHERE t.event = :event AND t.dateAndTime = :date AND t.seatMark = :seat")
	Ticket findByEventAndDateAndSeatMark(@Param("event") Event e, @Param("date") Date date, @Param("seat") String seat);
	
	Ticket findByID(UUID id);
	
	List<Ticket> findAllByEvent(Event event);
	
	//List<Ticket> findAllByEventAndDateAndTime(Event event, Date dateTime);
	
	@Query("SELECT t FROM Ticket t WHERE t.event.id = :eventId AND t.term = :term")
	List<Ticket> findByEventAndTerm(@Param("eventId") UUID eventId, @Param("term") int term);
	
	//Get not bought parter tickets for event term
	@Query("SELECT t FROM Ticket t WHERE t.event.id = :eventId AND t.term = :term AND t.seatMark LIKE %:mark% AND t.bought = :bought")
	List<Ticket> findByEventTermSeatMarkBought(@Param("eventId") UUID eventId, @Param("term") int term, @Param("mark") String mark, @Param("bought") Boolean bought);


}
