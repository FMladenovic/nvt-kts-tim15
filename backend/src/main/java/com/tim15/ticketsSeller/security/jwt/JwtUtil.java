package com.tim15.ticketsSeller.security.jwt;

import com.tim15.ticketsSeller.domain.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.util.Date;

import static com.tim15.ticketsSeller.security.jwt.SecurityConstants.TOKEN_EXP;
import static com.tim15.ticketsSeller.security.jwt.SecurityConstants.TOKEN_PREFIX;
import static com.tim15.ticketsSeller.security.jwt.SecurityConstants.TOKEN_SECRET;

public class JwtUtil {

    public String createJwt(User user) {
        return TOKEN_PREFIX + Jwts.builder()
                .setSubject(user.getUsername())
                .claim("user", user)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + TOKEN_EXP))
                .signWith(Keys.hmacShaKeyFor(TOKEN_SECRET.getBytes()), SignatureAlgorithm.HS256)
                .compact();
    }

    public String getUsernameFromToken(String token) {
        String username;
        try {
            Claims claims = this.getClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser().setSigningKey(TOKEN_SECRET)
                    .parseClaimsJws(token).getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }
}