package com.tim15.ticketsSeller.serviceImplementations;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tim15.ticketsSeller.DTO.INewCategories;
import com.tim15.ticketsSeller.domain.Categories;
import com.tim15.ticketsSeller.repositoryInterfaces.CategoriesMongoRepository;
import com.tim15.ticketsSeller.serviceInterfaces.ICategoriesService;

@Service("categoriesService")
public class CategoriesService implements ICategoriesService {

	@Autowired
	private CategoriesMongoRepository categoriesRepository;
	
	@Override
	public Iterable<Categories> getAll() {
		return categoriesRepository.findAll();
	}

	@Override
	public Categories getById(UUID id) {
		Optional<Categories> categoryMyb = categoriesRepository.findById(id);
		return categoryMyb.isPresent() ? categoryMyb.get() : null;
	}

	@Override
	public UUID post(INewCategories categories)  throws IllegalArgumentException{
		if(categories == null)
			throw new IllegalArgumentException("Implementation of INewCategories doesnt exist.");

		Categories newCategories = new Categories(categories.getLayout(), categories.getParterCapacities(), categories.getCategories());
		categoriesRepository.save(newCategories);
		
		return newCategories.getId();
	}

	@Override
	public void delete(UUID id){
		categoriesRepository.deleteById(id);
	}

}
