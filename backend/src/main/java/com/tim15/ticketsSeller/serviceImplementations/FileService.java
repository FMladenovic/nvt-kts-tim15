package com.tim15.ticketsSeller.serviceImplementations;

import com.tim15.ticketsSeller.serviceInterfaces.IFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class FileService implements IFileService {

    private static Logger logger = LoggerFactory.getLogger(FileService.class);

    private String fileLocation;

    private final String PATH_FOR_FRONT = "../../../assets/users/";

    public FileService(@Value("${file.location}") String fileLocation) throws IOException {
        this.fileLocation = fileLocation;
        Path path = Paths.get(fileLocation);

        if (!Files.exists(path)) {
            Files.createDirectory(path);
        }
    }

    @Override
    public String uploadFile(MultipartFile file) {

        String name = UUID.randomUUID() + "-" + file.getOriginalFilename();

        try {
            Path path = Paths.get(fileLocation + name);
            Files.write(path, file.getBytes());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        return PATH_FOR_FRONT + name;
    }
}
