package com.tim15.ticketsSeller.serviceImplementations;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sun.xml.txw2.IllegalAnnotationException;
import com.tim15.ticketsSeller.domain.Configuration;
import com.tim15.ticketsSeller.repositoryInterfaces.ConfigurationMongoRepository;
import com.tim15.ticketsSeller.serviceInterfaces.IConfigurationService;

@Service("configurationService")
public class ConfigurationService implements IConfigurationService {
	
	@Autowired
	private ConfigurationMongoRepository configurationRepository;
	

	
	@Override
	public Iterable<Configuration> get() {
		
		return configurationRepository.findAll();
	}

	@Override
	public Configuration getById(Integer id)  throws IllegalAnnotationException {
		Optional<Configuration> configurationMyb = configurationRepository.findById(id);
		return configurationMyb.isPresent() ? configurationMyb.get() : null;
	}

}
