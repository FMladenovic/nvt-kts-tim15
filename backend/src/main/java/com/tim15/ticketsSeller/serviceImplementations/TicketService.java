package com.tim15.ticketsSeller.serviceImplementations;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tim15.ticketsSeller.DTO.ITicketList;
import com.tim15.ticketsSeller.DTOImplementations.BookTickets;
import com.tim15.ticketsSeller.DTOImplementations.CancelTickets;
import com.tim15.ticketsSeller.DTOImplementations.Reservation;
import com.tim15.ticketsSeller.DTOImplementations.TicketList;
import com.tim15.ticketsSeller.domain.Categories;
import com.tim15.ticketsSeller.domain.Category;
import com.tim15.ticketsSeller.domain.Event;
import com.tim15.ticketsSeller.domain.Ticket;
import com.tim15.ticketsSeller.domain.User;
import com.tim15.ticketsSeller.exception.customExceptions.EventUndefinedException;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidTicketOwnerException;
import com.tim15.ticketsSeller.exception.customExceptions.ReservationPeriodExpiredException;
import com.tim15.ticketsSeller.exception.customExceptions.TermsUndefinedInEventException;
import com.tim15.ticketsSeller.exception.customExceptions.TicketPaidException;
import com.tim15.ticketsSeller.exception.customExceptions.TicketsAlreadyGeneratedException;
import com.tim15.ticketsSeller.exception.customExceptions.TicketsLimitNumberException;
import com.tim15.ticketsSeller.exception.customExceptions.TicketsNotAvailableException;
import com.tim15.ticketsSeller.exception.customExceptions.UnexistingTicketException;
import com.tim15.ticketsSeller.repositoryInterfaces.CategoriesMongoRepository;
import com.tim15.ticketsSeller.repositoryInterfaces.EventRepository;
import com.tim15.ticketsSeller.repositoryInterfaces.TicketRepository;
import com.tim15.ticketsSeller.repositoryInterfaces.UserRepository;
import com.tim15.ticketsSeller.serviceInterfaces.ITicketService;

import javax.persistence.EntityNotFoundException;

@Service("ticketService")
public class TicketService implements ITicketService{
	
	@Autowired
	EventRepository eventRepository;
	
	@Autowired
	CategoriesMongoRepository categoriesRepository;
	
	@Autowired
	TicketRepository ticketRepository;
	
	@Autowired
	UserRepository userRepository;
	
	public void countTicketNumberToCreate(List<String> rows) {
		
		int aCounter = 0;
		int bCounter = 0;
		int cCounter = 0;
		int dCounter = 0;
		
		for (String row : rows) {
			for (String seatInRow : row.split("")) {
				if(seatInRow.equals("a")) {
					aCounter++;
				}
				if(seatInRow.equals("b")) {
					bCounter++;
				}
				if(seatInRow.equals("c")) {
					cCounter++;
				}
				if(seatInRow.equals("d")) {
					dCounter++;
				}
			}			
		}
		
		System.out.println("a: "+aCounter+" b: "+bCounter+" c: "+cCounter+" d: "+dCounter);
	}

	public ITicketList availableTicketsDTO(UUID eventId, int term){
		Optional<Event> eventMyb = eventRepository.findById(eventId);
		
		if(!eventMyb.isPresent())
			throw new NoSuchElementException("Event with id: "+eventId+" is undefined.");
		
		Event event = eventMyb.get();
		
		
		List<Ticket> tickets = ticketRepository.findByEventTermSeatMarkBought(eventId, term, "", false);
		//ArrayList<UUID> ids = new ArrayList<UUID>();
		
		UUID categoriesId = event.getCategoriesId();
		Optional<Categories> categoriesMyb = categoriesRepository.findById(categoriesId);
		
		if(!categoriesMyb.isPresent())
			throw new NoSuchElementException("Categories with id: "+categoriesId+" is undefined.");
				
		Categories categories = categoriesMyb.get();
		List<Integer> parterCapacities = categories.getParterCapacities();
		List<Integer> availableParterCapacities = countAvailableParterTickets(parterCapacities, tickets);
		ITicketList send = new TicketList(categories, availableParterCapacities, event.getTicketsLimit()); //CHANGE
		
		for(Ticket t : tickets) {
			send.getTicketsIds().put(t.getSeatMark(), t.getID());
		}
		
		return send;
	}
	
	
	@SuppressWarnings("unused")
	private ArrayList<Integer> countAvailableParterTickets(List<Integer> parterCapacities, List<Ticket> tickets) {
		ArrayList<Integer> parterList = new ArrayList<Integer>();
		for(Integer i : parterCapacities) {
			parterList.add(0);
		}
		
		String mark;
		String[] splited;
		int i;
		for(Ticket t : tickets) {
			mark = t.getSeatMark();
			if(t.getSeatMark().contains("P")) {
				splited = mark.split("");
				i = Integer.parseInt(splited[1]);
				int holder = parterList.get(i-1);
				holder++;
				parterList.set(i-1, holder);
			}
		}
		
		return parterList;
	}
	
	@Override
	public Boolean generateTicketsForEvent(UUID eventId) throws EventUndefinedException, TicketsAlreadyGeneratedException {
		
		Optional<Event> ev = eventRepository.findById(eventId);
		/* 
		Ne ovako
		Event e = ev.get();
		if(e == null) {
			throw new EventUndefinedException("Event with id: "+eventId+" is undefined.");
		}
		*/
		
		if(!ev.isPresent()) 
			throw new NoSuchElementException("Event with id: "+eventId+" is undefined.");
		
		Event e = ev.get();
		
		if(!(e.getTickets().isEmpty())) {
			throw new TicketsAlreadyGeneratedException("Tickets for event: "+e.getName()+"were already generated." );
		}
		
		Optional<Categories> categoriesForEvent = categoriesRepository.findById(e.getCategoriesId());
		
		if(!categoriesForEvent.isPresent())
			throw new NoSuchElementException("Configuration with given id doesnt exist.");
		
		Categories categories = categoriesForEvent.get();
		
		countTicketNumberToCreate(categories.getLayout());
		
		int term = 0;
		for (Date dateTime : e.getTerms()) {
			term++;
			
			int aCounter = 0;
			int bCounter = 0;
			int cCounter = 0;
			int dCounter = 0;
			
			for (Category category : categories.getCategories()) {
				
				for (String rowInCategory : category.getSeats()) {
					for (String seatInRow : rowInCategory.split("")) {
						
						if(seatInRow.equals("a")) {
							aCounter++;
							e.getTickets().add(new Ticket(null, e, dateTime, term, "A"+aCounter, category.getPrice(), category.getName(), false));
						}
						
						else if(seatInRow.equals("b")) {
							bCounter++;
							e.getTickets().add(new Ticket(null, e, dateTime, term, "B"+bCounter, category.getPrice(), category.getName(), false));
						}
						
						else if(seatInRow.equals("c")) {
							cCounter++;
							e.getTickets().add(new Ticket(null, e, dateTime, term, "C"+cCounter, category.getPrice(), category.getName(), false));
						}
						
						else if(seatInRow.equals("d")) {
							dCounter++;
							e.getTickets().add(new Ticket(null, e, dateTime, term, "D"+dCounter, category.getPrice(), category.getName(), false));
						}
					}	
				}
				
				List<Integer> parterCapacities = category.getParterCapacities();
				
				if(parterCapacities != null) {
					int placeInList = 1;
					
					for (Integer capacity : parterCapacities) {
						if(capacity != null) {
							for (int i = 0; i < capacity; i++) {
								e.getTickets().add(new Ticket(null, e, dateTime, term, "P"+placeInList, category.getPrice(), category.getName(), false));
							}
						}
						placeInList++;
					}
				}	
			}
		}
		
		ticketRepository.saveAll(e.getTickets());
		
		return true;
		
	}
	
	//HELPER METODA ZA bookTickets
	public Boolean termsExist(Event e, List<Date> terms) {
		
		boolean flag = true;
		
		for (Date date : terms) {
			if(!(e.getTerms().contains(date))) {
				flag = false;
			}
		}
		
		return flag;	
	}
	
	//HELPER METODA ZA bookTickets
	public Boolean termDoesntExpired(Date date) {
		
		Date d = new Date();
		
		if(d.before(date)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	//HELPER METODA ZA ticketsAvailable
	//broji koliko za svaki segment partera treba mesta
	public Hashtable<String, Integer> countParterreSegments(List<String> seats){
		
		Hashtable<String, Integer> result = new Hashtable<String, Integer>();
		
		for (String string : seats) {
			if(!(result.containsKey(string))) {
				result.put(string, 1);
			}
			else {
				result.put(string, result.get(string) + 1);
			}
		}
		
		return result;
	}
	
	//HELPER METODA ZA bookTickets
	public List<Ticket> ticketsAvailable(Event e, List<Date> terms, List<String> seats) {
		
		List<Ticket> ticketsAvailable = new ArrayList<Ticket>();
		
		List<String> markedSeats = new ArrayList<String>();
		List<String> parterreSeats = new ArrayList<String>();
		
		for (String seat : seats) {
			if(seat.startsWith("P")) {
				parterreSeats.add(seat);
			}
			else {
				markedSeats.add(seat);
			}
		}
		
		for (String markedSeat : markedSeats) {
			for(Date d : terms) {
				Ticket t = ticketRepository.findByEventAndDateAndSeatMark(e, d, markedSeat);
				if(!(t.equals(null))) {
					if(t.getUser() == null) {
						ticketsAvailable.add(t);
					}
				}
			}
		}
		
		Hashtable<String, Integer> requiredParterreSeats = countParterreSegments(parterreSeats);
		
		Boolean checkParterreSegments = true;
		
		//proveravamo da li su mesta iz partera za svaki segment partera dostupna za sve zahtevane datume
		for (String parterreSegment : requiredParterreSeats.keySet()) {
			for (Date d : terms) {
				Integer availableForDate = ticketRepository.countAvailableTicketsByEventAndParterreSegmentAndDate(e, parterreSegment, d);
				if(availableForDate < requiredParterreSeats.get(parterreSegment)) {
					checkParterreSegments = false;
					break;
				}
			}
		}
		
		if(checkParterreSegments) {
			for (String parterreSegment : requiredParterreSeats.keySet()) {
				for (Date d : terms) {
					List<Ticket> tc = ticketRepository.getAvailableTicketsByEventAndParterreSegmentAndDate(e, parterreSegment, d, PageRequest.of(0,requiredParterreSeats.get(parterreSegment))).getContent();
					ticketsAvailable.addAll(tc);
				}		
			}
		}
		
		return ticketsAvailable;
		
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public List<Ticket> bookTickets(BookTickets bookTicketsDTO) throws EventUndefinedException, TermsUndefinedInEventException, ReservationPeriodExpiredException, TicketsLimitNumberException, TicketsNotAvailableException {
		
		Event e = eventRepository.getOne(bookTicketsDTO.getEventId());
		
		//da li trazeni dogadjaj postoji
		if(e == null) {
			throw new EventUndefinedException("Event with id: "+bookTicketsDTO.getEventId()+" is undefined.");
		}
		
		//da li termini u rezervaciji odgovaraju terminima koji postoje u dogadjaju
		if(!termsExist(e, bookTicketsDTO.getTerms())) {
			throw new TermsUndefinedInEventException("Event "+e.getName()+" doesn't have requested terms.");
		}
		
		//da li su rezervacije jos uvek moguce za dati dogadjaj
		if(!termDoesntExpired(e.getDueDateForReservation())) {
			throw new ReservationPeriodExpiredException("Reservation period for event "+e.getName()+" was expired.");
		}
		
		if(bookTicketsDTO.getSeats().size() > e.getTicketsLimit()) {
			throw new TicketsLimitNumberException("Limit for event "+e.getName()+" is "+e.getTicketsLimit());
		}
		
		List<Ticket> toReserve = ticketsAvailable(e, bookTicketsDTO.getTerms(), bookTicketsDTO.getSeats());
		
		if(toReserve.size() < (bookTicketsDTO.getTerms().size() * bookTicketsDTO.getSeats().size())) {
			throw new TicketsNotAvailableException("Required tickets not not available.");
		}
		
		User u = userRepository.findByUsername(bookTicketsDTO.getUsername()).orElseThrow(EntityNotFoundException::new);
		
		for (Ticket ticket : toReserve) {
			ticket.setUser(u);
		}
		
		ticketRepository.saveAll(toReserve);
		
		return toReserve;
	}

	@Override
	public Boolean payTickets(List<Ticket> tickets, User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ticket cancelTicket(CancelTickets cancelTiDTO) throws TicketPaidException, UnexistingTicketException, InvalidTicketOwnerException {
		
		User u = userRepository.findByUsername(cancelTiDTO.getUsername()).orElseThrow(EntityNotFoundException::new);
		
		Ticket ticketToCancel = ticketRepository.findByID(cancelTiDTO.getTicketIds());
		
		if(ticketToCancel == null) {
			throw new UnexistingTicketException("Cannot cancel unexisting ticket.");
		}
		
		if(!(ticketToCancel.getUser().equals(u))) {
			throw new InvalidTicketOwnerException("Ticket with id "+ticketToCancel.getID()+" doesn't belong to "+u.getUsername());
		}
		
		if(ticketToCancel.getBought() == false) {
			throw new TicketPaidException("Cannot cancel paid ticket.");
		}
		
		ticketToCancel.setUser(null);
		
		ticketRepository.save(ticketToCancel);
		
		return ticketToCancel;
	}
	
	
	public List<Ticket> getTickets(UUID eventId, int term){
		
		return ticketRepository.findByEventAndTerm(eventId, term);
	}
	
	public boolean reserveTickets(UUID eventId, int term,  Reservation reservationDTO) {
		List<Ticket> reserve = new ArrayList<Ticket>();
		if(reservationForSeats(eventId, term, reservationDTO.getTicketIds(), reserve) && reservationForParter(eventId, term, reservationDTO.getParterTickets(), reserve))
			ticketRepository.saveAll(reserve);
		return true;
	}
	private boolean reservationForSeats(UUID eventId, int term, List<UUID> ticketIds, List<Ticket> reserve) {
		for(UUID id : ticketIds) {
			Optional<Ticket> ticketMyb = ticketRepository.findById(id);
			if(ticketMyb == null) 
				return false; //throw not found;
			Ticket ticket = ticketMyb.get();
			if(ticket.getBought())
				return false; //throw custom exception
			ticket.setBought(true);
			//ticket.setUser(user); SET ACTIVE USER
			reserve.add(ticket);
		}
		return true;
	}
	
	private boolean reservationForParter(UUID eventId, int term, List<Integer> parterTickets, List<Ticket> reserve) {
		for(int i = 0; i < parterTickets.size(); i++) {
			int neededTickets = parterTickets.get(i);
			List<Ticket> availableTickets = ticketRepository.findByEventTermSeatMarkBought(eventId, term, "P"+(i+1), false);
			if(availableTickets.size()<neededTickets)
				return false;//throw custom exception
			for(int j= 0; j < neededTickets; j++) {
				Ticket ticketHolder = availableTickets.get(j);
				ticketHolder.setBought(true);
				//ticketHolder.setUser(user); SET ACTIVE USER
				reserve.add(ticketHolder);
			}
		}
		return true;
	}
	
}
