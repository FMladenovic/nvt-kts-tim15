package com.tim15.ticketsSeller.serviceImplementations;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tim15.ticketsSeller.DTO.INewEvent;
import com.tim15.ticketsSeller.domain.Event;
import com.tim15.ticketsSeller.repositoryInterfaces.EventRepository;
import com.tim15.ticketsSeller.serviceInterfaces.IEventService;

@Service("eventService")
public class EventService implements IEventService {

	@Autowired
	private EventRepository eventRepository;
	
	
	public EventService(EventRepository eventRepository) throws IllegalArgumentException {
		if(eventRepository == null)
			throw new IllegalArgumentException("Implementation of event repository must exist!");
		
		this.eventRepository = eventRepository;
	}
	
	@Override
	public Iterable<Event> getAll() {
		return eventRepository.findAll();
	}
	
	@Override
	public Page<Event> getAllPageable(Pageable pageable){
		return eventRepository.findAll(pageable);
		
	}

	@Override
	public Event getById(UUID id) {
		
		Optional<Event> eventMyb = eventRepository.findById(id);
		
		if(!eventMyb.isPresent())
			return null;
		
		return eventMyb.get();
		
	}

	@Override
	public UUID post(INewEvent event) {
		Event newEvent = new Event(event.getName(), event.getTicketsLimit(), event.getLocationId(), event.getCategoriesId());
		newEvent.setDescription(event.getDescription());
		eventRepository.save(newEvent);
		return newEvent.getId();
	}

	@Override
	public void put(UUID id, INewEvent event) {
		// TODO Auto-generated method  
		
	}

	@Override
	public void delete(UUID id) {
		eventRepository.deleteById(id);
	}

}
