package com.tim15.ticketsSeller.serviceImplementations;

import com.tim15.ticketsSeller.DTOImplementations.NewUser;
import com.tim15.ticketsSeller.DTOImplementations.UpdateUser;
import com.tim15.ticketsSeller.DTOImplementations.UserCredentials;
import com.tim15.ticketsSeller.DTOImplementations.UserDetails;
import com.tim15.ticketsSeller.config.Mapper;
import com.tim15.ticketsSeller.domain.ConfirmationToken;
import com.tim15.ticketsSeller.domain.User;
import com.tim15.ticketsSeller.event.RegistrationSuccessEvent;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidLoginException;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidPasswordException;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidTokenException;
import com.tim15.ticketsSeller.repositoryInterfaces.ConfirmationTokenRepository;
import com.tim15.ticketsSeller.repositoryInterfaces.UserRepository;
import com.tim15.ticketsSeller.security.jwt.JwtUtil;
import com.tim15.ticketsSeller.serviceInterfaces.IFileService;
import com.tim15.ticketsSeller.serviceInterfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.UUID;

@Service
@Validated
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private Mapper mapper;

    @Autowired
    private IFileService fileService;

    @Autowired
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;

    @Override
    public String login(UserCredentials userCredentials) {
        try {
            UsernamePasswordAuthenticationToken authenticationToken =
                    new UsernamePasswordAuthenticationToken(userCredentials.getUsername(), userCredentials.getPassword());

            Authentication authentication = authenticationManager.authenticate(authenticationToken);

            return jwtUtil.createJwt((User) authentication.getPrincipal());
        } catch (Exception ex) {
            throw new InvalidLoginException("Login failed. " + ex.getMessage());
        }
    }

    @Override
    public NewUser registerUser(NewUser newUser, MultipartFile image) {
        User user = mapper.map(newUser, User.class);
        user.setId(UUID.randomUUID());

        if (image != null) {
            user.setImagePath(fileService.uploadFile(image));
        }

        userRepository.save(user);
        eventPublisher.publishEvent(new RegistrationSuccessEvent(user));

        return mapper.map(user, NewUser.class);
    }

    @Override
    @Transactional
    public void confirmUserAccount(String token) {
        ConfirmationToken confirmationToken =
                confirmationTokenRepository.findByToken(token).orElseThrow(EntityNotFoundException::new);
        UUID userId;

        try {
            userId = confirmationToken.getUser().getId();
        } catch (NullPointerException ex) {
            throw new IllegalArgumentException("Id of user must exist.");
        }

        if (hasTokenExpired(confirmationToken)) {
            userRepository.deleteById(userId);
            throw new InvalidTokenException("Given token has expired.");
        }

        enableConfirmation(userId, confirmationToken);
    }

    private void enableConfirmation(UUID userId, ConfirmationToken confirmationToken) {
        User user = userRepository.findById(userId).orElseThrow(EntityNotFoundException::new);
        user.setEnabled(true);

        //delete token
        user.setConfirmationToken(null);
        userRepository.save(user);
        confirmationTokenRepository.deleteById(confirmationToken.getId());
    }

    private boolean hasTokenExpired(ConfirmationToken confirmationToken) {
        return LocalDate.now().isAfter(confirmationToken.getCreatedDate().plusDays(1));
    }

    @Override
    public UpdateUser updateUser(UpdateUser updatedUser, MultipartFile image, Authentication authentication) {

        User user = getUserFromAuthentication(authentication);

        if (!isOldPasswordCorrect(updatedUser.getOldPassword(), user)) {
            throw new InvalidPasswordException("Given password is not valid.");
        }

        mapper.map(updatedUser, user);

        if (image != null) {
            user.setImagePath(fileService.uploadFile(image));
        }

        userRepository.save(user);
        return mapper.map(user, UpdateUser.class);
    }

    @Override
    public UserDetails getLoggedUser(Authentication authentication) {
        return mapper.map(getUserFromAuthentication(authentication), UserDetails.class);
    }

    private boolean isOldPasswordCorrect(String oldPassword, User user) {
        return user.getPassword().equals(oldPassword);
    }

    @Transactional
    public User getUserFromAuthentication(Authentication authentication) {
        try {
            return (userRepository.findByUsername(((User) authentication.getPrincipal()).getUsername()))
                    .orElseThrow(EntityNotFoundException::new);
        } catch (ClassCastException | NullPointerException | EntityNotFoundException ex) {
            throw new AccessDeniedException("User is not logged in. Access is denied.");
        }
    }
}
