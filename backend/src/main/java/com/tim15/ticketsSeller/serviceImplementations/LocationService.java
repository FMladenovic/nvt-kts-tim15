package com.tim15.ticketsSeller.serviceImplementations;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.tim15.ticketsSeller.DTO.INewLocation;
import com.tim15.ticketsSeller.domain.Location;
import com.tim15.ticketsSeller.exception.customExceptions.ForeignKeyException;
import com.tim15.ticketsSeller.exception.customExceptions.UniqueViolationException;
import com.tim15.ticketsSeller.repositoryInterfaces.ConfigurationMongoRepository;
import com.tim15.ticketsSeller.repositoryInterfaces.LocationRepository;
import com.tim15.ticketsSeller.serviceInterfaces.ILocationService;

@Service("locationService")
public class LocationService implements ILocationService {
	
	@Autowired
	private LocationRepository locationRepository;
	
	@Autowired
	private ConfigurationMongoRepository configurationRepository;
	

	@Override
	public Iterable<Location> getAll() {	
		return locationRepository.findAll();
	}

	@Override
	public Location getById(UUID id) throws IllegalArgumentException{		
		Optional<Location> locationMyb = locationRepository.findById(id);
		return locationMyb.isPresent() ? locationMyb.get() : null;
	}
	
	@Override
	public UUID post(INewLocation location) throws IllegalArgumentException, NoSuchElementException, UniqueViolationException{	
		if(location == null)
			throw new IllegalArgumentException("You didn't send implementation of NewLocation.");
		
		if(!configurationRepository.existsById(location.getConfigurationId()))
			throw new NoSuchElementException("Configuration with given id doesnt exist.");
		
		Location saveLocation = new Location(location.getName(), location.getConfigurationId());
		
		try {
			locationRepository.save(saveLocation);
		}
		catch(DataIntegrityViolationException ex) {
			throw new UniqueViolationException("Location with given name already exists");
		}

		
		return saveLocation.getId();
	}

	@Override
	public void put(UUID id, INewLocation location) throws IllegalArgumentException, NoSuchElementException, UniqueViolationException{
		if(location == null)
			throw new IllegalArgumentException("You didn't send implementation of NewLocation.");
		
		Optional<Location> locationMyb = locationRepository.findById(id);
		
		if(!locationMyb.isPresent())
			throw new NoSuchElementException("Location with given id doesnt exist.");
		
		if(!configurationRepository.existsById(location.getConfigurationId()))
			throw new NoSuchElementException("Configuration with given id doesnt exist.");
		
		
		Location locationUpd = locationMyb.get();
		locationUpd.setName(location.getName());
		locationUpd.setConfigurationId(location.getConfigurationId());
		locationUpd.setActive(location.isActive());

		try {
			locationRepository.save(locationUpd);
		}
		catch(DataIntegrityViolationException ex) {
			throw new UniqueViolationException("Location with given name already exists");
		}
	}

	@Override
	public boolean delete(UUID id) throws ForeignKeyException {
		try {
			locationRepository.deleteById(id);
		}
		catch(EmptyResultDataAccessException ex) {
			return false;
		}
		catch(DataIntegrityViolationException  ex) {
			throw new ForeignKeyException("Another entity is using this location!");
		}
		
		return true;
	}



}
