package com.tim15.ticketsSeller.enumeration;

public enum Role {
    ROLE_ADMIN, ROLE_USER
}