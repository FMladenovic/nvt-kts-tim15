package com.tim15.ticketsSeller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.tim15.ticketsSeller.domain.Configuration;
import com.tim15.ticketsSeller.repositoryInterfaces.ConfigurationMongoRepository;

@SpringBootApplication
public class TicketsSellerApplication {
	@Autowired
    private ConfigurationMongoRepository repo;
    
    private List<Configuration> load() {
    	Type listType = new TypeToken<ArrayList<Configuration>>(){}.getType();
		List<Configuration> configurations = null;
		Path resourceDirectory = Paths.get("src","main","resources","static","configurations.json");
		try {
			configurations = new Gson().fromJson(new FileReader(resourceDirectory.toFile()), listType);
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}		
		return configurations;
    }

    @Bean
    CommandLineRunner preLoadMongo() throws Exception {
        return args -> {
        	try {
        		repo.saveAll(load());
        	}catch(Exception e) {
        		System.out.println("Configuration repository is already initialized. {"+e.getMessage()+"}");
        	}
       };
    }
	public static void main(String[] args) {
		SpringApplication.run(TicketsSellerApplication.class, args);
	}

}
