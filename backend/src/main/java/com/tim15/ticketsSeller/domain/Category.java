package com.tim15.ticketsSeller.domain;

import java.util.List;

import org.springframework.util.StringUtils;

public class Category {

	private String name;
	private double price;
	private String color;
	
	private List<String> seats;
	private List<Integer> parterCapacities;
	
	
	public Category(String name, double price, List<String> seats, List<Integer> parterCapacities) throws IllegalArgumentException {
		if(!checkCompatibilityOfSeatsAndParterCapacities(seats, parterCapacities))
			throw new IllegalArgumentException("Seats and parter capacities are not compatible");
		
		this.setName(name);
		this.setPrice(price);
		this.seats = seats;
		this.parterCapacities = parterCapacities;
		
	}

	public Category() {
		
	}

	public String getName() {
		return name;
	}

	public double getPrice() {
		return price;
	}

	public String getColor() {
		return color;
	}
	
	public List<String> getSeats() {
		return seats;
	}

	public List<Integer> getParterCapacities() {
		return parterCapacities;
	}


	public void setName(String name) throws IllegalArgumentException {		
		if(name == null || name.trim().length() == 0)
			throw new IllegalArgumentException("Name must exist.");
		this.name = name;
	}

	public void setPrice(double price) throws IllegalArgumentException{	
		if(price <= 0)
			throw new IllegalArgumentException("Price must be greater then 0.");		
		this.price = price;
	}
	
	public void setSeats(List<String> seats) {
		if(seats == null)
			throw new IllegalArgumentException("Seats must exist.");
		this.seats = seats;
	}

	public void setParterCapacities(List<Integer> parterCapacities) {
		this.parterCapacities = parterCapacities;
	}

	public void setColor(String color){
		this.color = color;
	}
	
	private static boolean checkCompatibilityOfSeatsAndParterCapacities(List<String> seats, List<Integer> parterCapacities) throws IllegalArgumentException{
		
		if(seats == null)
			throw new IllegalArgumentException("Seats must exist.");
		int countP = 0;
		for(String line : seats) {
			countP = countP + StringUtils.countOccurrencesOf(line.toLowerCase(), "p");
		}
		
		if(countP == 0)
			return parterCapacities==null;
		
		if(parterCapacities==null)
			return false;
		
		int countNotNull = 0;
		for(Integer value : parterCapacities) {
			if(value!=null)
				countNotNull++;
		}
		
		return countNotNull==countP;
	}
	
	
}
