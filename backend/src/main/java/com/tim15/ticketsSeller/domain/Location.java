package com.tim15.ticketsSeller.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Locations")
public class Location {

	@Id 
	@org.hibernate.annotations.Type(type="uuid-char")
	private UUID id;
	
	@Column(name = "name", nullable = false, unique = true)
	private String name;
	
	@Column(name = "configurationId", nullable = false)
	private int configurationId;
	
	@Column(name = "active", nullable = false)
	private boolean active;

	public Location() {}
	
	public Location(String name, int configurationId) throws IllegalArgumentException {
		this.id = UUID.randomUUID();
		this.setName(name);	
		this.setConfigurationId(configurationId);
		this.active = true;
	}
	
	public Location(UUID id, String name, int configurationId) throws IllegalArgumentException {
		this.setId(id);
		this.setName(name);	
		this.setConfigurationId(configurationId);
		this.active = true;

	}
	

	public UUID getId() {
		return this.id;
	}
	
	public String getName() {
		return name;
	}

	public int getConfigurationId() {
		return configurationId;
	}

	
	public boolean isActive() {
		return this.active;
	}
	
	public void setId(UUID id) throws IllegalArgumentException {
		
		if(id == null)
			throw new IllegalArgumentException("ID must exist.");
		
		this.id = id;
	}
	
	public void setName(String name) throws IllegalArgumentException {
		
		if(name == null || name.trim().length() == 0)
			throw new IllegalArgumentException("Name must exist.");
		
		this.name = name;
	}

	public void setConfigurationId(int configurationId) throws IllegalArgumentException {
		
		if(configurationId < 0)
			throw new IllegalArgumentException("configurationId must be greater then 0.");

		this.configurationId = configurationId;
	}
	
	
	public void setActive(boolean active) {
		this.active = active;
	}
}
