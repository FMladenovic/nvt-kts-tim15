package com.tim15.ticketsSeller.domain;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "Events")
public class Event {
	
	@Id	
	@org.hibernate.annotations.Type(type="uuid-char")
	private UUID id;
	
	@Column(name = "name", nullable= false)
	private String name;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "dueDateForReservation")
	private Date dueDateForReservation;

	@ElementCollection(fetch = FetchType.LAZY)
	private List<Date> terms;
	
	@Column
	@OneToMany(mappedBy = "event", fetch = FetchType.LAZY)
	private List<Ticket> tickets;
	
	@Column(name = "ticketsLimit", nullable= false)
	private int ticketsLimit;
/*	
    @ManyToOne(targetEntity = Location.class, cascade = CascadeType.DETACH)
	@JoinColumn(name = "locationId")
*/
	@Column(name = "locationId")
	@org.hibernate.annotations.Type(type="uuid-char")
	private UUID locationId;
	
	@Column(name = "categoriesId", nullable= false)
	@org.hibernate.annotations.Type(type="uuid-char")
	private UUID categoriesId;
	
	
	public Event(String name, int ticketsLimit, UUID locationId, UUID categoriesId) {
		this.id = UUID.randomUUID();
		this.name = name;
		this.ticketsLimit = ticketsLimit;
		this.locationId = locationId;
		this.categoriesId = categoriesId;
	}
	

	public Event(UUID iD, String name, int ticketsLimit, UUID locationId, UUID categoriesId) {
		super();
		this.id = iD;
		this.name = name;
		this.ticketsLimit = ticketsLimit;
		this.locationId = locationId;
		this.categoriesId = categoriesId;
	}
	
	
	
	
	public Event(UUID id, String name, String description, Date dueDateForReservation, List<Date> terms,
			List<Ticket> tickets, int ticketsLimit, UUID locationId, UUID categoriesId) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.dueDateForReservation = dueDateForReservation;
		this.terms = terms;
		this.tickets = tickets;
		this.ticketsLimit = ticketsLimit;
		this.locationId = locationId;
		this.categoriesId = categoriesId;
	}


	public Event() {}
	
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) throws IllegalArgumentException{
		if(id == null)
			throw new IllegalArgumentException("ID must exist"); 
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		if(name == null)
			throw new IllegalArgumentException("Name must exist"); 
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}	
	
	public List<Date> getTerms() {
		return terms;
	}

	public void setTerms(List<Date> terms) {
		this.terms = terms;
	}

	public List<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}

	public int getTicketsLimit() {
		return ticketsLimit;
	}
	public void setTicketsLimit(int ticketsLimit) {
		this.ticketsLimit = ticketsLimit;
	}
	public UUID getLocationId() {
		return locationId;
	}
	public void setLocationId(UUID locationId) {
		if(locationId == null)
			throw new IllegalArgumentException("location id must exist");
		this.locationId = locationId;
	}
	public UUID getCategoriesId() {
		return categoriesId;
	}
	public void setCategoriesId(UUID categoriesId) throws IllegalArgumentException {
		if(categoriesId == null)
			throw new IllegalArgumentException("Categories id must exist"); 
		this.categoriesId = categoriesId;
	}

	public Date getDueDateForReservation() {
		return dueDateForReservation;
	}

	public void setDueDateForReservation(Date dueDateForReservation) {
		this.dueDateForReservation = dueDateForReservation;
	}
	
}
