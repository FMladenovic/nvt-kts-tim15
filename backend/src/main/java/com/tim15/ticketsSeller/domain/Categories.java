package com.tim15.ticketsSeller.domain;

import java.util.List;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

@Document(collection = "categories")
public class Categories {
	@Id
	private UUID id;
	private List<String> layout;
	private List<Integer> parterCapacities;
	private List<Category> categories;
	
	public Categories() {
		
	}

	public Categories(UUID id, List<String> layout, List<Integer> parterCapacities,
			List<Category> categories) throws IllegalArgumentException {
		
		if(!checkCompatibilityOfLayoutAndParterCapacities(layout, parterCapacities))
			throw new IllegalArgumentException("Layout and parterCapacities are not compatible");
		if(!checkCompatibilityOfLayoutAndCategories(layout, categories))
			throw new IllegalArgumentException("Layout and categories are not compatible");
		if(!checkCompatibilityOfCategoriesAndParterCapacities(categories, parterCapacities))
			throw new IllegalArgumentException("Categories and parterCapacities are not compatible");

		this.setId(id);
		this.layout = layout;
		this.parterCapacities = parterCapacities;
		this.categories = categories;
	}

	public Categories(List<String> layout, List<Integer> parterCapacities,
			List<Category> categories) throws IllegalArgumentException {
		
		if(!checkCompatibilityOfLayoutAndParterCapacities(layout, parterCapacities))
			throw new IllegalArgumentException("Layout and parterCapacities are not compatible");
		if(!checkCompatibilityOfLayoutAndCategories(layout, categories))
			throw new IllegalArgumentException("Layout and categories are not compatible");
		if(!checkCompatibilityOfCategoriesAndParterCapacities(categories, parterCapacities))
			throw new IllegalArgumentException("Categories and parterCapacities are not compatible");
		
		this.id = UUID.randomUUID();
		this.layout = layout;
		this.parterCapacities = parterCapacities;
		this.categories = categories;
	}
	
	public UUID getId() {
		return id;
	}

	public List<String> getLayout() {
		return layout;
	}

	public List<Integer> getParterCapacities() {
		return parterCapacities;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setId(UUID id) throws IllegalArgumentException {
		if(id == null)
			throw new IllegalArgumentException("ID must exist.");
		this.id = id;
	}

	public void setLayout(List<String> layout) throws IllegalArgumentException {
		if(layout == null)
			throw new IllegalArgumentException("Layout must exist.");
		if(!checkCompatibilityOfLayoutAndParterCapacities(layout, this.parterCapacities))
			throw new IllegalArgumentException("Layout and parterCapacities are not compatible");
		if(!checkCompatibilityOfLayoutAndCategories(layout, this.categories))
			throw new IllegalArgumentException("Layout and categories are not compatible");
		this.layout = layout;
	}

	public void setParterCapacities(List<Integer> parterCapacities) throws IllegalArgumentException {
		if(parterCapacities == null)
			throw new IllegalArgumentException("Parter capacities must exist.");
		if(!checkCompatibilityOfCategoriesAndParterCapacities(this.categories, parterCapacities))
			throw new IllegalArgumentException("Categories and parterCapacities are not compatible");
		if(!checkCompatibilityOfLayoutAndParterCapacities(this.layout, parterCapacities))
			throw new IllegalArgumentException("Layout and parterCapacities are not compatible");
		this.parterCapacities = parterCapacities;
	}

	public void setCategories(List<Category> categories) throws IllegalArgumentException {
		if(categories == null)
			throw new IllegalArgumentException("Categories must exist.");
		if(!checkCompatibilityOfLayoutAndCategories(this.layout, categories))
			throw new IllegalArgumentException("Layout and categories are not compatible");
		if(!checkCompatibilityOfCategoriesAndParterCapacities(categories, this.parterCapacities))
			throw new IllegalArgumentException("Layout and parterCapacities are not compatible");
		this.categories = categories;
	}
	
	private static boolean checkCompatibilityOfLayoutAndParterCapacities(List<String> layout, List<Integer> parterCapacities) throws IllegalArgumentException {
		if(layout == null  || parterCapacities == null )
			throw new IllegalArgumentException("Null argument.");
		int countP = 0;
		for(String line : layout) {
			countP = countP + StringUtils.countOccurrencesOf(line.toLowerCase(), "p");
		}
		return countP == parterCapacities.size();
	}
	
	private static boolean checkCompatibilityOfLayoutAndCategories(List<String> layout, List<Category> categories) throws IllegalArgumentException{
		if(layout == null  || categories == null )
			throw new IllegalArgumentException("Null argument.");
		
		for(Category c : categories) {
			if(layout.size() != c.getSeats().size())
				return false;
			for(int i = 0; i < layout.size(); i++) {
				if(layout.get(i).length() != c.getSeats().get(i).length())
					return false;
			}
		}
		return true;
	}
	
	private static boolean checkCompatibilityOfCategoriesAndParterCapacities(List<Category> categories, List<Integer> parterCapacities) throws IllegalArgumentException {
		if(categories == null  || categories == null )
			throw new IllegalArgumentException("Null argument.");
		
		for(Category c : categories) {
			List<Integer> holder = c.getParterCapacities();
			if(holder != null) {
				if(holder.size() != parterCapacities.size())
					return false;
				for(int i=0; i<holder.size(); i++) {
					Integer counter = holder.get(i);
					if(counter!=null && counter > parterCapacities.get(i))
						return false;
				}
			}
		}
		
		return true;
		
	}
	
	
	
}
