package com.tim15.ticketsSeller.domain;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "Ticket")
public class Ticket {
	
	@Id
	@org.hibernate.annotations.Type(type="uuid-char")
	private UUID ID;

	@ManyToOne
	private User user;

	@ManyToOne(optional = false)
	private Event event;
	
	@Column(name = "date_and_time", nullable = false)
	private Date dateAndTime;
	
	@Column(name = "term", nullable = false)
	private int term; //da se ne bi jurili sa konkretnim datumom na karti samo upisemo redni broj dogadjaja
	//ako se event desava 22.11.2019 u 19h i 23.11.2019. u 13h prve karte ce pored datuma da imaju oznaku 1, a druge oznaku 2 i td..
	//dodato je u generisanju karata, eventualno da se lista vremena(datuma) sortira u rastucem rodosledu mada mislim da je to atuomatski...
	
	@Column(name = "seat_mark", nullable = false)
	private String seatMark;
	
	@Column(name = "price", nullable = false)
	private Double price;
	
	@Column(name = "category_name", nullable = false)
	private String categoryName;
	
	@Column(name = "bought", nullable = false)
	private Boolean bought;
	
	@Version
	private Long version;
	
	public Ticket() {
		
	}
	
	public Ticket(User user, Event event, Date dateAndTime, int term, String seatMark, Double price, String categoryName, Boolean bought) {
		super();
		ID = UUID.randomUUID();
		this.user = user;
		this.event = event;
		this.dateAndTime = dateAndTime;
		this.term = term;
		this.seatMark = seatMark;
		this.price = price;
		this.categoryName = categoryName;
		this.bought = bought;
	}

	public UUID getID() {
		return ID;
	}

	public void setID(UUID iD) {
		ID = iD;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Date getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(Date dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public int getTerm() {
		return term;
	}

	public void setTerm(int term) {
		this.term = term;
	}
	
	public String getSeatMark() {
		return seatMark;
	}

	public void setSeatMark(String seatMark) {
		this.seatMark = seatMark;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Boolean getBought() {
		return bought;
	}

	public void setBought(Boolean bought) {
		this.bought = bought;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

}
