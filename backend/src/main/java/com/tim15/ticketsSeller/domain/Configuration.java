package com.tim15.ticketsSeller.domain;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

@Document(collection = "configurations")
public class Configuration {
	
	@Id
	private int id;
	private String name;
	private ArrayList<String> layout;
	private ArrayList<Integer> parterCapacities;
	
	public Configuration(int id, String name, ArrayList<String> layout, ArrayList<Integer> parterCapacities) throws IllegalArgumentException {
		
		if(id < 0)
			throw new IllegalArgumentException("Id must be greater then 0."); //0 for test
		
		if(name == null || name.trim().length() == 0)
			throw new IllegalArgumentException("Name must exist.");
		
		if(layout == null)
			throw new IllegalArgumentException("Layout must exist.");
		
		if(parterCapacities == null)
			throw new IllegalArgumentException("Capacity for non numerable spots must exists.");
		
		if(!checkConfiguration(layout, parterCapacities))
			throw new IllegalArgumentException("Apperence of parters and parters capacities must be equals");
		
		
		this.id = id;
		this.name = name;
		this.layout = layout;
		this.parterCapacities = parterCapacities;
	}
	public Configuration() {
		
	}
	
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public ArrayList<String> getLayout() {
		return layout;
	}
	public ArrayList<Integer> getParterCapacities() {
		return parterCapacities;
	}
	
	
	public void setId(int id) throws IllegalArgumentException {
		if(id < 0)
			throw new IllegalArgumentException("Id must be greater then 0."); //0 for test
		this.id = id;
	}
	
	public void setName(String name) throws IllegalArgumentException {
		if(name == null || name.trim().length() == 0)
			throw new IllegalArgumentException("Name must exist.");
		this.name = name;
	}
	
	public void setLayout(ArrayList<String> layout) throws IllegalArgumentException {
		if(layout == null)
			throw new IllegalArgumentException("Layout must exist.");
		
		if(!checkConfiguration(layout, this.parterCapacities))
			throw new IllegalArgumentException("Apperence of parters and parters capacities must be equals");
		
		this.layout = layout;
	}
	
	public void setParterCapacities(ArrayList<Integer> parterCapacities) {
		if(parterCapacities == null)
			throw new IllegalArgumentException("Capacity for non numerable spots must exists.");
		
		if(!checkConfiguration(this.layout, parterCapacities))
			throw new IllegalArgumentException("Apperence of parters and parters capacities must be equals");
		
		this.parterCapacities = parterCapacities;
	}
	
	
	private static boolean checkConfiguration(ArrayList<String> layout, ArrayList<Integer> pCapacity) {
		
		int countP = 0;

		for(String line : layout){  
			countP = countP + StringUtils.countOccurrencesOf(line.toLowerCase(), "p"); 
		}
		
		if(pCapacity.size() == countP)
			return true;
		
		return false;
	}
	
	
}
