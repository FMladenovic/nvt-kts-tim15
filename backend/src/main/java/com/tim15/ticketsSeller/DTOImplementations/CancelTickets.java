package com.tim15.ticketsSeller.DTOImplementations;

import java.util.UUID;

public class CancelTickets {
	
	String username;
	
	UUID ticketIds;

	public CancelTickets(String username, UUID ticketIds) {
		super();
		this.username = username;
		this.ticketIds = ticketIds;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UUID getTicketIds() {
		return ticketIds;
	}

	public void setTicketIds(UUID ticketIds) {
		this.ticketIds = ticketIds;
	}

}
