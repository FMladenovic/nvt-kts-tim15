package com.tim15.ticketsSeller.DTOImplementations;

import java.util.List;

import com.tim15.ticketsSeller.DTO.INewCategories;
import com.tim15.ticketsSeller.domain.Category;

public class NewCategories implements INewCategories {

	private List<String> layout;

	private List<Integer> parterCapacities;
	
	private List<Category> categories;
	
	public NewCategories() {
	}
	
	public NewCategories(List<String> layout, List<Integer> parterCapacities, List<Category> categories) {
		super();
		this.layout = layout;
		this.parterCapacities = parterCapacities;
		this.categories = categories;
	}
	
	public List<String> getLayout() {
		return layout;
	}
	public List<Integer> getParterCapacities() {
		return parterCapacities;
	}
	public List<Category> getCategories() {
		return categories;
	}

}
