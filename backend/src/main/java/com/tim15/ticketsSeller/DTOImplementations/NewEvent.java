package com.tim15.ticketsSeller.DTOImplementations;

import java.util.UUID;

import com.tim15.ticketsSeller.DTO.INewEvent;
import com.tim15.ticketsSeller.domain.Event;

public class NewEvent implements INewEvent {

	private String name;
	
	private String description;
	
	//private List<Ticket> tickets;
	
	private int ticketsLimit;
	
	private UUID locationId;

	private UUID categoriesId;
	
	public NewEvent(Event event) {
		this(event.getName(), event.getDescription(), event.getTicketsLimit(), event.getLocationId(), event.getCategoriesId());
	}
	
	
	
	public NewEvent(String name, String description, int ticketsLimit, UUID locationId, UUID categoriesId) {
		super();
		this.name = name;
		this.description = description;
		this.ticketsLimit = ticketsLimit;
		this.locationId = locationId;
		this.categoriesId = categoriesId;
	}



	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public int getTicketsLimit() {
		return ticketsLimit;
	}

	public UUID getLocationId() {
		return locationId;
	}

	public UUID getCategoriesId() {
		return categoriesId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setTicketsLimit(int ticketsLimit) {
		this.ticketsLimit = ticketsLimit;
	}

	public void setLocationId(UUID locationId) {
		this.locationId = locationId;
	}

	public void setCategoriesId(UUID categoriesId) {
		this.categoriesId = categoriesId;
	}


}
