package com.tim15.ticketsSeller.DTOImplementations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.tim15.ticketsSeller.DTO.ITicketList;
import com.tim15.ticketsSeller.domain.Categories;

public class TicketList implements ITicketList {

	
	private Categories categories;
	private Map<String, UUID> ticketsIds;
	private List<Integer> availableParterTicket;
	private int ticketsLimit;
	
	public TicketList() {
		this.ticketsIds = new HashMap<String, UUID>();
		this.availableParterTicket = new ArrayList<Integer>();
	}
	
	public TicketList(Categories categories, List<Integer> availableParterTicket, int ticketsLimit) {
		this.ticketsIds = new HashMap<String, UUID>();
		this.availableParterTicket = availableParterTicket;
		this.categories = categories;
		this.ticketsLimit = ticketsLimit;
	}

	
	@Override
	public Categories getCategories() {
		// TODO Auto-generated method stub
		return categories;
	}

	@Override
	public Map<String, UUID> getTicketsIds() {
		// TODO Auto-generated method stub
		return ticketsIds;
	}

	@Override
	public int getTicketsLimit() {
		return ticketsLimit;
	}

	public List<Integer> getAvailableParterTicket(){
		return availableParterTicket;
	}

	
}
