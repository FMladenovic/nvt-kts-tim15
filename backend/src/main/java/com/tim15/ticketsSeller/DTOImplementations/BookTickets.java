package com.tim15.ticketsSeller.DTOImplementations;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class BookTickets {
	
	UUID eventId;
	String username;
	List<Date> terms;
	List<String> seats;
	
	public BookTickets(UUID eventId, String username, List<Date> terms, List<String> seats) {
		super();
		this.eventId = eventId;
		this.username = username;
		this.terms = terms;
		this.seats = seats;
	}

	public UUID getEventId() {
		return eventId;
	}

	public void setEventId(UUID eventId) {
		this.eventId = eventId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Date> getTerms() {
		return terms;
	}

	public void setTerms(List<Date> terms) {
		this.terms = terms;
	}

	public List<String> getSeats() {
		return seats;
	}

	public void setSeats(List<String> seats) {
		this.seats = seats;
	}

}
