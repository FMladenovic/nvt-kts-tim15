package com.tim15.ticketsSeller.DTOImplementations;

import com.tim15.ticketsSeller.DTO.IJwtResponse;

public class JwtResponse implements IJwtResponse {

    private String value;

    public JwtResponse() {
    }

    public JwtResponse(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }
}
