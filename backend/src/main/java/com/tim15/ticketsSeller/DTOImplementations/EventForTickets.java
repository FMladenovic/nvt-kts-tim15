package com.tim15.ticketsSeller.DTOImplementations;

import java.util.UUID;

public class EventForTickets {
	
	UUID id;
	String eventName;
	
	
	public EventForTickets() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EventForTickets(UUID id, String eventName) {
		super();
		this.id = id;
		this.eventName = eventName;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

}
