package com.tim15.ticketsSeller.DTOImplementations;

import java.util.List;
import java.util.UUID;

import com.tim15.ticketsSeller.DTO.IReservation;

public class Reservation implements IReservation {

	private List<Integer> parterTickets;
	private List<UUID> ticketIds;
	
	public Reservation() {
		
	}

	@Override
	public List<Integer> getParterTickets() {
		return parterTickets;
	}

	@Override
	public List<UUID> getTicketIds() {
		return ticketIds;
	}

}
