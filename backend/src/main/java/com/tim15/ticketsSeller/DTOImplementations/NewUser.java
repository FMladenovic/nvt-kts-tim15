package com.tim15.ticketsSeller.DTOImplementations;

import com.tim15.ticketsSeller.DTO.INewUser;
import com.tim15.ticketsSeller.enumeration.Role;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static com.tim15.ticketsSeller.enumeration.Role.ROLE_USER;

public class NewUser implements INewUser {

    @NotBlank
    private String username;
    @NotBlank
    @Size(min = 8)
    private String password;
    @NotBlank
    @Size(min = 8)
    private String confirmPassword;
    @NotBlank
    private String name;
    @NotBlank
    private String surname;
    @NotBlank
    @Email
    private String email;

    private Role role = ROLE_USER;

    @AssertTrue
    public boolean isConfirmedPasswordValid() {
        if (password != null){
            return password.equals(confirmPassword);
        }
        return true;
    }

    public NewUser() {
    }

    public NewUser(String username, String password, String confirmPassword, String name,
                   String surname, String email) {
        this.username = username;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.name = name;
        this.surname = surname;
        this.email = email;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getConfirmPassword() {
        return confirmPassword;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public Role getRole() {
        return role;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }
}
