package com.tim15.ticketsSeller.DTOImplementations;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UpdateUser {

    @NotBlank
    private String oldPassword;

    @Size(min = 8)
    private String password;

    @Size(min = 8)
    private String confirmPassword;

    private String name;

    private String surname;

    @AssertTrue
    public boolean isConfirmedPasswordValid() {
        return (password == null || confirmPassword == null) || password.equals(confirmPassword);
    }

    @AssertTrue
    public boolean isNewPasswordValid() {
        return (password == null) || !password.equals(oldPassword);
    }

    @AssertTrue
    public boolean isConfirmPasswordEnteredValid() {
        return (password == null) || confirmPassword != null;
    }

    public UpdateUser() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
