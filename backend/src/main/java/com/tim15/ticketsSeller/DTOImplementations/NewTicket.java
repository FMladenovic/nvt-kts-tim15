package com.tim15.ticketsSeller.DTOImplementations;

import java.util.Date;
import java.util.List;

public class NewTicket {
	
	private String eventName;
	
	private String locationName;
	
	private List<Date> datesAndTimes;
	
	private List<String> seatsAndParterreSpots;

	public NewTicket(String eventName, String locationName, List<Date> datesAndTimes, List<String> seatsAndParterreSpots) {
		super();
		this.eventName = eventName;
		this.locationName = locationName;
		this.datesAndTimes = datesAndTimes;
		this.seatsAndParterreSpots = seatsAndParterreSpots;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public List<Date> getDatesAndTimes() {
		return datesAndTimes;
	}

	public void setDatesAndTimes(List<Date> datesAndTimes) {
		this.datesAndTimes = datesAndTimes;
	}

	public List<String> getSeatsAndParterreSpots() {
		return seatsAndParterreSpots;
	}

	public void setSeatsAndParterreSpots(List<String> seatsAndParterreSpots) {
		this.seatsAndParterreSpots = seatsAndParterreSpots;
	}
	
	

}
