package com.tim15.ticketsSeller.DTOImplementations;

import javax.validation.constraints.NotBlank;

import com.tim15.ticketsSeller.DTO.INewLocation;

public class NewLocation implements INewLocation {

    @NotBlank
	private String name;
    @NotBlank
	private int configurationId;
    @NotBlank
	private boolean active;
	
	public String getName() {
		return name;
	}
	public int getConfigurationId() {
		return configurationId;
	}
	public boolean isActive() {
		return active;
	}

	public NewLocation() {
		
	}
	
	public NewLocation(String name, int configurationId, boolean active) {
		this.name = name;
		this.configurationId = configurationId;
		this.active = active;
	}
}
