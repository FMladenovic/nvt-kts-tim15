package com.tim15.ticketsSeller.DTOImplementations;

import com.tim15.ticketsSeller.DTO.IUserCredentials;

import javax.validation.constraints.NotBlank;

public class UserCredentials implements IUserCredentials {

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    public UserCredentials(){}

    public UserCredentials(String username, String password){
        this.username = username;
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }
}