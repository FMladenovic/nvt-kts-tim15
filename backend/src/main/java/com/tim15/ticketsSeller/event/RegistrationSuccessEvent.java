package com.tim15.ticketsSeller.event;

import com.tim15.ticketsSeller.domain.User;
import org.springframework.context.ApplicationEvent;

public class RegistrationSuccessEvent extends ApplicationEvent {

    private User user;

    public RegistrationSuccessEvent(User user) {
        super(user);
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
