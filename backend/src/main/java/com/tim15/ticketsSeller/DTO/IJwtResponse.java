package com.tim15.ticketsSeller.DTO;

public interface IJwtResponse {

    String getValue();
}
