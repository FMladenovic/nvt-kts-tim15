package com.tim15.ticketsSeller.DTO;

public interface INewLocation {
	
	String getName();
	int getConfigurationId();
	boolean isActive();
}
