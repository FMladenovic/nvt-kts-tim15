package com.tim15.ticketsSeller.DTO;

public interface IUpdateUser {

    String getOldPassword();

    void setOldPassword(String oldPassword);

    String getPassword();

    void setPassword(String password);

    String getConfirmPassword();

    void setConfirmPassword(String confirmPassword);

    String getName();

    void setName(String name);

    String getSurname();

    void setSurname(String surname);
}
