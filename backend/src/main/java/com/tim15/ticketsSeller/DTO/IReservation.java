package com.tim15.ticketsSeller.DTO;

import java.util.List;
import java.util.UUID;

public interface IReservation {
	
	List<Integer> getParterTickets();
    List<UUID> getTicketIds();

}
