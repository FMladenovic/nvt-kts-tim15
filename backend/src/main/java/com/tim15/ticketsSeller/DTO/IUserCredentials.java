package com.tim15.ticketsSeller.DTO;

public interface IUserCredentials {

    String getUsername();

    void setUsername(String username);

    String getPassword();

    void setPassword(String password);
}