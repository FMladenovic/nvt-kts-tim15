package com.tim15.ticketsSeller.DTO;

public interface IUserDetails {

    String getName();

    void setName(String name);

    String getSurname();

    void setSurname(String surname);

    String getUsername();

    void setUsername(String username);

    String getEmail();

    void setEmail(String email);

    String getImagePath();

    void setImagePath(String imagePath);
}
