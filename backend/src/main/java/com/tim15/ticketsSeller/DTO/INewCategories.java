package com.tim15.ticketsSeller.DTO;

import java.util.List;

import com.tim15.ticketsSeller.domain.Category;

public interface INewCategories {
	
	public List<String> getLayout();
	public List<Integer> getParterCapacities();
	public List<Category> getCategories();
}
