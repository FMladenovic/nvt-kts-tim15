package com.tim15.ticketsSeller.DTO;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.tim15.ticketsSeller.domain.Categories;

public interface ITicketList {
	
	Categories getCategories();
	Map<String, UUID> getTicketsIds();
	List<Integer> getAvailableParterTicket();
	int getTicketsLimit();

}
