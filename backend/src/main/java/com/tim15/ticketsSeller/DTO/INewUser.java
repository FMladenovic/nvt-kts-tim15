package com.tim15.ticketsSeller.DTO;

import com.tim15.ticketsSeller.enumeration.Role;

public interface INewUser {

    String getUsername();

    String getPassword();

    String getConfirmPassword();

    String getName();

    String getSurname();

    String getEmail();

    Role getRole();

    void setPassword(String password);

    void setConfirmPassword(String confirmPassword);

    void setName(String name);

    void setSurname(String surname);

    void setEmail(String email);

    void setRole(Role role);

    void setUsername(String username);
}
