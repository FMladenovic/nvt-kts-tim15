package com.tim15.ticketsSeller.DTO;

import java.util.UUID;

public interface INewEvent {
	
	public String getName();
	
	public String getDescription();
	
	//private List<Ticket> tickets;
	
	public int getTicketsLimit();

	public UUID getLocationId();
	
	public UUID getCategoriesId();

}
