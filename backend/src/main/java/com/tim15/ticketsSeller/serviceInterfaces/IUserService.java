package com.tim15.ticketsSeller.serviceInterfaces;

import com.tim15.ticketsSeller.DTOImplementations.NewUser;
import com.tim15.ticketsSeller.DTOImplementations.UpdateUser;
import com.tim15.ticketsSeller.DTOImplementations.UserCredentials;
import com.tim15.ticketsSeller.DTOImplementations.UserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface IUserService {

    String login(@Valid UserCredentials userCredentials);

    NewUser registerUser(@Valid NewUser newUser, MultipartFile image);

    void confirmUserAccount(@NotNull String token);

    UpdateUser updateUser(@Valid UpdateUser updatedUser, MultipartFile image, Authentication authentication);

    UserDetails getLoggedUser(Authentication authentication);
}
