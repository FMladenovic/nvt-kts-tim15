package com.tim15.ticketsSeller.serviceInterfaces;

import java.util.List;
import java.util.UUID;

import com.tim15.ticketsSeller.DTOImplementations.BookTickets;
import com.tim15.ticketsSeller.DTOImplementations.CancelTickets;
import com.tim15.ticketsSeller.domain.Ticket;
import com.tim15.ticketsSeller.domain.User;
import com.tim15.ticketsSeller.exception.customExceptions.EventUndefinedException;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidTicketOwnerException;
import com.tim15.ticketsSeller.exception.customExceptions.TermsUndefinedInEventException;
import com.tim15.ticketsSeller.exception.customExceptions.TicketPaidException;
import com.tim15.ticketsSeller.exception.customExceptions.TicketsAlreadyGeneratedException;
import com.tim15.ticketsSeller.exception.customExceptions.TicketsLimitNumberException;
import com.tim15.ticketsSeller.exception.customExceptions.TicketsNotAvailableException;
import com.tim15.ticketsSeller.exception.customExceptions.UnexistingTicketException;
import com.tim15.ticketsSeller.exception.customExceptions.ReservationPeriodExpiredException;

public interface ITicketService {	
	
	Boolean generateTicketsForEvent(UUID eventId) throws EventUndefinedException, TicketsAlreadyGeneratedException;
	
	List<Ticket> bookTickets(BookTickets bookTicketsDTO) throws EventUndefinedException, TermsUndefinedInEventException, ReservationPeriodExpiredException, TicketsLimitNumberException, TicketsNotAvailableException;
	
	Ticket cancelTicket(CancelTickets cancelTiDTO) throws TicketPaidException, UnexistingTicketException, InvalidTicketOwnerException;
	
	Boolean payTickets(List<Ticket> tickets, User user);

}
