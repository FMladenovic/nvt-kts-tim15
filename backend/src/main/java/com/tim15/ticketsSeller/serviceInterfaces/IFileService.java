package com.tim15.ticketsSeller.serviceInterfaces;

import org.springframework.web.multipart.MultipartFile;

public interface IFileService {

    String uploadFile(MultipartFile file);
}
