package com.tim15.ticketsSeller.serviceInterfaces;

import java.util.NoSuchElementException;
import java.util.UUID;

import com.tim15.ticketsSeller.DTO.INewLocation;
import com.tim15.ticketsSeller.domain.Location;
import com.tim15.ticketsSeller.exception.customExceptions.ForeignKeyException;
import com.tim15.ticketsSeller.exception.customExceptions.UniqueViolationException;

public interface ILocationService {

	Iterable<Location> getAll();
	
	Location getById(UUID id);
	
	UUID post(INewLocation location) throws IllegalArgumentException, NoSuchElementException, UniqueViolationException;
	
	void put(UUID id, INewLocation location) throws IllegalArgumentException, NoSuchElementException, UniqueViolationException;
	
	boolean delete(UUID id) throws ForeignKeyException;
	
}
