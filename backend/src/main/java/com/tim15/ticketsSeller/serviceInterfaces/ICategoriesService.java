package com.tim15.ticketsSeller.serviceInterfaces;

import java.io.IOException;
import java.util.UUID;

import com.google.gson.JsonIOException;
import com.tim15.ticketsSeller.DTO.INewCategories;
import com.tim15.ticketsSeller.domain.Categories;

public interface ICategoriesService {
	
	Iterable<Categories> getAll();
	
	Categories getById(UUID id);
	
	UUID post(INewCategories categories) throws IllegalArgumentException, JsonIOException, IOException;
	
	void delete(UUID id) throws JsonIOException, IOException;

}
