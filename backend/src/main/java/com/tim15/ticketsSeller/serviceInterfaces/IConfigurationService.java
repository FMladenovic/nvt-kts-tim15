package com.tim15.ticketsSeller.serviceInterfaces;

import com.tim15.ticketsSeller.domain.Configuration;

public interface IConfigurationService {
	
	public Iterable<Configuration>  get();
	public Configuration getById(Integer id);
}