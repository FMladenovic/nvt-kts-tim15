package com.tim15.ticketsSeller.serviceInterfaces;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.tim15.ticketsSeller.DTO.INewEvent;
import com.tim15.ticketsSeller.domain.Event;

public interface IEventService {

	
	Iterable<Event> getAll();
	
	Event getById(UUID id);
	
	UUID post(INewEvent event); //Change to IEventDTO
	
	void put(UUID id, INewEvent event); //Change to IEventDTO
	
	void delete(UUID id);

	Page<Event> getAllPageable(Pageable pageable);
	
	
	
}
