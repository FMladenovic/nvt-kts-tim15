package com.tim15.ticketsSeller.config;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Mapper {
    private final MapperFacade mapperFacade;

    public Mapper() {
        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();

        this.mapperFacade = mapperFactory.getMapperFacade();
    }

    public <S, D> D map(S source, Class<D> destination) {
        return mapperFacade.map(source, destination);
    }

    public <S, D> void map(S source, D destination) {
        mapperFacade.map(source, destination);
    }
}
