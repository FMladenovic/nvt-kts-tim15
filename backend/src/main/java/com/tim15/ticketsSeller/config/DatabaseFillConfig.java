package com.tim15.ticketsSeller.config;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.tim15.ticketsSeller.domain.Event;
import com.tim15.ticketsSeller.domain.Ticket;
import com.tim15.ticketsSeller.repositoryInterfaces.EventRepository;

@Component
public class DatabaseFillConfig implements ApplicationListener<ApplicationReadyEvent> {
	
	@Autowired
	EventRepository eventRepo;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		persistEventInDB();
		
	}
	
	public void persistEventInDB() {
		
		List<Date> terms = new ArrayList<Date>();
		
		 SimpleDateFormat dateformat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		 String strdate1 = "07-01-2020 19:35:00";
		 String strdate2 = "08-01-2020 18:35:00";
		 String strdate3 = "09-01-2020 17:35:00";
		 String strdate4 = "25-12-2020 17:40:00";
		 
		 
		 try {
			Date term1 = dateformat.parse(strdate1);
			Date term2 = dateformat.parse(strdate2);
			Date term3 = dateformat.parse(strdate3);
			terms.add(term1);
			terms.add(term2);
			terms.add(term3);
			
			Date term4 = dateformat.parse(strdate4);
			
			Event e = new Event(UUID.fromString("69ae1cb4-4b1c-4487-8cb0-ee7f5ae68581"), "KoncertZC", "Koncert Zdravka Colica", term4, terms,
					new ArrayList<Ticket>(), 5, null, UUID.fromString("22032b8e-460f-4627-8106-fd2fecc8a3de"));
			
			eventRepo.save(e);
		 }catch(ParseException e) {
			 e.printStackTrace();
		 }
	}
}
