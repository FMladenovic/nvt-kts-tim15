package com.tim15.ticketsSeller.exception.customExceptions;

public class UniqueViolationException extends Exception {

	private static final long serialVersionUID = 1L;

	public UniqueViolationException() {
		super();
	}

	public UniqueViolationException(String message, Throwable arg1, boolean arg2, boolean arg3) {
		super(message, arg1, arg2, arg3);
	}

	public UniqueViolationException(String message, Throwable arg1) {
		super(message, arg1);
	}

	public UniqueViolationException(String message) {
		super(message);
	}

	public UniqueViolationException(Throwable arg0) {
		super(arg0);
	}

	
}
