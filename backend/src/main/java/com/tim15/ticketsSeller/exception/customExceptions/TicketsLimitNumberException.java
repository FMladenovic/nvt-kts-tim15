package com.tim15.ticketsSeller.exception.customExceptions;

public class TicketsLimitNumberException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TicketsLimitNumberException(String message, Throwable cause) {
		super(message, cause);
	}

	public TicketsLimitNumberException(String message) {
		super(message);
	}

	public TicketsLimitNumberException(Throwable cause) {
		super(cause);
	}
}
