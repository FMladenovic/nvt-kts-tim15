package com.tim15.ticketsSeller.exception;

import com.tim15.ticketsSeller.exception.customExceptions.ForeignKeyException;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidLoginException;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidPasswordException;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidTokenException;
import com.tim15.ticketsSeller.exception.customExceptions.UniqueViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedCredentialsNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.util.NoSuchElementException;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity<ErrorInfo> handleIllegalArgumentException(HttpServletRequest req, IllegalArgumentException ex) {
        return new ResponseEntity<>(new ErrorInfo(req.getRequestURL().toString(), ex), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoSuchElementException.class)
    ResponseEntity<ErrorInfo> handleNoSuchElementException(HttpServletRequest req, NoSuchElementException ex) {
        return new ResponseEntity<>(new ErrorInfo(req.getRequestURL().toString(), ex), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UniqueViolationException.class)
    ResponseEntity<ErrorInfo> handleUniqueViolationException(HttpServletRequest req, UniqueViolationException ex) {
        return new ResponseEntity<>(new ErrorInfo(req.getRequestURL().toString(), ex), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ForeignKeyException.class)
    ResponseEntity<ErrorInfo> handleForeignKeyException(HttpServletRequest req, ForeignKeyException ex) {
        return new ResponseEntity<>(new ErrorInfo(req.getRequestURL().toString(), ex), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(InvalidTokenException.class)
    ResponseEntity<ErrorInfo> handleInvalidTokenException(HttpServletRequest req, InvalidTokenException ex) {
        return new ResponseEntity<>(new ErrorInfo(req.getRequestURL().toString(), ex), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    ResponseEntity<ErrorInfo> handleConstraintViolationException(HttpServletRequest request, ConstraintViolationException exception) {
        return ResponseEntity.unprocessableEntity().body(new ErrorInfo(request.getRequestURL().toString(), exception));
    }

    @ExceptionHandler(EntityNotFoundException.class)
    ResponseEntity<ErrorInfo> handleEntityNotFoundException(HttpServletRequest req, EntityNotFoundException ex) {
        return new ResponseEntity<>(new ErrorInfo(req.getRequestURL().toString(), ex), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PreAuthenticatedCredentialsNotFoundException.class)
    ResponseEntity<ErrorInfo> handlePreAuthenticatedCredentialsNotFoundException(HttpServletRequest req, PreAuthenticatedCredentialsNotFoundException ex) {
        return new ResponseEntity<>(new ErrorInfo(req.getRequestURL().toString(), ex), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    ResponseEntity<ErrorInfo> handleDataIntegrityViolationException(HttpServletRequest req, DataIntegrityViolationException ex) {
        return new ResponseEntity<>(new ErrorInfo(req.getRequestURL().toString(), ex), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(InvalidLoginException.class)
    ResponseEntity<ErrorInfo> handleInvalidLoginException(HttpServletRequest req, InvalidLoginException ex) {
        return new ResponseEntity<>(new ErrorInfo(req.getRequestURL().toString(), ex), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(InvalidPasswordException.class)
    ResponseEntity<ErrorInfo> handleInvalidLoginException(HttpServletRequest req, InvalidPasswordException ex) {
        return new ResponseEntity<>(new ErrorInfo(req.getRequestURL().toString(), ex), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorInfo> handleAllExceptions(HttpServletRequest req, Exception ex) {
        return new ResponseEntity<>(new ErrorInfo(req.getRequestURL().toString(), ex), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}