package com.tim15.ticketsSeller.exception.customExceptions;

public class TermsUndefinedInEventException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TermsUndefinedInEventException(String message, Throwable cause) {
		super(message, cause);
	}

	public TermsUndefinedInEventException(String message) {
		super(message);
	}

	public TermsUndefinedInEventException(Throwable cause) {
		super(cause);
	}
}
