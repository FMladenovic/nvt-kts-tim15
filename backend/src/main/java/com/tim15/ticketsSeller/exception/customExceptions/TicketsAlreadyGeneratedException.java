package com.tim15.ticketsSeller.exception.customExceptions;

public class TicketsAlreadyGeneratedException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TicketsAlreadyGeneratedException(String message, Throwable cause) {
		super(message, cause);
	}

	public TicketsAlreadyGeneratedException(String message) {
		super(message);
	}

	public TicketsAlreadyGeneratedException(Throwable cause) {
		super(cause);
	}

}
