package com.tim15.ticketsSeller.exception.customExceptions;

public class ForeignKeyException extends Exception {

	private static final long serialVersionUID = 1L;

	public ForeignKeyException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ForeignKeyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ForeignKeyException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ForeignKeyException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ForeignKeyException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
