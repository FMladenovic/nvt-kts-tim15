package com.tim15.ticketsSeller.exception.customExceptions;

public class InvalidTicketOwnerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidTicketOwnerException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidTicketOwnerException(String message) {
		super(message);
	}

	public InvalidTicketOwnerException(Throwable cause) {
		super(cause);
	}
	
}
