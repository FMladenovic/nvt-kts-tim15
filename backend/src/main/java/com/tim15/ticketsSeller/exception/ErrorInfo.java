package com.tim15.ticketsSeller.exception;
public class ErrorInfo {
    public final String path;
    public final String error;
    public final String message;
    

    public ErrorInfo(String path, Exception ex) {
        this.path = path;
        this.error = ex.getLocalizedMessage();
        this.message = ex.getMessage();
    }
}