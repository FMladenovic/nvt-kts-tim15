package com.tim15.ticketsSeller.exception.customExceptions;

public class TicketsNotAvailableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TicketsNotAvailableException(String message, Throwable cause) {
		super(message, cause);
	}

	public TicketsNotAvailableException(String message) {
		super(message);
	}

	public TicketsNotAvailableException(Throwable cause) {
		super(cause);
	}
}
