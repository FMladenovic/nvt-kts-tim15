package com.tim15.ticketsSeller.exception.customExceptions;

public class TicketPaidException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TicketPaidException(String message, Throwable cause) {
		super(message, cause);
	}

	public TicketPaidException(String message) {
		super(message);
	}

	public TicketPaidException(Throwable cause) {
		super(cause);
	}
}
