package com.tim15.ticketsSeller.exception.customExceptions;

public class ReservationPeriodExpiredException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ReservationPeriodExpiredException(String message, Throwable cause) {
		super(message, cause);
	}

	public ReservationPeriodExpiredException(String message) {
		super(message);
	}

	public ReservationPeriodExpiredException(Throwable cause) {
		super(cause);
	}
}
