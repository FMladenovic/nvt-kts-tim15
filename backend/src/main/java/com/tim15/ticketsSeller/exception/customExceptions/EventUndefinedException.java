package com.tim15.ticketsSeller.exception.customExceptions;

public class EventUndefinedException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EventUndefinedException(String message, Throwable cause) {
		super(message, cause);
	}

	public EventUndefinedException(String message) {
		super(message);
	}

	public EventUndefinedException(Throwable cause) {
		super(cause);
	}

}
