package com.tim15.ticketsSeller.exception.customExceptions;

public class UnexistingTicketException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnexistingTicketException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnexistingTicketException(String message) {
		super(message);
	}

	public UnexistingTicketException(Throwable cause) {
		super(cause);
	}
	
}
