package com.tim15.ticketsSeller.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.TestPropertySource;

@Configuration
@Profile("test")
@TestPropertySource(locations = "classpath:application-test.properties")
public class TestConfig {
}