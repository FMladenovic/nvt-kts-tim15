package com.tim15.ticketsSeller.controllers;


import com.tim15.ticketsSeller.DTOImplementations.JwtResponse;
import com.tim15.ticketsSeller.DTOImplementations.NewUser;
import com.tim15.ticketsSeller.DTOImplementations.UserCredentials;
import com.tim15.ticketsSeller.config.TestConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Import(TestConfig.class)
public class UserControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @LocalServerPort
    private int port;

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/api/user" + uri;
    }

    private NewUser mockNewUser;

    private HttpHeaders mockHeaders;

    private FileSystemResource mockFileSystemResource;

    private MultiValueMap<String, Object> mockParameters;

    private Set<String> initialFiles;

    @Before
    public void setUp() throws IOException {

        mockNewUser = new NewUser("username", "password", "password",
                "name", "surname", "email@ex.ex");

        mockHeaders = new HttpHeaders();
        mockHeaders.set("Content-Type", "multipart/form-data");
        mockHeaders.set("Accept", "application/json");

        mockFileSystemResource =
                new FileSystemResource("src\\test\\resources\\static\\images\\user.png");

        mockParameters = new LinkedMultiValueMap<>();

        initialFiles = getAllFiles();
    }

    @After
    public void tearDown() throws IOException {
        getAllFiles().forEach(path -> {
            File file = new File(path);
            if (file.exists() && !initialFiles.contains(path)) {
                file.delete();
            }
        });
    }

    private Set<String> getAllFiles() {
        Set<String> allFiles = new HashSet<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get("src\\test\\resources\\static\\images\\"))) {
            stream.forEach(path -> {
                if (!Files.isDirectory(path)) {
                    allFiles.add(path.toString());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return allFiles;
    }

    @Test
    public void login_ValidRequest_ShouldLoginUser() {
        UserCredentials userCredentials = new UserCredentials("user", "user");

        ResponseEntity<JwtResponse> responseEntity =
                testRestTemplate.postForEntity(this.createURLWithPort("/login"), userCredentials, JwtResponse.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(Objects.requireNonNull(responseEntity.getBody()).getValue().contains("Bearer"));
    }

    @Test
    public void login_UsernameNotProvided_ShouldResultInUnProcessableEntityStatus() {
        UserCredentials userCredentials = new UserCredentials(null, "user");

        ResponseEntity<JwtResponse> responseEntity =
                testRestTemplate.postForEntity(this.createURLWithPort("/login"), userCredentials, JwtResponse.class);

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
    }

    @Test
    public void login_PasswordNotProvided_ShouldResultInUnProcessableEntityStatus() {
        UserCredentials userCredentials = new UserCredentials("user", null);

        ResponseEntity<JwtResponse> responseEntity =
                testRestTemplate.postForEntity(this.createURLWithPort("/login"), userCredentials, JwtResponse.class);

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
    }

    @Test
    public void registerUser_ValidRequest_ShouldResultInCreatedStatus() {
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockNewUser);

        ResponseEntity<NewUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort("/register"),
                HttpMethod.POST, new HttpEntity<>(mockParameters, mockHeaders), NewUser.class);

        NewUser createdUser = responseEntity.getBody();

        assertThat(createdUser, hasProperty("username", is(mockNewUser.getUsername())));
        assertThat(createdUser, hasProperty("password", is(mockNewUser.getPassword())));
        assertThat(createdUser, hasProperty("name", is(mockNewUser.getName())));
        assertThat(createdUser, hasProperty("surname", is(mockNewUser.getSurname())));
        assertThat(createdUser, hasProperty("email", is(mockNewUser.getEmail())));
        assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.CREATED));
    }

    @Test
    public void registerUser_UsernameAlreadyExists_ShouldResultInConflictStatus() {
        mockNewUser.setUsername("admin");
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockNewUser);

        ResponseEntity<NewUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort("/register"),
                HttpMethod.POST, new HttpEntity<>(mockParameters, mockHeaders), NewUser.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.CONFLICT));
    }

    @Test
    public void registerUser_BlankUsername_ShouldResultInUnProcessableEntityStatus() {
        mockNewUser.setUsername(null);
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockNewUser);

        ResponseEntity<NewUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort("/register"),
                HttpMethod.POST, new HttpEntity<>(mockParameters, mockHeaders), NewUser.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void registerUser_EmailAlreadyExists_ShouldResultInConflictStatus() {
        mockNewUser.setEmail("admin@ex.ex");
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockNewUser);

        ResponseEntity<NewUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort("/register"),
                HttpMethod.POST, new HttpEntity<>(mockParameters, mockHeaders), NewUser.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.CONFLICT));
    }

    @Test
    public void registerUser_BadFormedEmail_ShouldResultInUnProcessableEntityStatus() {
        mockNewUser.setEmail("adminex.ex");
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockNewUser);

        ResponseEntity<NewUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort("/register"),
                HttpMethod.POST, new HttpEntity<>(mockParameters, mockHeaders), NewUser.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void registerUser_BlankEmail_ShouldResultInUnProcessableEntityStatus() {
        mockNewUser.setEmail(null);
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockNewUser);

        ResponseEntity<NewUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort("/register"),
                HttpMethod.POST, new HttpEntity<>(mockParameters, mockHeaders), NewUser.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void registerUser_BlankName_ShouldResultInUnProcessableEntityStatus() {
        mockNewUser.setName(null);
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockNewUser);

        ResponseEntity<NewUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort("/register"),
                HttpMethod.POST, new HttpEntity<>(mockParameters, mockHeaders), NewUser.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void registerUser_BlankSurname_ShouldResultInUnProcessableEntityStatus() {
        mockNewUser.setSurname(null);
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockNewUser);

        ResponseEntity<NewUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort("/register"),
                HttpMethod.POST, new HttpEntity<>(mockParameters, mockHeaders), NewUser.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void registerUser_BlankPassword_ShouldResultInUnProcessableEntityStatus() {
        mockNewUser.setPassword(null);
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockNewUser);

        ResponseEntity<NewUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort("/register"),
                HttpMethod.POST, new HttpEntity<>(mockParameters, mockHeaders), NewUser.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void registerUser_BlankConfirmPassword_ShouldResultInUnProcessableEntityStatus() {
        mockNewUser.setConfirmPassword(null);
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockNewUser);

        ResponseEntity<NewUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort("/register"),
                HttpMethod.POST, new HttpEntity<>(mockParameters, mockHeaders), NewUser.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void registerUser_ConfirmPasswordDoesNotMatchToPassword_ShouldResultInUnProcessableEntityStatus() {
        mockNewUser.setConfirmPassword("wrong password");
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockNewUser);

        ResponseEntity<NewUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort("/register"),
                HttpMethod.POST, new HttpEntity<>(mockParameters, mockHeaders), NewUser.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.UNPROCESSABLE_ENTITY));
    }

    @Test
    public void registerUser_ShortPassword_ShouldResultInUnProcessableEntityStatus() {
        mockNewUser.setPassword("short");
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockNewUser);

        ResponseEntity<NewUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort("/register"),
                HttpMethod.POST, new HttpEntity<>(mockParameters, mockHeaders), NewUser.class);

        assertThat(responseEntity.getStatusCode(), is(HttpStatus.UNPROCESSABLE_ENTITY));
    }

//    @Test
//    public void confirmUserAccount_ValidRequest_ShouldResultInNoContentStatus() {
//        ResponseEntity<Void> responseEntity =
//                testRestTemplate.getForEntity(this.createURLWithPort("/confirmAccount?token=token"), Void.class);
//
//        assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));
//    }

    @Test
    public void confirmUserAccount_TokenHasExpired_ShouldResultUnAuthorizedStatus() {
        ResponseEntity<Void> responseEntity =
                testRestTemplate.getForEntity(this.createURLWithPort("/confirmAccount?token=expired token"), Void.class);

        assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.UNAUTHORIZED));
    }

    @Test
    public void confirmUserAccount_TokenDoesNotExist_ShouldResultInNotFoundStatus() {
        ResponseEntity<Void> responseEntity =
                testRestTemplate.getForEntity(this.createURLWithPort("/confirmAccount?token=token not found"), Void.class);

        assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.NOT_FOUND));
    }

    @Test
    public void confirmUserAccount_UserInTokenIsNull_ShouldResultInBadRequestStatus() {
        ResponseEntity<Void> responseEntity =
                testRestTemplate.getForEntity(this.createURLWithPort("/confirmAccount?token=invalid token"), Void.class);

        assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }
}
