package com.tim15.ticketsSeller.controllers;

import com.google.gson.Gson;
import com.tim15.ticketsSeller.DTOImplementations.JwtResponse;
import com.tim15.ticketsSeller.DTOImplementations.UserCredentials;
import com.tim15.ticketsSeller.config.TestConfig;
import com.tim15.ticketsSeller.domain.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Import(TestConfig.class)
public class ConfigurationControllerTest {
	
	@LocalServerPort
    private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;

	
	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}

	@Before
	public void login() {
		UserCredentials userCredentials = new UserCredentials("admin", "admin");

		ResponseEntity<JwtResponse> responseEntity =
				restTemplate.postForEntity(this.createURLWithPort("/api/user/login"), userCredentials, JwtResponse.class);

		restTemplate.getRestTemplate().setInterceptors(
				Collections.singletonList((request, body, execution) -> {
					request.getHeaders()
							.add("Authorization", responseEntity.getBody().getValue());
					return execution.execute(request, body);
				}));
	}

	@Test
	public void getConfigurations() {		
		ResponseEntity<String> re = restTemplate.getForEntity(this.createURLWithPort("/api/configurations/"), String.class);
		
		Gson g = new Gson();
		Configuration[] resp = g.fromJson(re.getBody(), Configuration[].class);


		assertEquals(HttpStatus.OK, re.getStatusCode());
		assertTrue(resp.length > 0);
	}
	
	@Test
	public void getConfigurationById_whenServerContainsConfigurationWithGivenId_returnsResponseWithConfiguration() {		
		int id = 0;
		ResponseEntity<String> re = restTemplate.getForEntity(this.createURLWithPort("/api/configurations/"+id), String.class);
		
		Gson g = new Gson();
		Configuration resp = g.fromJson(re.getBody(), Configuration.class);


		assertEquals(HttpStatus.OK, re.getStatusCode());
		assertEquals(resp.getId(), id);
	}
	
	@Test
	public void getConfigurationById_whenServerDoesntContainConfigurationWithGivenId_returnsResponseWithStatusCode404() {		
		int id = -3;
		ResponseEntity<String> re = restTemplate.getForEntity(this.createURLWithPort("/api/configurations/"+id), String.class);
		assertEquals(HttpStatus.NOT_FOUND, re.getStatusCode());
	}
	
	
}
