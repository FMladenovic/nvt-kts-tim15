package com.tim15.ticketsSeller.controllers;

import com.google.gson.Gson;
import com.tim15.ticketsSeller.DTOImplementations.JwtResponse;
import com.tim15.ticketsSeller.DTOImplementations.NewLocation;
import com.tim15.ticketsSeller.DTOImplementations.UserCredentials;
import com.tim15.ticketsSeller.config.TestConfig;
import com.tim15.ticketsSeller.domain.Location;
import com.tim15.ticketsSeller.repositoryInterfaces.LocationRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Import(TestConfig.class)
public class LocationControllerTest {
	
	@Autowired
	LocationRepository locationRepository;
	
	private List<Location> locations = new ArrayList<Location>(); 
	

	@LocalServerPort
    private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}
	
	@Before
	public final void setUp()  {
		UUID id1 = UUID.fromString("1d7546ff-725a-44bc-8a77-1705d084900c");
		UUID id2 = UUID.fromString("1181f1d5-c7c1-482a-8b05-ba8e6f2c50ec");
		UUID id3 = UUID.fromString("6ae49682-c1b4-44eb-b9b7-e5128eab451d");
		UUID id4 = UUID.fromString("b32d6062-a991-4347-b90f-cc9a316aabfc");
		UUID id5 = UUID.fromString("a6660045-1d12-4756-8888-6c8847434f55");
		locations.add(new Location(id1, "test1", 0));
		locations.add(new Location(id2, "test2", 0));
		locations.add(new Location(id3, "test3", 0));
		locations.add(new Location(id4, "test4", 0));
		locations.add(new Location(id5, "test5", 0));
		
		locationRepository.saveAll(locations);
    }

	@Before
	public void login() {
		UserCredentials userCredentials = new UserCredentials("admin", "admin");

		ResponseEntity<JwtResponse> responseEntity =
				restTemplate.postForEntity(this.createURLWithPort("/api/user/login"), userCredentials, JwtResponse.class);

		restTemplate.getRestTemplate().setInterceptors(
				Collections.singletonList((request, body, execution) -> {
					request.getHeaders()
							.add("Authorization", responseEntity.getBody().getValue());
					return execution.execute(request, body);
				}));
	}

	@After
	public final void tearDown() {
		locations = new ArrayList<Location>();
		locationRepository.deleteAll();
	}
	
	@Test
	public void getLocations() {		
		ResponseEntity<String> re = restTemplate.getForEntity(this.createURLWithPort("/api/locations/"), String.class);
		
		Gson g = new Gson();
		Location[] resp = g.fromJson(re.getBody(), Location[].class);


		assertEquals(HttpStatus.OK, re.getStatusCode());
		assertEquals(resp.length, this.locations.size());
	}
	
	@Test
	public void getLocationById_whenServerContainstLocationWithGivenId_returnsResposnseWithtLocation() {		
		UUID id = UUID.fromString("1d7546ff-725a-44bc-8a77-1705d084900c");
		ResponseEntity<String> re = restTemplate.getForEntity(this.createURLWithPort("/api/locations/"+id.toString()), String.class);

		Gson g = new Gson();
		Location resp = g.fromJson(re.getBody(), Location.class);

		assertEquals(HttpStatus.OK, re.getStatusCode());
		assertEquals(resp.getId(), id);
	}
	
	@Test
	public void getLocationById_whenServerDoesntContainLocationWithGivenId_returnsResponseWithStatuscode404() {		
		UUID id = UUID.randomUUID();
		ResponseEntity<String> re = restTemplate.getForEntity(this.createURLWithPort("/api/locations/"+id.toString()), String.class);

		assertEquals(HttpStatus.NOT_FOUND, re.getStatusCode());
	}
	
	@Test
	public void postLocation_whenGivenLocationIsValid_returnsResponseWithStatusCode201() {		
		NewLocation newLocation = new NewLocation("Test15", 0, true);

		ResponseEntity<String> re = restTemplate.postForEntity(this.createURLWithPort("/api/locations/"), newLocation, String.class);
		assertEquals(HttpStatus.CREATED, re.getStatusCode());
		
		ResponseEntity<String> re1 = restTemplate.getForEntity(this.createURLWithPort(re.getHeaders().get("Location").get(0)), String.class);
		assertEquals(HttpStatus.OK, re1.getStatusCode());
	}
	
	@Test
	public void postLocation_whenGivenLocationIsWithExistingName_returnsResponseWithStatusCode409() {		
		NewLocation newLocation = new NewLocation("test1", 0, true);

		ResponseEntity<String> re = restTemplate.postForEntity(this.createURLWithPort("/api/locations/"), newLocation, String.class);
		assertEquals(HttpStatus.CONFLICT, re.getStatusCode());
	}
	
	@Test
	public void postLocation_whenGivenLocationHasUnexistingConfigurationId_returnsResponseWithStatusCode404() {	
		NewLocation newLocation = new NewLocation("Test1", 1, true);

		ResponseEntity<String> re = restTemplate.postForEntity(this.createURLWithPort("/api/locations/"), newLocation, String.class);
		assertEquals(HttpStatus.NOT_FOUND, re.getStatusCode());
	}
	
	
	@Test
	public void postLocation_whenGivenLocationIsInvalid_returnsResponseWithStatusCode400() {		
		ResponseEntity<String> re = restTemplate.postForEntity(this.createURLWithPort("/api/locations/"), new NewLocation(), String.class);
		assertEquals(HttpStatus.BAD_REQUEST, re.getStatusCode());
	}
	
	@Test 
	public void putLocation_whenGivenLocationIsValid_returnsResponseWithStatusCode200() {
		UUID id = UUID.fromString("1d7546ff-725a-44bc-8a77-1705d084900c");
		NewLocation newLocation = new NewLocation("Test15", 0, true);
		restTemplate.put(this.createURLWithPort("/api/locations/"+id.toString()), newLocation);
		
		ResponseEntity<String> re = restTemplate.getForEntity(this.createURLWithPort("/api/locations/"+id.toString()), String.class);
		Gson g = new Gson();
		Location resp = g.fromJson(re.getBody(), Location.class);
		assertEquals(resp.getId(), id);
    }

}
