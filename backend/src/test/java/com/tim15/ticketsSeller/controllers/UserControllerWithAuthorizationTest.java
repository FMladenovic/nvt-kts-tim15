package com.tim15.ticketsSeller.controllers;


import com.tim15.ticketsSeller.DTOImplementations.JwtResponse;
import com.tim15.ticketsSeller.DTOImplementations.UpdateUser;
import com.tim15.ticketsSeller.DTOImplementations.UserCredentials;
import com.tim15.ticketsSeller.DTOImplementations.UserDetails;
import com.tim15.ticketsSeller.config.TestConfig;
import com.tim15.ticketsSeller.domain.User;
import com.tim15.ticketsSeller.repositoryInterfaces.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityNotFoundException;
import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertNotSame;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Import(TestConfig.class)
public class UserControllerWithAuthorizationTest {

    private final String USERNAME_OF_LOGGED_USER = "user";

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private UserRepository userRepository;

    @LocalServerPort
    private int port;

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/api/user" + uri;
    }

    private UpdateUser mockUpdateUser;

    private HttpHeaders mockHeaders;

    private FileSystemResource mockFileSystemResource;

    private MultiValueMap<String, Object> mockParameters;

    private User mockLoggedUser;

    private Set<String> initialFiles;

    @Before
    public void setUp() throws IOException {

        mockLoggedUser = getUser();

        mockUpdateUser = new UpdateUser();
        mockUpdateUser.setOldPassword("user");

        mockHeaders = new HttpHeaders();
        mockHeaders.set("Content-Type", "multipart/form-data");
        mockHeaders.set("Accept", "application/json");

        mockFileSystemResource =
                new FileSystemResource("src\\test\\resources\\static\\images\\user.png");

        mockParameters = new LinkedMultiValueMap<>();

        initialFiles = getFiles();
    }

    @Before
    public void login() {
        UserCredentials userCredentials = new UserCredentials(USERNAME_OF_LOGGED_USER, "user");

        ResponseEntity<JwtResponse> responseEntity =
                testRestTemplate.postForEntity(this.createURLWithPort("/login"), userCredentials, JwtResponse.class);

        testRestTemplate.getRestTemplate().setInterceptors(
                Collections.singletonList((request, body, execution) -> {
                    request.getHeaders()
                            .add("Authorization", responseEntity.getBody().getValue());
                    return execution.execute(request, body);
                }));
    }

    @After
    public void tearDown() throws IOException {

        //delete added user images
        getFiles().forEach(path -> {
            File file = new File(path);
            if (file.exists() && !initialFiles.contains(path)) {
                file.delete();
            }
        });

        //set user as on original state
        userRepository.save(mockLoggedUser);
    }

    private Set<String> getFiles() throws IOException {
        Set<String> allFiles = new HashSet<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get("src\\test\\resources\\static\\images\\"))) {
            stream.forEach(path -> {
                if (!Files.isDirectory(path)) {
                    allFiles.add(path.toString());
                }
            });
        }

        return allFiles;
    }

    private User getUser() {
        return userRepository.findByUsername(USERNAME_OF_LOGGED_USER).orElseThrow(EntityNotFoundException::new);
    }

    @Test
    public void getLoggedUser_ValidRequest_ShouldReturnLoggedUser() {

        ResponseEntity<UserDetails> responseEntity =
                testRestTemplate.getForEntity(this.createURLWithPort(""), UserDetails.class);

        UserDetails loggedUser = responseEntity.getBody();

        assertNotNull(loggedUser);
        assertEquals("user", loggedUser.getUsername());
        assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void getLoggedUser_UserNotLoggedIn_ShouldResultInForbiddenStatus() {
        //remove logged user
        testRestTemplate.getRestTemplate().setInterceptors(
                Collections.singletonList((request, body, execution) -> {
                    request.getHeaders().remove("Authorization");
                    return execution.execute(request, body);
                }));

        ResponseEntity<UserDetails> responseEntity =
                testRestTemplate.getForEntity(this.createURLWithPort(""), UserDetails.class);

        assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.FORBIDDEN));
    }

    @Test
    public void updateUser_UpdateName_ShouldSaveUpdatedUser() {
        mockUpdateUser.setName("New name");
        mockParameters.add("file", null);
        mockParameters.add("user", mockUpdateUser);

        ResponseEntity<UpdateUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort(""),
                HttpMethod.PATCH, new HttpEntity<>(mockParameters, mockHeaders), UpdateUser.class);
        UpdateUser updatedUser = responseEntity.getBody();

        assertEquals("New name", updatedUser.getName());
        //updated user in db
        String nameAfterUpdate = getUser().getName();
        assertNotSame(mockLoggedUser.getName(), nameAfterUpdate);
        assertEquals(nameAfterUpdate, "New name");
    }

    @Test
    public void updateUser_UpdateSurname_ShouldSaveUpdatedUser() {
        mockUpdateUser.setSurname("New surname");
        mockParameters.add("file", null);
        mockParameters.add("user", mockUpdateUser);

        ResponseEntity<UpdateUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort(""),
                HttpMethod.PATCH, new HttpEntity<>(mockParameters, mockHeaders), UpdateUser.class);
        UpdateUser updatedUser = responseEntity.getBody();

        assertEquals("New surname", updatedUser.getSurname());
        //updated user in db
        String surnameAfterUpdate = getUser().getSurname();
        assertNotSame(mockLoggedUser.getSurname(), surnameAfterUpdate);
        assertEquals(surnameAfterUpdate, "New surname");
    }


    @Test
    public void updateUser_UpdatePassword_ShouldSaveUpdatedUser() {
        mockUpdateUser.setPassword("New password");
        mockUpdateUser.setConfirmPassword("New password");
        mockParameters.add("file", null);
        mockParameters.add("user", mockUpdateUser);

        ResponseEntity<UpdateUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort(""),
                HttpMethod.PATCH, new HttpEntity<>(mockParameters, mockHeaders), UpdateUser.class);
        UpdateUser updatedUser = responseEntity.getBody();

        assertEquals("New password", updatedUser.getPassword());
        //updated user in db
        String passwordAfterUpdate = getUser().getPassword();
        assertNotSame(mockLoggedUser.getPassword(), passwordAfterUpdate);
        assertEquals(passwordAfterUpdate, "New password");
    }


    @Test
    public void updateUser_UpdateImage_ShouldSaveUpdatedUser() {
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockUpdateUser);

        ResponseEntity<UpdateUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort(""),
                HttpMethod.PATCH, new HttpEntity<>(mockParameters, mockHeaders), UpdateUser.class);

        //updated user in db
        User userAfterUpdate = getUser();
        assertNotSame(userAfterUpdate.getImagePath(), mockLoggedUser.getImagePath());
    }


    @Test
    public void updateUser_UpdateAll_ShouldSaveUpdatedUser() {
        mockUpdateUser.setName("New name");
        mockUpdateUser.setSurname("New surname");
        mockUpdateUser.setPassword("New password");
        mockUpdateUser.setConfirmPassword("New password");
        mockParameters.add("file", mockFileSystemResource);
        mockParameters.add("user", mockUpdateUser);

        ResponseEntity<UpdateUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort(""),
                HttpMethod.PATCH, new HttpEntity<>(mockParameters, mockHeaders), UpdateUser.class);

        //updated user in db
        User userAfterUpdate = getUser();
        assertEquals(userAfterUpdate.getName(), mockUpdateUser.getName());
        assertEquals(userAfterUpdate.getSurname(), mockUpdateUser.getSurname());
        assertEquals(userAfterUpdate.getPassword(), mockUpdateUser.getPassword());
        //assert that user in db changed
        assertNotSame(userAfterUpdate.getName(), mockLoggedUser.getName());
        assertNotSame(userAfterUpdate.getSurname(), mockLoggedUser.getSurname());
        assertNotSame(userAfterUpdate.getPassword(), mockLoggedUser.getPassword());
        assertNotSame(userAfterUpdate.getImagePath(), mockLoggedUser.getImagePath());
    }

    @Test
    public void updateUser_InvalidOldPassword_ShouldResultInUnProcessableEntity() {
        mockUpdateUser.setOldPassword("invalid password");

        mockParameters.add("file", null);
        mockParameters.add("user", mockUpdateUser);

        ResponseEntity<UpdateUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort(""),
                HttpMethod.PATCH, new HttpEntity<>(mockParameters, mockHeaders), UpdateUser.class);

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
    }

    /*update user data validation*/
    @Test
    public void updateUser_BlankOldPassword_ShouldResultInUnProcessableEntity() {
        mockUpdateUser.setOldPassword("");

        mockParameters.add("file", null);
        mockParameters.add("user", mockUpdateUser);

        ResponseEntity<UpdateUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort(""),
                HttpMethod.PATCH, new HttpEntity<>(mockParameters, mockHeaders), UpdateUser.class);

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
    }

    @Test
    public void updateUser_ShortPassword_ShouldResultInUnProcessableEntity() {
        mockUpdateUser.setPassword("pass");

        mockParameters.add("file", null);
        mockParameters.add("user", mockUpdateUser);

        ResponseEntity<UpdateUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort(""),
                HttpMethod.PATCH, new HttpEntity<>(mockParameters, mockHeaders), UpdateUser.class);

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
    }

    @Test
    public void updateUser_ConfirmPasswordIsDifferentThanPassword_ShouldResultInUnProcessableEntity() {
        mockUpdateUser.setPassword("new password");
        mockUpdateUser.setConfirmPassword("different password");

        mockParameters.add("file", null);
        mockParameters.add("user", mockUpdateUser);

        ResponseEntity<UpdateUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort(""),
                HttpMethod.PATCH, new HttpEntity<>(mockParameters, mockHeaders), UpdateUser.class);

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
    }

    @Test
    public void updateUser_ShortConfirmPassword_ShouldResultInUnProcessableEntity() {
        mockUpdateUser.setConfirmPassword("pass");

        mockParameters.add("file", null);
        mockParameters.add("user", mockUpdateUser);

        ResponseEntity<UpdateUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort(""),
                HttpMethod.PATCH, new HttpEntity<>(mockParameters, mockHeaders), UpdateUser.class);

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
    }

    @Test
    public void updateUser_NewPasswordSameAsOldPassword_ShouldResultInUnProcessableEntity() {
        mockUpdateUser.setPassword(mockUpdateUser.getOldPassword());

        mockParameters.add("file", null);
        mockParameters.add("user", mockUpdateUser);

        ResponseEntity<UpdateUser> responseEntity = testRestTemplate.exchange(this.createURLWithPort(""),
                HttpMethod.PATCH, new HttpEntity<>(mockParameters, mockHeaders), UpdateUser.class);

        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, responseEntity.getStatusCode());
    }
}
