package com.tim15.ticketsSeller.controllers;

import com.google.gson.Gson;
import com.tim15.ticketsSeller.DTOImplementations.JwtResponse;
import com.tim15.ticketsSeller.DTOImplementations.NewCategories;
import com.tim15.ticketsSeller.DTOImplementations.UserCredentials;
import com.tim15.ticketsSeller.config.TestConfig;
import com.tim15.ticketsSeller.domain.Categories;
import com.tim15.ticketsSeller.domain.Category;
import com.tim15.ticketsSeller.repositoryInterfaces.CategoriesMongoRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Import(TestConfig.class)
public class CategoriesControllerTest {

    @Autowired
    private CategoriesMongoRepository categoriesRepository;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private List<Categories> listCategories = new ArrayList<Categories>();


    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    @SuppressWarnings("serial")
    private NewCategories newCategories() {
        List<String> layout = new ArrayList<String>() {
            {
                add("aaaaa");
                add("a_p_a");
                add("a__pa");
                add("ap__a");
                add("aaaaa");
            }
        };
        Category c1 = new Category();
        c1.setSeats(new ArrayList<String>() {
                        {
                            add("_____");
                            add("_p___");
                            add("_____");
                            add("___p_");
                            add("_____");
                        }
                    }
        );
        c1.setParterCapacities(new ArrayList<Integer>() {
            {
                add(100);
                add(null);
                add(100);
            }
        });
        c1.setColor("blue");
        c1.setName("Parter1");
        c1.setPrice(100);

        Category c2 = new Category();
        c2.setSeats(new ArrayList<String>() {
                        {
                            add("_____");
                            add("_____");
                            add("___p_");
                            add("_____");
                            add("_____");
                        }
                    }
        );
        c2.setParterCapacities(new ArrayList<Integer>() {
            {
                add(null);
                add(200);
                add(null);
            }
        });
        c2.setColor("green");
        c2.setName("Parter2");
        c2.setPrice(50);

        Category c3 = new Category();
        c3.setSeats(new ArrayList<String>() {
                        {
                            add("aaaaa");
                            add("a___a");
                            add("a___a");
                            add("a___a");
                            add("aaaaa");
                        }
                    }
        );
        c3.setParterCapacities(new ArrayList<Integer>() {
            {
                add(null);
                add(200);
                add(null);
            }
        });
        c3.setColor("yellow");
        c3.setName("Seats");
        c3.setPrice(200);

        List<Category> categories = new ArrayList<Category>() {
            {
                add(c1);
                add(c2);
                add(c3);
            }
        };

        List<Integer> parterCapacities = new ArrayList<Integer>() {
            {
                add(100);
                add(200);
                add(100);
            }
        };

        return new NewCategories(layout, parterCapacities, categories);
    }

    @SuppressWarnings("serial")
    @Before
    public final void setUp() {
        UUID id1 = UUID.fromString("6aa5ac20-e20e-4e34-873a-8283904fda39");
        UUID id2 = UUID.fromString("228c132f-5355-4f90-8fac-9cdaa8d7e869");
        UUID id3 = UUID.fromString("d5a96313-d390-4862-94a0-8c6770e82c83");
        UUID id4 = UUID.fromString("c0fa08f3-057e-474a-af54-3dc976f3e084");
        List<String> layout = new ArrayList<String>() {
            {
                add("aaaaa");
                add("a_p_a");
                add("a__pa");
                add("ap__a");
                add("aaaaa");
            }
        };
        Category c1 = new Category();
        c1.setSeats(new ArrayList<String>() {
                        {
                            add("_____");
                            add("_p___");
                            add("_____");
                            add("___p_");
                            add("_____");
                        }
                    }
        );
        c1.setParterCapacities(new ArrayList<Integer>() {
            {
                add(100);
                add(null);
                add(100);
            }
        });
        c1.setColor("blue");
        c1.setName("Parter1");
        c1.setPrice(100);

        Category c2 = new Category();
        c2.setSeats(new ArrayList<String>() {
                        {
                            add("_____");
                            add("_____");
                            add("___p_");
                            add("_____");
                            add("_____");
                        }
                    }
        );
        c2.setParterCapacities(new ArrayList<Integer>() {
            {
                add(null);
                add(200);
                add(null);
            }
        });
        c2.setColor("green");
        c2.setName("Parter2");
        c2.setPrice(50);

        Category c3 = new Category();
        c3.setSeats(new ArrayList<String>() {
                        {
                            add("aaaaa");
                            add("a___a");
                            add("a___a");
                            add("a___a");
                            add("aaaaa");
                        }
                    }
        );
        c3.setParterCapacities(new ArrayList<Integer>() {
            {
                add(null);
                add(200);
                add(null);
            }
        });
        c3.setColor("yellow");
        c3.setName("Seats");
        c3.setPrice(200);

        List<Category> categories = new ArrayList<Category>() {
            {
                add(c1);
                add(c2);
                add(c3);
            }
        };

        List<Integer> parterCapacities = new ArrayList<Integer>() {
            {
                add(100);
                add(200);
                add(100);
            }
        };

        this.listCategories.add(new Categories(id1, layout, parterCapacities, categories));
        this.listCategories.add(new Categories(id2, layout, parterCapacities, categories));
        this.listCategories.add(new Categories(id3, layout, parterCapacities, categories));
        this.listCategories.add(new Categories(id4, layout, parterCapacities, categories));

        categoriesRepository.saveAll(listCategories);

    }

    @Before
    public void login() {
		UserCredentials userCredentials = new UserCredentials("admin", "admin");

		ResponseEntity<JwtResponse> responseEntity =
				restTemplate.postForEntity(this.createURLWithPort("/api/user/login"), userCredentials, JwtResponse.class);

        restTemplate.getRestTemplate().setInterceptors(
                Collections.singletonList((request, body, execution) -> {
                    request.getHeaders()
                            .add("Authorization", responseEntity.getBody().getValue());
                    return execution.execute(request, body);
                }));
    }

    @After
    public final void tearDown() {
        categoriesRepository.deleteAll();
        this.listCategories = new ArrayList<Categories>();
    }


    @Test
    public void getCategories() {
        ResponseEntity<String> re = restTemplate.getForEntity(this.createURLWithPort("/api/categories/"), String.class);

        Gson g = new Gson();
        Categories[] resp = g.fromJson(re.getBody(), Categories[].class);


        assertEquals(HttpStatus.OK, re.getStatusCode());
        assertEquals(resp.length, this.listCategories.size());
    }

    @Test
    public void getCategoriesById_whenServerContainsCategoriesnWithGivenId_returnsResposnseWithCategories() {
        UUID id = UUID.fromString("6aa5ac20-e20e-4e34-873a-8283904fda39");
        ResponseEntity<String> re = restTemplate.getForEntity(this.createURLWithPort("/api/categories/" + id.toString()), String.class);

        Gson g = new Gson();
        Categories resp = g.fromJson(re.getBody(), Categories.class);

        assertEquals(HttpStatus.OK, re.getStatusCode());
        assertEquals(resp.getId(), id);
    }

    @Test
    public void getCategoriesById_whenServerDoesntContainCategoriesWithGivenId_returnsResponseWithStatuscode404() {
        UUID id = UUID.randomUUID();
        ResponseEntity<String> re = restTemplate.getForEntity(this.createURLWithPort("/api/categories/" + id.toString()), String.class);

        assertEquals(HttpStatus.NOT_FOUND, re.getStatusCode());
    }

    @Test
    public void postCategories_whenGivenCategoriesAreValid_returnsResponseWithStatusCode201() {
        NewCategories newCategories = this.newCategories();
        ResponseEntity<String> re = restTemplate.postForEntity(this.createURLWithPort("/api/categories/"), newCategories, String.class);
        assertEquals(HttpStatus.CREATED, re.getStatusCode());

        ResponseEntity<String> re1 = restTemplate.getForEntity(this.createURLWithPort(re.getHeaders().get("Location").get(0)), String.class);
        assertEquals(HttpStatus.OK, re1.getStatusCode());
    }

    @Test
    public void postCategories_whenGivenCategoriesAreInvalid_returnsResponseWithStatusCode400() {
        ResponseEntity<String> re = restTemplate.postForEntity(this.createURLWithPort("/api/categories/"), new NewCategories(), String.class);
        assertEquals(HttpStatus.BAD_REQUEST, re.getStatusCode());
    }

}
