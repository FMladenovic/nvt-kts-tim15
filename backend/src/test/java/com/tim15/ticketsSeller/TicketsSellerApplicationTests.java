package com.tim15.ticketsSeller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class TicketsSellerApplicationTests {

	@Test
	void contextLoads() {
	}

}
