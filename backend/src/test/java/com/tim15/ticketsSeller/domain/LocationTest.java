package com.tim15.ticketsSeller.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.UUID;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class LocationTest {
	
	@SuppressWarnings("unused")
	private static Stream<Arguments> invalidParamsProviderConstThreeParams() {
	    return Stream.of(
	      Arguments.of(null, 1),
	      Arguments.of("validName", -1),
	      Arguments.of(null, -1)
	    );
	}
	
	

	@ParameterizedTest
	@MethodSource("invalidParamsProviderConstThreeParams")
	void constructorWithTwoParams_WhenInvalidValuesIsGiven_ThrowsIllegalArgumentException(String name, int configurationId) {
		assertThrows(IllegalArgumentException.class, () -> {new Location(name, configurationId);});
	}
	@Test
	void constructorWithTwoParams_WhenValidValuesIsGiven_CreatesInstanceWithGivenProps() {
		
		String name = "ValidName";
		int configurationId = 1;
		
		Location location = new Location(name, configurationId);
		
		assertEquals(location.getName(), name);
		assertEquals(location.getConfigurationId(), configurationId);
	}
	
	@ParameterizedTest
	@MethodSource("invalidParamsProviderConstThreeParams")
	void constructorWithThreeParams_WhenInvalidValuesIsGiven_ThrowsIllegalArgumentException(String name, int configurationId) {
		assertThrows(IllegalArgumentException.class, () -> {new Location(null, name, configurationId);});
	}
	
	@Test
	void constructorWithThreeParams_WhenValidValuesIsGiven_CreatesInstanceWithGivenProps() {
		
		UUID id = UUID.randomUUID();
		String name = "ValidName";
		int configurationId = 1;
		
		Location location = new Location(id ,name, configurationId);
		
		assertEquals(location.getId(), id);
		assertEquals(location.getName(), name);
		assertEquals(location.getConfigurationId(), configurationId);

	}
	
	
	@Test
	void setId_WhenGivenIdIsNull_ThrowsIllegalArgumentException() {
		
		UUID id = UUID.randomUUID();
		String name = "ValidName";
		int configurationId = 1;
		
		Location location = new Location(id ,name, configurationId);
		
		assertThrows(IllegalArgumentException.class, () -> {location.setId(null);});
	}
	
	@Test
	void setId_WhenGivenIdIsValid_SetsIdOnGivenValue() {
		
		UUID id = UUID.randomUUID();
		String name = "ValidName";
		int configurationId = 1;
		Location location = new Location(UUID.randomUUID(), name, configurationId);
		
		location.setId(id);
		
		assertEquals(location.getId(), id);
	}

	@ParameterizedTest
	@NullAndEmptySource
	void setName_WhenGivenNameIsInvalid_ThrowsIllegalArgumentException(String name) {
		UUID id = UUID.randomUUID();
		int configurationId = 1;		
		Location location = new Location(id ,"ValidName", configurationId);
		
		assertThrows(IllegalArgumentException.class, () -> {location.setName(name);});
	}

	@ParameterizedTest
	@ValueSource(strings = {"validName", "Valid"})
	void setName_WhenGivenNameIsValid_SetsNameOnGivenValue(String name) {
		UUID id = UUID.randomUUID();
		int configurationId = 1;		
		Location location = new Location(id ,"ValidName", configurationId);
		
		location.setName(name);
		
		assertEquals(location.getName(), name);
	}


	@ParameterizedTest
	@ValueSource(ints = {-1, -3, -5, -15, Integer.MIN_VALUE})
	void setConfigurationId_WhenGivenConfigurationIdIsInvalid_ThrowsIllegalArgumentException(int configurationId) {
		UUID id = UUID.randomUUID();
		String name = "ValidName";		
		Location location = new Location(id ,name, 1);
		
		assertThrows(IllegalArgumentException.class, () -> {location.setConfigurationId(configurationId);});
	}

	@ParameterizedTest
	@ValueSource(ints = {1, 3, 5, 15, Integer.MAX_VALUE})
	void setConfigurationId_WhenGivenConfigurationIdIsValid_SetsConfigurationIdOnGivenValue(int configurationId) {
		UUID id = UUID.randomUUID();
		String name = "ValidName";
		
		Location location = new Location(id ,name, 1);
		
		location.setConfigurationId(configurationId);
		assertEquals(location.getConfigurationId(), configurationId);
	}



}
