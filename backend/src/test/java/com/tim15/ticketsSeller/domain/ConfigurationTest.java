package com.tim15.ticketsSeller.domain;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class ConfigurationTest {
	
	@SuppressWarnings("unused")
	private static Stream<Arguments> invalidParamsProvider() {
	    return Stream.of(
	      Arguments.of(-1, "validName", new ArrayList<String>(), new ArrayList<Integer>()),
	      Arguments.of(1, "", new ArrayList<String>(), new ArrayList<Integer>() ),
	      Arguments.of(1, null, new ArrayList<String>(), new ArrayList<Integer>() ),
	      Arguments.of(1, "validName", null, new ArrayList<Integer>()),
	      Arguments.of(-1, "", new ArrayList<String>(), null),
	      Arguments.of(1, null, null, null)
	    );
	}
	
	
	@ParameterizedTest
	@MethodSource("invalidParamsProvider")
	void constructor_WhenInvalidParamsIsGiven_ThrowsIllegalArgumentException(int id, String name, ArrayList<String> layout, ArrayList<Integer> parterCapacities) {
		assertThrows(IllegalArgumentException.class, () -> {new Configuration(id, name, layout, parterCapacities);});
	}
	
	@Test
	void constructor_WhenValidParamsIsGiven_CreatesInstanceOfConfigurationWithGivenValues() {
		int id = 1;
		String name = "validValue";
		ArrayList<String> layout = new ArrayList<String>();
		ArrayList<Integer> parterCapacities = new ArrayList<Integer>();
		Configuration configuration = new Configuration(id, name, layout, parterCapacities);
		
		assertEquals(configuration.getId(), id);
		assertEquals(configuration.getName(), name);
		assertEquals(configuration.getLayout(), layout);

	}

	@ParameterizedTest
	@ValueSource(ints = {-1, -3, -5, -15, Integer.MIN_VALUE})
	void setID_WhenGivenIDIsInvalid_ThrowsIllegalArgumentException(int id) {
		Configuration configuration = new Configuration(1, "validName", new ArrayList<String>(), new ArrayList<Integer>());
		assertThrows(IllegalArgumentException.class, () -> {configuration.setId(id);});
	}

	@ParameterizedTest
	@ValueSource(ints = {1, 3, 5, 15, Integer.MAX_VALUE})
	void setID_WhenGivenIDIsValid_SetsIDOnGivenValue(int id) {
		Configuration configuration = new Configuration(10, "validName", new ArrayList<String>(), new ArrayList<Integer>());
		
		configuration.setId(id);
		
		assertEquals(configuration.getId(), id);
	}

	@ParameterizedTest
	@NullAndEmptySource
	void setName_WhenGivenNameIsInvalid_ThrowsIllegalArgumentException(String name) {
		Configuration configuration = new Configuration(1, "validName", new ArrayList<String>(), new ArrayList<Integer>());
		assertThrows(IllegalArgumentException.class, () -> {configuration.setName(name);});
	}

	@ParameterizedTest
	@ValueSource(strings = {"validName", "Valid"})
	void setName_WhenGivenNameIsValid_SetsNameOnGivenValue(String name) {
		Configuration configuration = new Configuration(10, "validName", new ArrayList<String>(), new ArrayList<Integer>());
		
		configuration.setName(name);
		
		assertEquals(configuration.getName(), name);
	}
	
	@Test
	void setLayout_WhenGivenLayoutIsNull_ThrowsIllegalArgumentException() {
		Configuration configuration = new Configuration(1, "validName", new ArrayList<String>(), new ArrayList<Integer>());
		assertThrows(IllegalArgumentException.class, () -> {configuration.setLayout(null);});
	}
	
	@Test
	@SuppressWarnings("serial")
	void setLayout_WhenGivenLayoutDoesntMatchWithPCapacities_ThrowsIllegalArgumentException() {
		
		ArrayList<String> layout = new ArrayList<String>(){ 
            { 
                add("_p_"); 
                add("__p"); 
                add("p__"); 
            } 
        }; 
        
        ArrayList<Integer> pCapacities = new ArrayList<Integer>() {
			{
				add(1);
				add(2);
				add(3);
			}
		};
        
		Configuration configuration = new Configuration(1, "validName", layout, pCapacities);
		assertThrows(IllegalArgumentException.class, () -> {configuration.setLayout(new ArrayList<String>());});
	}

	@Test
	void setLayout_WhenGivenLayoutIsValid_SetsLayoutOnGivenValue() {
		Configuration configuration = new Configuration(10, "validName", new ArrayList<String>(), new ArrayList<Integer>());
		ArrayList<String> layout = new ArrayList<String>();
		
		configuration.setLayout(layout);
		
		assertEquals(configuration.getLayout(), layout);
	}
	
	@Test
	void setPCapacitiesCapacity_WhenGivenPCapacitiesIsNull_ThrowsIllegalArgumentException() {
		Configuration configuration = new Configuration(1, "validName", new ArrayList<String>(), new ArrayList<Integer>());
		assertThrows(IllegalArgumentException.class, () -> {configuration.setParterCapacities(null);});
	}
	
	@Test
	@SuppressWarnings("serial")
	void setPCapacities_WhenGivenPCapacitiesDoesntMatchWithLayout_ThrowsIllegalArgumentException() {
		
		ArrayList<String> layout = new ArrayList<String>(){ 
            { 
                add("_p_"); 
                add("__p"); 
                add("p__"); 
            } 
        }; 
        
        ArrayList<Integer> pCapacities = new ArrayList<Integer>() {
			{
				add(1);
				add(2);
				add(3);
			}
		};
        
		Configuration configuration = new Configuration(1, "validName", layout, pCapacities);
		assertThrows(IllegalArgumentException.class, () -> {configuration.setParterCapacities(new ArrayList<Integer>());});
	}

	@Test
	void setpCapacities_WhenGivenPCapacitiesIsValid_SetsPCapacitiesOnGivenValue() {
		Configuration configuration = new Configuration(10, "validName", new ArrayList<String>(), new ArrayList<Integer>());
		ArrayList<Integer> pCapacities = new ArrayList<Integer>();
		
		configuration.setParterCapacities(pCapacities);
		
		assertEquals(configuration.getParterCapacities(), pCapacities);
	}
	
	@SuppressWarnings("serial")
	@Test
	void checkConfiguration_WhenGivenLayoutAndPCapacitiesDontMatch_ReturnsFalse() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {			
		
		Method method = null;

		method = Configuration.class.getDeclaredMethod("checkConfiguration", new ArrayList<String>().getClass(), new ArrayList<Integer>().getClass());

		method.setAccessible(true);
		
		Object[] obj = new Object[2];
		
		obj[0] = new ArrayList<String>(){ 
            { 
                add("_p_"); 
                add("__p"); 
                add("p__"); 
            } 
        }; 
		obj[1] = new ArrayList<Integer>() {
			{
				add(1);
				add(2);
			}
		};
		
		assertFalse((boolean) method.invoke(null, obj));
	}
	
	@SuppressWarnings("serial")
	@Test
	void checkConfiguration_WhenGivenLayoutAndPCapacitiesMatch_ReturnsTrue() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {			
		
		Method method = null;

		method = Configuration.class.getDeclaredMethod("checkConfiguration", new ArrayList<String>().getClass(), new ArrayList<Integer>().getClass());

		method.setAccessible(true);
		
		Object[] obj = new Object[2];
		
		obj[0] = new ArrayList<String>(){ 
            { 
                add("_p_"); 
                add("__p"); 
                add("p__"); 
            } 
        }; 
		obj[1] = new ArrayList<Integer>() {
			{
				add(1);
				add(2);
				add(3);
			}
		};
		
		
		assertTrue((boolean) method.invoke(null, obj));
	}
	

}
