package com.tim15.ticketsSeller.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class CategoryTest {

	@ParameterizedTest
	@NullAndEmptySource
	void setName_WhenNameIsInvalidString_ThrowsIllegalArgumentException(String name) {
		Category category = new Category();
		
		assertThrows(IllegalArgumentException.class, () -> {category.setName(name);});
	}
	
	@Test
	void setName_WhenNameIsValidString_SetsNameOfTheObject() {
		//Arange
		Category category = new Category();
		String name = "SomethingElse";
		
		//Act
		category.setName(name);
		
		//Assert
		assertEquals(category.getName(), name);
	}
	
	@ParameterizedTest
	@ValueSource(doubles = {0, -1,- 3, -5, -15, Integer.MIN_VALUE})
	void setPrice_WhenPriceIsInvalidValue_ThrowsIllegalArgumentException(double price) {
		Category category = new Category();
		
		assertThrows(IllegalArgumentException.class, () -> {category.setPrice(price);});
	}
	
	@ParameterizedTest
	@ValueSource(doubles = {10.12, 15.33, Integer.MAX_VALUE})
	void setPrice_WhenPriceIsValidValue_SetsPriceOfTheObject(double price) {
		//Arange
		Category category = new Category();
		
		//Act
		category.setPrice(price);
		
		//Assert
		assertEquals(category.getPrice(), price);
	}
	
	@Test
	void setSeats_WhenSeatsAreInvalidValue_ThrowsIllegalArgumentException() {
		Category category = new Category();
		
		assertThrows(IllegalArgumentException.class, () -> {category.setSeats(null);});
	}
	
	@Test
	void setSeats_WhenSeatsAreValidValue_setsSeats() {
		//Arange
		Category category = new Category();
		ArrayList<String> seats = new ArrayList<String>();
		//Act
		category.setSeats(seats);
		
		//Assert
		assertEquals(category.getSeats(), seats);
	}

	
	@SuppressWarnings("serial")
	@Test
	void checkCompatibilityOfSeatsAndParterCapacities_WhenGivenSeatsAndPCapacitiesDontMatch_ReturnsFalse() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {			
		
		Method method = null;

		method = Category.class.getDeclaredMethod("checkCompatibilityOfSeatsAndParterCapacities", List.class, List.class);

		method.setAccessible(true);
		
		Object[] obj = new Object[2];
		
		obj[0] = new ArrayList<String>(){ 
            { 
                add("_p_"); 
                add("__p"); 
                add("p__"); 
            } 
        }; 
		obj[1] = new ArrayList<Integer>() {
			{
				add(1);
				add(2);
			}
		};
		
		assertFalse((boolean) method.invoke(null, obj));
	}
	
	@SuppressWarnings("serial")
	@Test
	void checkCompatibilityOfSeatsAndParterCapacities_WhenGivenSeatsAndPCapacitiesMatch_ReturnsTrue() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {			
		
		Method method = null;

		method = Category.class.getDeclaredMethod("checkCompatibilityOfSeatsAndParterCapacities", 
				List.class, 
				List.class
				);

		method.setAccessible(true);
		
		Object[] obj = new Object[2];
		
		obj[0] = new ArrayList<String>(){ 
            { 
                add("_p_"); 
                add("__p"); 
                add("p__"); 
            } 
        }; 
		obj[1] = new ArrayList<Integer>() {
			{
				add(1);
				add(2);
				add(3);
			}
		};
		
		
		assertTrue((boolean) method.invoke(null, obj));
	}
	
	
	

}
