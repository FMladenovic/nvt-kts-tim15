package com.tim15.ticketsSeller.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;

class CategoriesTest {

	@SuppressWarnings("serial")
	private Categories setCategories() {
		List<String> layout = new ArrayList<String>() {
			{
				add("aaaaa");
				add("a_p_a"); 
                add("a__pa"); 
                add("ap__a"); 
				add("aaaaa");
			}
		};
		
		Category c1 = new Category();
		c1.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("_p___"); 
	                add("_____"); 
	                add("___p_"); 
					add("_____");
				}
			}
		);
		c1.setParterCapacities(new ArrayList<Integer>() {
			{
				add(100);
				add(null);
				add(100);
			}
		});
		
		
		Category c2 = new Category();
		c2.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("_____"); 
	                add("___p_"); 
	                add("_____"); 
					add("_____");
				}
			}
		);
		c2.setParterCapacities(new ArrayList<Integer>() {
			{
				add(null);
				add(200);
				add(null);
			}
		});
		Category c3 = new Category();
		c3.setSeats(new ArrayList<String>(){
				{
					add("aaaaa");
					add("a___a"); 
	                add("a___a"); 
	                add("a___a"); 
					add("aaaaa");
				}
			}
		);
		
		List<Category> categories = new ArrayList<Category>() {
			{
				add(c1);
				add(c2);
				add(c3);
			}
		};
		
		List<Integer> parterCapacities = new ArrayList<Integer>() {
			{
				add(100);
				add(200);
				add(100);
			}
		};
		
		return new Categories(layout, parterCapacities, categories);
	}
	
	@Test
	void setId_whenIdIsGivenAsNull_throwsIllegalArgumentException() {
		Categories categories = new Categories();
		assertThrows(IllegalArgumentException.class, () -> categories.setId(null));	
	}
	
	@Test
	void setId_whenIdIsValidValue_setsId() {
		UUID id = UUID.randomUUID();
		Categories categories = new Categories();
		
		categories.setId(id);
		
		assertEquals(id, categories.getId());
	}

	@SuppressWarnings("serial")
	@Test
	void checkCompatibilityOfLayoutAndParterCapacities_WhenGivenLayoutAndPCapacitiesMatch_ReturnsTrue() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {			
		
		Method method = null;


		method = Categories.class.getDeclaredMethod("checkCompatibilityOfLayoutAndParterCapacities", List.class,  List.class);

		method.setAccessible(true);
		
		Object[] obj = new Object[2];
		
		obj[0] = new ArrayList<String>(){ 
            { 
                add("_p_"); 
                add("__p"); 
                add("p__"); 
            } 
        }; 
		obj[1] = new ArrayList<Integer>() {
			{
				add(1);
				add(2);
				add(3);
			}
		};
		
		
		assertTrue((boolean) method.invoke(method, obj));
	}
	
	@SuppressWarnings("serial")
	@Test
	void checkCompatibilityOfLayoutAndParterCapacities_WhenGivenLayoutAndPCapacitiesDontMatch_ReturnsFalse() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {			
		
		Method method = null;


		method = Categories.class.getDeclaredMethod("checkCompatibilityOfLayoutAndParterCapacities", List.class,  List.class);

		method.setAccessible(true);
		
		Object[] obj = new Object[2];
		
		obj[0] = new ArrayList<String>(){ 
            { 
                add("_p_"); 
                add("__p"); 
                add("p__"); 
            } 
        }; 
		obj[1] = new ArrayList<Integer>() {
			{
				add(1);
				add(2);
			}
		};
		
		
		assertFalse((boolean) method.invoke(method, obj));
	}
	

	@SuppressWarnings("serial")
	@Test
	void checkCompatibilityOfLayoutAndCategories_whenLeyoutAndCategoriesAreCompatible_returnsTrue() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {			
		
		List<String> layout = new ArrayList<String>() {
			{
				add("aaaaa");
				add("a_p_a"); 
                add("a__pa"); 
                add("ap__a"); 
				add("aaaaa");
			}
		};
		
		Category c1 = new Category();
		c1.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("__p__"); 
	                add("___p_"); 
	                add("_p___"); 
					add("_____");
				}
			}
		);
		Category c2 = new Category();
		c2.setSeats(new ArrayList<String>(){
				{
					add("aaaaa");
					add("a___a"); 
	                add("a___a"); 
	                add("a___a"); 
					add("aaaaa");
				}
			}
		);
		
		List<Category> categories = new ArrayList<Category>() {
			{
				add(c1);
				add(c2);
			}
		};
		
		
		Method method = null;
		method = Categories.class.getDeclaredMethod("checkCompatibilityOfLayoutAndCategories", 
				List.class, 
				List.class
				);

		method.setAccessible(true);
		Object[] obj = new Object[2];
	
		obj[0] = layout;
		obj[1] = categories;
		
		
		assertTrue((boolean) method.invoke(null, obj));
	}
	
	@SuppressWarnings("serial")
	@Test
	void checkCompatibilityOfLayoutAndCategories_whenLeyoutAndCategoriesAreNotCompatible_returnsFalse() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {			
		
		List<String> layout = new ArrayList<String>() {
			{
				add("aaaaa");
				add("a_p_a"); 
                add("a__pa"); 
                add("ap__a"); 
				add("aaaaa");
			}
		};
		
		Category c1 = new Category();
		c1.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("__p__"); 
	                add("___p_"); 
	                add("_p___"); 
					add("_____");
				}
			}
		);
		Category c2 = new Category();
		c2.setSeats(new ArrayList<String>(){
				{
					add("aaaaaa");
					add("a___a"); 
	                add("a___a"); 
	                add("a___a"); 
					add("aaaaa");
				}
			}
		);
		Category c3 = new Category();
		c3.setSeats(new ArrayList<String>(){
				{
					add("aaaaa");
					add("a___a"); 
	                add("a___a"); 
	                add("a___a"); 
					add("aaaaa");
					add("aaaaa");

				}
			}
		);
		
		
		List<Category> categories1 = new ArrayList<Category>() {
			{
				add(c1);
				add(c2);
			}
		};
		
		List<Category> categories2 = new ArrayList<Category>() {
			{
				add(c1);
				add(c3);
			}
		};
		
		Method method = null;
		method = Categories.class.getDeclaredMethod("checkCompatibilityOfLayoutAndCategories", 
				List.class, 
				List.class
				);

		method.setAccessible(true);
		Object[] obj = new Object[2];
	
		obj[0] = layout;
		obj[1] = categories1;
		assertFalse((boolean) method.invoke(null, obj));
		
		obj[1] = categories2;
		assertFalse((boolean) method.invoke(null, obj));
	}

	@SuppressWarnings("serial")
	@Test
	void checkCompatibilityOfCategoriesAndParterCapacities_whenCategoriesAndParterCapacitiesAreCompatible_returnsTrue() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {			

		List<Integer> parterCapacities = new ArrayList<Integer>() {
			{
				add(100);
				add(200);
				add(100);
			}
		};
		
		Category c1 = new Category();
		c1.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("_____"); 
	                add("___p_"); 
	                add("_____"); 
					add("_____");
				}
			}
		);
		c1.setParterCapacities(new ArrayList<Integer>() {
			{
				add(null);
				add(200);
				add(null);
			}
		});
		
		
		Category c2 = new Category();
		c2.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("_p___"); 
	                add("_____"); 
	                add("___p_"); 
					add("_____");
				}
			}
		);
		c2.setParterCapacities(new ArrayList<Integer>() {
			{
				add(100);
				add(null);
				add(100);
			}
		});
		
		
		
		
		List<Category> categories = new ArrayList<Category>() {
			{
				add(c1);
				add(c2);
			}
		};
		

		
		Method method = null;
		method = Categories.class.getDeclaredMethod("checkCompatibilityOfCategoriesAndParterCapacities", 
				List.class, 
				List.class
				);

		method.setAccessible(true);
		Object[] obj = new Object[2];
	
		obj[1] = parterCapacities;
		obj[0] = categories;
		assertTrue((boolean) method.invoke(null, obj));
		

	}
	
	@SuppressWarnings("serial")
	@Test
	void checkCompatibilityOfCategoriesAndParterCapacities_whenCategoriesAndParterCapacitiesAreNotCompatible_returnsFalse() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {			

		List<Integer> parterCapacities = new ArrayList<Integer>() {
			{
				add(100);
				add(200);
				add(100);
			}
		};
		
		Category c1 = new Category();
		c1.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("_____"); 
	                add("___p_"); 
	                add("_____"); 
					add("_____");
				}
			}
		);
		c1.setParterCapacities(new ArrayList<Integer>() {
			{
				add(null);
				add(200);
				add(null);
				add(100);
			}
		});
		
		
		Category c2 = new Category();
		c2.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("_p___"); 
	                add("_____"); 
	                add("___p_"); 
					add("_____");
				}
			}
		);
		c2.setParterCapacities(new ArrayList<Integer>() {
			{
				add(100);
				add(null);
				add(100);
			}
		});
		
		
		Category c3 = new Category();
		c3.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("_____"); 
	                add("___p_"); 
	                add("_____"); 
					add("_____");
				}
			}
		);
		c3.setParterCapacities(new ArrayList<Integer>() {
			{
				add(null);
				add(201);
				add(null);
			}
		});
		
		
		
		
		List<Category> categories = new ArrayList<Category>() {
			{
				add(c1);
				add(c2);
			}
		};
		
		List<Category> categories1 = new ArrayList<Category>() {
			{
				add(c2);
				add(c3);
			}
		};

		
		Method method = null;
		method = Categories.class.getDeclaredMethod("checkCompatibilityOfCategoriesAndParterCapacities", 
				List.class, 
				List.class
				);

		method.setAccessible(true);
		Object[] obj = new Object[2];
	
		obj[1] = parterCapacities;
		obj[0] = categories;
		assertFalse((boolean) method.invoke(null, obj));
		
		obj[1] = parterCapacities;
		obj[0] = categories1;
		assertFalse((boolean) method.invoke(null, obj));
		
	}
	
	@SuppressWarnings("serial")
	@Test
	void setLayout_whenLayoutIsCompatibile_setsLayout(){
		
		List<String> newLayout = new ArrayList<String>() {
			{
				add("aaaaa");
				add("a_p_b"); 
                add("a__pb"); 
                add("ap__b"); 
				add("aaaab");
			}
		};
		Categories categoriesObj = this.setCategories();
		
		categoriesObj.setLayout(newLayout);
		
		assertEquals(categoriesObj.getLayout(), newLayout);
		
	}
	
	@Test
	void setLayout_whenLayoutIsNull_throwsIllegalArgumentException(){
		Categories categoriesObj = this.setCategories();
		assertThrows(IllegalArgumentException.class, ()->categoriesObj.setLayout(null));
	}
	
	@SuppressWarnings("serial")
	@Test
	void setParterCapacities_whenParterCapacitieAreCompatibile_setsParterCapacities(){
		Categories categoriesObj = this.setCategories();
		
		List<Integer> parterCapacities = new ArrayList<Integer>() {
			{
				add(120);
				add(210);
				add(330);
			}
		};
		
		categoriesObj.setParterCapacities(parterCapacities);
		
		assertEquals(categoriesObj.getParterCapacities(), parterCapacities);
	}
	
	@Test
	void setParterCapacities_whenParterCapacitieAreNull_throwsIllegalArgumentException(){
		Categories categoriesObj = this.setCategories();
		assertThrows(IllegalArgumentException.class, ()->categoriesObj.setParterCapacities(null));
	}

	@SuppressWarnings("serial")
	@Test
	void setCategories_whenCategoriesAreCompatibile_setsCategories(){
		Categories categoriesObj = this.setCategories();
		
		Category c1 = new Category();
		c1.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("_p___"); 
	                add("_____"); 
	                add("___p_"); 
					add("_____");
				}
			}
		);
		c1.setParterCapacities(new ArrayList<Integer>() {
			{
				add(33);
				add(null);
				add(11);
			}
		});
		
		
		Category c2 = new Category();
		c2.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("_____"); 
	                add("___p_"); 
	                add("_____"); 
					add("_____");
				}
			}
		);
		c2.setParterCapacities(new ArrayList<Integer>() {
			{
				add(null);
				add(22);
				add(null);
			}
		});
		Category c3 = new Category();
		c3.setSeats(new ArrayList<String>(){
				{
					add("aaaaa");
					add("a___a"); 
	                add("a___a"); 
	                add("a___a"); 
					add("aaaaa");
				}
			}
		);
		
		List<Category> categories = new ArrayList<Category>() {
			{
				add(c1);
				add(c2);
				add(c3);
			}
		};
		
		categoriesObj.setCategories(categories);
		
		assertEquals(categoriesObj.getCategories(), categories);
	}
	
	@Test
	void setCategories_whenCategoriesAreNull_throwsIllegalArgumentException(){
		Categories categoriesObj = this.setCategories();
		assertThrows(IllegalArgumentException.class, ()->categoriesObj.setCategories(null));
	}
	
	
}
