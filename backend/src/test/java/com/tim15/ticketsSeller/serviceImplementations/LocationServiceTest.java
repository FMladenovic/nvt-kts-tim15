package com.tim15.ticketsSeller.serviceImplementations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.StreamSupport;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.tim15.ticketsSeller.DTO.INewLocation;
import com.tim15.ticketsSeller.DTOImplementations.NewLocation;
import com.tim15.ticketsSeller.domain.Location;
import com.tim15.ticketsSeller.exception.customExceptions.ForeignKeyException;
import com.tim15.ticketsSeller.exception.customExceptions.UniqueViolationException;
import com.tim15.ticketsSeller.repositoryInterfaces.ConfigurationMongoRepository;
import com.tim15.ticketsSeller.repositoryInterfaces.LocationRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class LocationServiceTest {
	

	@MockBean
	LocationRepository locationRepository;
	
	@MockBean
	ConfigurationMongoRepository configurationRepository;
	
	@Autowired 
	LocationService locationService;
	
	ArrayList<Location> setTest(){
		ArrayList<Location> test = new ArrayList<Location>();
		test.add(new Location("test1", 0));
		test.add(new Location("test2", 0));
		test.add(new Location("test3", 0));
		test.add(new Location("test4", 0));
		test.add(new Location("test5", 0));
		return test;
	}

	@Test 
	public void getAll_whenLocationRepositoryDoesntHaveEntityToReturn_returns0Entities() {
		
		when(locationRepository.findAll()).thenReturn(new ArrayList<Location>());	
		
		
		Iterable<Location> locations = locationService.getAll();
		
		assertNotNull(locations);
		assertEquals(StreamSupport.stream(locations.spliterator(), false).count(), 0);	
    }
	
	
	@Test 
	public void getAll_whenLocationRepositoryHasEntityToReturn_returnsAllEntities() {
		
		ArrayList<Location> test = setTest();
		when(locationRepository.findAll()).thenReturn(test);	
		
		Iterable<Location> locations = locationService.getAll();
		
		assertNotNull(locations);
		assertEquals(StreamSupport.stream(locations.spliterator(), false).count(), test.size());
    }
	
	@Test 
	public void getById_whenLocationRepositoryDoesntHaveEntityWithGivenId_returnsNull() {
		
		UUID id = UUID.randomUUID();
		Optional<Location> locationMyb = Optional.empty();
		
		when(locationRepository.findById(id)).thenReturn(locationMyb);	
		
		Location location = locationService.getById(id);
		
		assertNull(location);
    }
	
	@Test 
	public void getById_whenLocationRepositoryHasEntityWithGivenId_returnsEntity() {
		Location testLocation = new Location("test1", 0);
		Optional<Location> test = Optional.of(testLocation);
		UUID id = testLocation.getId();
		
		when(locationRepository.findById(id)).thenReturn(test);	
		
		Location location = locationService.getById(id);
		
		assertNotNull(location);
		assertEquals(location.getId(), id);
		assertEquals(location.getName(), testLocation.getName());
		assertEquals(location.getConfigurationId(), testLocation.getConfigurationId());
    }
	
	@Test 
	public void post_whenLocationRepositoryDoesntContainEntityWithGivenNameAndConfigurationWithGivenIdExists_LocationRepositoryGetsLocationEntity() {
		
		INewLocation newLocation = mock(NewLocation.class);
        when(newLocation.getName()).thenReturn("test1");
        when(newLocation.getConfigurationId()).thenReturn(0);
        when(newLocation.isActive()).thenReturn(true);
        when(configurationRepository.existsById(0)).thenReturn(true);
        UUID id = null;
		try {
			id = locationService.post(newLocation);
		} catch (IllegalArgumentException | NoSuchElementException | UniqueViolationException e) {
			e.printStackTrace();
		}
		
        verify(locationRepository, times(1)).save(isA(Location.class));
        assertNotNull(id);

    }
	
	@Test 
	public void post_whenConfigurationWithGivenIdDoestExist_throwsNoSuchElementException() {
		
		INewLocation newLocation = mock(NewLocation.class);
        when(newLocation.getName()).thenReturn("test1");
        when(newLocation.getConfigurationId()).thenReturn(0);
        when(newLocation.isActive()).thenReturn(true);
        when(configurationRepository.existsById(0)).thenReturn(false);     

		try {
			assertThrows(NoSuchElementException.class, () -> locationService.post(newLocation));
		} catch (IllegalArgumentException | NoSuchElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test 
	public void post_whenLocationRepositoryContainsEntityWithGivenName_throwsUniqueViolationException() {
	
		INewLocation newLocation = mock(NewLocation.class);
        when(newLocation.getName()).thenReturn("test1");
        when(newLocation.getConfigurationId()).thenReturn(0);
        when(newLocation.isActive()).thenReturn(true);
        when(configurationRepository.existsById(0)).thenReturn(true);
        
        doThrow(new DataIntegrityViolationException("Name already exists.")).when(locationRepository).save(isA(Location.class));
      
		try {
			assertThrows(UniqueViolationException.class, () -> locationService.post(newLocation));
		} catch (IllegalArgumentException | NoSuchElementException e) {
			e.printStackTrace();
		}
		
        verify(locationRepository, times(1)).save(isA(Location.class));
	}
	
	@Test 
	public void post_whenParamIsNull_throwsIllegalArgumentException() {
		try {
			assertThrows(IllegalArgumentException.class, () -> locationService.post(null));
		} catch (IllegalArgumentException | NoSuchElementException e) {
			e.printStackTrace();
		}
	}
	
	@Test 
	public void put_whenLocationRepositoryDoesntContainEntityWithGivenNameAndConfigurationWithGivenIdExistsAndLocationWithGivenIdExists_LocationRepositoryGetsLocationEntity() {
		
		INewLocation newLocation = mock(NewLocation.class);
		UUID id = UUID.randomUUID();
        when(newLocation.getName()).thenReturn("test1");
        when(newLocation.getConfigurationId()).thenReturn(0);
        when(newLocation.isActive()).thenReturn(true);
		Location location = new Location(id, newLocation.getName(),newLocation.getConfigurationId());
        when(configurationRepository.existsById(0)).thenReturn(true);
        when(locationRepository.findById(id)).thenReturn(Optional.of(location));

        
      
		try {
			locationService.put(id ,newLocation);
		} catch (IllegalArgumentException | NoSuchElementException | UniqueViolationException e) {
			e.printStackTrace();
		}
		
        verify(locationRepository, times(1)).save(location);
    }
	
	
	@Test 
	public void put_whenLocationWithGivenIdDoesntExist_throwsNoSuchElementException() {
		
		INewLocation newLocation = mock(NewLocation.class);
		UUID id = UUID.randomUUID();
        when(newLocation.getName()).thenReturn("test1");
        when(newLocation.getConfigurationId()).thenReturn(0);
        when(newLocation.isActive()).thenReturn(true);

        doThrow(new NoSuchElementException()).when(locationRepository).findById(id);
        
		try {
			assertThrows(NoSuchElementException.class, () -> locationService.put(id, newLocation));
		} catch (IllegalArgumentException | NoSuchElementException e) {
			e.printStackTrace();
		}

    }
	
	@Test 
	public void put_whenConfigurationWithGivenIdDoesntExist_throwsNoSuchElementException() {
		
		INewLocation newLocation = mock(NewLocation.class);
		UUID id = UUID.randomUUID();
        when(newLocation.getName()).thenReturn("test1");
        when(newLocation.getConfigurationId()).thenReturn(0);
        when(newLocation.isActive()).thenReturn(true);
        when(configurationRepository.existsById(0)).thenReturn(false);
        Location location = new Location(id, newLocation.getName(),newLocation.getConfigurationId());
        when(locationRepository.findById(id)).thenReturn(Optional.of(location));
        
		try {
			assertThrows(NoSuchElementException.class, () -> locationService.put(id, newLocation));
		} catch (IllegalArgumentException | NoSuchElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test 
	public void put_whenLocationRepositoryContainsEntityWithGivenName_throwsUniqueViolationException() {
	
		INewLocation newLocation = mock(NewLocation.class);
		UUID id = UUID.randomUUID();
        when(newLocation.getName()).thenReturn("test1");
        when(newLocation.getConfigurationId()).thenReturn(0);
        when(newLocation.isActive()).thenReturn(true);
        when(configurationRepository.existsById(0)).thenReturn(true);
        Location location = new Location(id, newLocation.getName(),newLocation.getConfigurationId());
        when(locationRepository.findById(id)).thenReturn(Optional.of(location));
        
        doThrow(new DataIntegrityViolationException("Name already exists.")).when(locationRepository).save(isA(Location.class));
      
		try {
			assertThrows(UniqueViolationException.class, () -> locationService.put(id, newLocation));
		} catch (IllegalArgumentException | NoSuchElementException e) {
			e.printStackTrace();
		}
		
        verify(locationRepository, times(1)).save(location);
	}
	
	@Test 
	public void put_whenNewLocationIsNull_throwsIllegalArgumentException() {
		try {
			assertThrows(IllegalArgumentException.class, () -> locationService.put(UUID.randomUUID(), null));
		} catch (IllegalArgumentException | NoSuchElementException e) {
			e.printStackTrace();
		}
	}
	
	@Test 
	public void delete_whenLocationRepositoryContainsLocationWithGivenId_returnsTrue() {
				
		UUID id = UUID.randomUUID();
		boolean exist = false;
		try {
			exist = locationService.delete(id);
		} catch (ForeignKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(exist);
        verify(locationRepository, times(1)).deleteById(id);
	}
	
	@Test 
	public void delete_whenLocationRepositoryDoesntContainLocationWithGivenId_returnsFalse() {	
		
		UUID id = UUID.randomUUID();
		doThrow(new EmptyResultDataAccessException(1)).when(locationRepository).deleteById(id);
		boolean exist = true;
		try {
			exist = locationService.delete(id);
		} catch (ForeignKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertFalse(exist);
        verify(locationRepository, times(1)).deleteById(id);
	}
	
	@Test 
	public void delete_whenSomeEntityUsesLocationOfGivenId_throwsForeignKeyException() {	
		
		UUID id = UUID.randomUUID();
		doThrow(new DataIntegrityViolationException("Another entity is using this location!")).when(locationRepository).deleteById(id);

		assertThrows(ForeignKeyException.class, () -> locationService.delete(id));
        verify(locationRepository, times(1)).deleteById(id);
	}
	
}
