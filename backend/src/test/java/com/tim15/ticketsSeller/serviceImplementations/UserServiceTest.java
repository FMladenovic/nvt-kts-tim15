package com.tim15.ticketsSeller.serviceImplementations;

import com.tim15.ticketsSeller.DTOImplementations.NewUser;
import com.tim15.ticketsSeller.DTOImplementations.UpdateUser;
import com.tim15.ticketsSeller.DTOImplementations.UserCredentials;
import com.tim15.ticketsSeller.DTOImplementations.UserDetails;
import com.tim15.ticketsSeller.config.Mapper;
import com.tim15.ticketsSeller.domain.ConfirmationToken;
import com.tim15.ticketsSeller.domain.User;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidLoginException;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidPasswordException;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidTokenException;
import com.tim15.ticketsSeller.listener.RegistrationListener;
import com.tim15.ticketsSeller.repositoryInterfaces.ConfirmationTokenRepository;
import com.tim15.ticketsSeller.repositoryInterfaces.UserRepository;
import com.tim15.ticketsSeller.security.jwt.JwtUtil;
import com.tim15.ticketsSeller.serviceInterfaces.IFileService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static com.tim15.ticketsSeller.enumeration.Role.ROLE_USER;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class UserServiceTest {

    @SpyBean
    private Mapper mapper;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private IFileService fileService;

    @MockBean
    private ConfirmationTokenRepository confirmationTokenRepository;

    @MockBean
    private ApplicationEventPublisher eventPublisher;

    @MockBean
    private RegistrationListener registrationListener;

    @MockBean
    private AuthenticationManager authenticationManager;

    @SpyBean
    private JwtUtil jwtUtil;

    @Autowired
    private UserService userService;

    private NewUser mockNewUser;

    private User mockUser;

    private MultipartFile mockMultipartFile;

    private ConfirmationToken mockToken;

    private UpdateUser mockUpdateUser;

    private Authentication mockAuthentication;

    private UserCredentials mockUserCredentials;

    private Object[] mocks;

    @Before
    public void setUp() throws IOException {
        mockNewUser = new NewUser("username", "password", "password", "name",
                "surname", "email@user.user");

        mockUser = new User(UUID.randomUUID(), "user", "user",
                "user", "user", "email@example.ex", "", ROLE_USER);

        Path path = Paths.get("src\\test\\resources\\static\\images\\user.png");
        mockMultipartFile = new MockMultipartFile("file", Files.readAllBytes(path));

        mockToken = new ConfirmationToken(UUID.randomUUID(), "Valid token", LocalDate.now(), mockUser);

        mockUpdateUser = new UpdateUser();
        mockUpdateUser.setOldPassword("user");

        mockAuthentication = new UsernamePasswordAuthenticationToken(mockUser, null,
                Collections.singletonList(new SimpleGrantedAuthority(mockUser.getRole().toString())));

        mockUserCredentials = new UserCredentials("user", "user");

        mocks = new Object[]{userRepository, fileService, mapper,
                confirmationTokenRepository, eventPublisher, registrationListener, authenticationManager, jwtUtil};
    }

    @Test
    public void login_ValidRequest_ShouldLoginUser() {

        given(authenticationManager.authenticate(any())).willReturn(mockAuthentication);

        String token = userService.login(mockUserCredentials);

        assertNotNull(token);
        assertTrue(token.contains("Bearer"));

        verify(authenticationManager).authenticate(any());
        verify(jwtUtil).createJwt(any());
        verifyNoMoreInteractions(mocks);
    }

    @Test(expected = ConstraintViolationException.class)
    public void login_PasswordIsNotProvided_ShouldThrowException() {
        mockUserCredentials.setPassword(null);

        userService.login(mockUserCredentials);
    }

    @Test(expected = ConstraintViolationException.class)
    public void login_UsernameIsNotProvided_ShouldThrowException() {
        mockUserCredentials.setUsername(null);

        userService.login(mockUserCredentials);
    }

    @Test(expected = InvalidLoginException.class)
    public void login_BadCredentials_ShouldThrowException() {
        given(authenticationManager.authenticate(any())).willThrow(BadCredentialsException.class);

        userService.login(mockUserCredentials);
    }

    @Test
    public void registerUser_ValidUserAndValidImage_ShouldSaveUser() {

        given(fileService.uploadFile(any())).willReturn("image path");
        given(userRepository.save(any())).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        willDoNothing().given(eventPublisher).publishEvent(any());
        willDoNothing().given(registrationListener).onApplicationEvent(any());

        NewUser registeredUser = userService.registerUser(mockNewUser, mockMultipartFile);

        assertThat(registeredUser, hasProperty("email", is(mockNewUser.getEmail())));
        assertThat(registeredUser, hasProperty("password", is(mockNewUser.getPassword())));
        assertThat(registeredUser, hasProperty("username", is(mockNewUser.getUsername())));
        assertThat(registeredUser, hasProperty("name", is(mockNewUser.getName())));
        assertThat(registeredUser, hasProperty("surname", is(mockNewUser.getSurname())));

        verify(userRepository).save(any());
        verify(fileService).uploadFile(any());
        verify(registrationListener).onApplicationEvent(any());
        verify(mapper, times(2)).map(any(), any());
        verifyNoMoreInteractions(mocks);
    }

    /*** Validate data in request ***/

    @Test(expected = ValidationException.class)
    public void registerUser_InvalidPassword_ShouldThrowValidationException() {
        mockNewUser.setPassword(null);

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_InvalidConfirmPassword_ShouldThrowConstraintViolationException() {
        mockNewUser.setConfirmPassword("different password");

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_BlankConfirmPassword_ShouldThrowConstraintViolationException() {
        mockNewUser.setConfirmPassword(null);

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_InvalidEmail_ShouldThrowConstraintViolationException() {
        mockNewUser.setEmail("invalid email");

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_BlankEmail_ShouldThrowConstraintViolationException() {
        mockNewUser.setEmail(null);

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_InvalidUsername_ShouldThrowConstraintViolationException() {
        mockNewUser.setUsername(null);

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_InvalidName_ShouldThrowConstraintViolationException() {
        mockNewUser.setName(null);

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_InvalidSurname_ShouldThrowConstraintViolationException() {
        mockNewUser.setSurname(null);

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test
    public void confirmUserAccount_ValidToken_ShouldConfirmUserAccount() {

        given(confirmationTokenRepository.findByToken("Valid token")).willReturn(Optional.of(mockToken));
        given(userRepository.findById(any())).willReturn(Optional.ofNullable(mockUser));
        given(userRepository.save(any())).willReturn(mockUser);
        willDoNothing().given(confirmationTokenRepository).deleteById(any());

        userService.confirmUserAccount("Valid token");

        verify(confirmationTokenRepository).findByToken(anyString());
        verify(userRepository).findById(any(UUID.class));
        verify(userRepository).save(any());
        verify(confirmationTokenRepository).deleteById(any());
        verifyNoMoreInteractions(mocks);

    }

    @Test(expected = EntityNotFoundException.class)
    public void confirmUserAccount_TokenDoesNotExist_ShouldThrowEntityNotFoundException() {
        given(confirmationTokenRepository.findByToken("Invalid token")).willThrow(EntityNotFoundException.class);

        userService.confirmUserAccount("Invalid token");
    }

    @Test(expected = EntityNotFoundException.class)
    public void confirmUserAccount_UserDoesNotExist_ShouldThrowEntityNotFoundException() {
        given(confirmationTokenRepository.findByToken(anyString())).willReturn(Optional.of(mockToken));
        given(userRepository.findById(any())).willThrow(EntityNotFoundException.class);

        userService.confirmUserAccount("Token");
    }

    @Test(expected = InvalidTokenException.class)
    public void confirmUserAccount_TokenHasExpired_ShouldThrowInvalidTokenException() {
        mockToken.setCreatedDate(LocalDate.now().minusDays(3));

        given(confirmationTokenRepository.findByToken(anyString())).willReturn(Optional.of(mockToken));

        userService.confirmUserAccount("Expired token");
    }

    @Test(expected = IllegalArgumentException.class)
    public void confirmUserAccount_NullUser_ShouldThrowNullPointerException() {
        mockToken.setUser(null);

        given(confirmationTokenRepository.findByToken(anyString())).willReturn(Optional.of(mockToken));

        userService.confirmUserAccount("Token");
    }

    @Test
    public void updateUser_UpdatedName_ShouldUpdateUser() {
        mockUpdateUser.setName("New Name");
        given(userRepository.findByUsername(any())).willReturn(Optional.of(mockUser));

        UpdateUser updatedUser = userService.updateUser(mockUpdateUser, null, mockAuthentication);

        assertNotNull(updatedUser);
        assertThat(updatedUser, hasProperty("name", is(mockUpdateUser.getName())));

        assertThat(updatedUser, hasProperty("surname", is(mockUser.getSurname())));
        assertThat(updatedUser, hasProperty("password", is(mockUser.getPassword())));

        verify(userRepository).findByUsername(any());
        verify(mapper).map(any(UpdateUser.class), any(User.class));
        verify(userRepository).save(any());
        verify(mapper).map(any(), any());
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void updateUser_UpdatedSurname_ShouldUpdateUser() {
        mockUpdateUser.setSurname("New Surname");
        given(userRepository.findByUsername(any())).willReturn(Optional.of(mockUser));

        UpdateUser updatedUser = userService.updateUser(mockUpdateUser, null, mockAuthentication);

        assertNotNull(updatedUser);
        assertThat(updatedUser, hasProperty("surname", is(mockUpdateUser.getSurname())));

        assertThat(updatedUser, hasProperty("name", is(mockUser.getName())));
        assertThat(updatedUser, hasProperty("password", is(mockUser.getPassword())));

        verify(userRepository).findByUsername(any());
        verify(mapper).map(any(UpdateUser.class), any(User.class));
        verify(userRepository).save(any());
        verify(mapper).map(any(), any());
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void updateUser_UpdatedPassword_ShouldUpdateUser() {
        mockUpdateUser.setPassword("password password");
        mockUpdateUser.setConfirmPassword("password password");

        given(userRepository.findByUsername(any())).willReturn(Optional.of(mockUser));

        UpdateUser updatedUser = userService.updateUser(mockUpdateUser, null, mockAuthentication);

        assertNotNull(updatedUser);
        assertThat(updatedUser, hasProperty("password", is(mockUpdateUser.getPassword())));

        assertThat(updatedUser, hasProperty("name", is(mockUser.getName())));
        assertThat(updatedUser, hasProperty("surname", is(mockUser.getSurname())));

        verify(userRepository).findByUsername(any());
        verify(mapper).map(any(UpdateUser.class), any(User.class));
        verify(userRepository).save(any());
        verify(mapper).map(any(), any());
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void updateUser_UpdatedImage_ShouldUpdateUser() {
        given(userRepository.findByUsername(any())).willReturn(Optional.of(mockUser));
        given(fileService.uploadFile(mockMultipartFile)).willReturn("file path");

        UpdateUser updatedUser = userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);

        assertNotNull(updatedUser);

        verify(userRepository).findByUsername(any());
        verify(mapper).map(any(UpdateUser.class), any(User.class));
        verify(fileService).uploadFile(any());
        verify(userRepository).save(any());
        verify(mapper).map(any(), any());
        verifyNoMoreInteractions(mocks);
    }

    @Test
    public void updateUser_UpdateAll_ShouldUpdateUser() {
        mockUpdateUser.setName("New name");
        mockUpdateUser.setSurname("New surname");
        mockUpdateUser.setPassword("password password");
        mockUpdateUser.setConfirmPassword("password password");

        given(userRepository.findByUsername(any())).willReturn(Optional.of(mockUser));
        given(fileService.uploadFile(mockMultipartFile)).willReturn("file path");

        UpdateUser updatedUser = userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);

        assertNotNull(updatedUser);
        assertThat(updatedUser, hasProperty("password", is(mockUpdateUser.getPassword())));
        assertThat(updatedUser, hasProperty("name", is(mockUpdateUser.getName())));
        assertThat(updatedUser, hasProperty("surname", is(mockUpdateUser.getSurname())));

        verify(userRepository).findByUsername(any());
        verify(mapper).map(any(UpdateUser.class), any(User.class));
        verify(fileService).uploadFile(any());
        verify(userRepository).save(any());
        verify(mapper).map(any(), any());
        verifyNoMoreInteractions(mocks);
    }

    @Test(expected = AccessDeniedException.class)
    public void updateUser_UserNotLoggedIn_ShouldThrowAccessDeniedException() {
        userService.updateUser(mockUpdateUser, mockMultipartFile, null);
    }

    @Test(expected = AccessDeniedException.class)
    public void updateUser_UserDoesNotExist_ShouldThrowEntityNotFoundException() {
        given(userRepository.findByUsername(any())).willThrow(EntityNotFoundException.class);

        userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);
    }

    @Test(expected = InvalidPasswordException.class)
    public void updateUser_InvalidOldPassword_ShouldThrowInvalidPasswordException() {
        mockUpdateUser.setOldPassword("wrong old password");
        given(userRepository.findByUsername(any())).willReturn(Optional.of(mockUser));

        userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);
    }

    @Test(expected = ConstraintViolationException.class)
    public void updateUser_ConfirmPasswordInvalid_ShouldThrowConstraintViolationException() {
        mockUpdateUser.setPassword("pass");
        mockUpdateUser.setConfirmPassword("pass ");

        userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);
    }

    @Test(expected = ConstraintViolationException.class)
    public void updateUser_PasswordShort_ShouldThrowConstraintViolationException() {
        mockUpdateUser.setPassword("pass");

        userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);
    }

    @Test(expected = ConstraintViolationException.class)
    public void updateUser_ConfirmPasswordShort_ShouldThrowConstraintViolationException() {
        mockUpdateUser.setPassword("new password");
        mockUpdateUser.setConfirmPassword("pass");

        userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);
    }

    @Test
    public void getLoggedUser_ValidRequest_ShouldReturnLoggedUser() {

        given(userRepository.findByUsername(anyString())).willReturn(Optional.of(mockUser));

        UserDetails userDetails = userService.getLoggedUser(mockAuthentication);

        assertEquals(userDetails.getEmail(), ((User) mockAuthentication.getPrincipal()).getEmail());
        assertEquals(userDetails.getUsername(), ((User) mockAuthentication.getPrincipal()).getUsername());
        assertEquals(userDetails.getName(), ((User) mockAuthentication.getPrincipal()).getName());
        assertEquals(userDetails.getSurname(), ((User) mockAuthentication.getPrincipal()).getSurname());

        verify(userRepository).findByUsername(anyString());
        verify(mapper).map(any(), any());
        verifyNoMoreInteractions(mocks);
    }

    @Test(expected = AccessDeniedException.class)
    public void getLoggedUser_UserDoesNotExist_ShouldThrowException() {
        given(userRepository.findByUsername(anyString())).willThrow(EntityNotFoundException.class);

        userService.getLoggedUser(mockAuthentication);
    }

    @Test(expected = AccessDeniedException.class)
    public void getLoggedUser_UserIsNotLoggedIn_ShouldThrowException() {
        Authentication mockAuthentication =
                new UsernamePasswordAuthenticationToken("anonymous", "anonymous", null);

        userService.getLoggedUser(mockAuthentication);
    }
}
