package com.tim15.ticketsSeller.serviceImplementations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.tim15.ticketsSeller.domain.Configuration;
import com.tim15.ticketsSeller.repositoryInterfaces.ConfigurationMongoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ConfigurationServiceTest {

	
	@MockBean
	private ConfigurationMongoRepository configurationRepository;
	
	@Autowired 
	private ConfigurationService configurationService;
	
	private ArrayList<Configuration> setTest(){
		ArrayList<Configuration> test = new ArrayList<Configuration>();
		test.add(new Configuration());
		test.add(new Configuration());
		test.add(new Configuration());
		test.add(new Configuration());
		test.add(new Configuration());
		return test;
	}
	
	@Test
	public void get_whenConfigurationRepositoryDoesntHaveEntityToReturn_returns0Entities() {
		when(configurationRepository.findAll()).thenReturn(new ArrayList<Configuration>());

		Iterable<Configuration> configurations = configurationService.get();
		
		assertEquals(StreamSupport.stream(configurations.spliterator(), false).count(), 0);
	}
	
	@Test
	public void get_whenConfigurationRepositoryHasEntityToReturn_returnsAllEntities() {
		List<Configuration> test = this.setTest();
		when(configurationRepository.findAll()).thenReturn(test);

		Iterable<Configuration> configurations = configurationService.get();
		
		assertEquals(StreamSupport.stream(configurations.spliterator(), false).count(), test.size());
	}

	
	@Test 
	public void getById_whenConfiguratioRepositoryDoesntHaveEntityWithGivenId_returnsNull() {
		int id = 2;
		when(configurationRepository.findById(id)).thenReturn(Optional.empty());	
		
		Configuration configuration = configurationService.getById(id);
		
		assertNull(configuration);
    }
	
	@Test 
	public void getById_whenConfiguratioRepositoryHasEntityWithGivenId_returnsEntity() {
		Configuration testConfiguration = new Configuration();
		int id = 2;
		testConfiguration.setId(id);
		
		
		when(configurationRepository.findById(id)).thenReturn(Optional.of(testConfiguration));	
		
		Configuration configuration = configurationService.getById(id);
		
		assertNotNull(configuration);
		assertEquals(configuration.getId(), id);
    }


}
