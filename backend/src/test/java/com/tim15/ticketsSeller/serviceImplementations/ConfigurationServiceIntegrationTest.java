package com.tim15.ticketsSeller.serviceImplementations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.stream.StreamSupport;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.tim15.ticketsSeller.domain.Configuration;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:application-test.properties")
@ActiveProfiles("test")
public class ConfigurationServiceIntegrationTest {
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Test
	public void getAll() {
		Iterable<Configuration> configurations = configurationService.get();
		assertTrue(StreamSupport.stream(configurations.spliterator(), false).count() >= 1);
	}
	
	@Test
	public void getById() {
		int id = 0;
		Configuration configuration = configurationService.getById(id);
		assertEquals(configuration.getId(), id);
	}

}
