package com.tim15.ticketsSeller.serviceImplementations;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.StreamSupport;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.tim15.ticketsSeller.DTOImplementations.NewCategories;
import com.tim15.ticketsSeller.domain.Categories;
import com.tim15.ticketsSeller.domain.Category;
import com.tim15.ticketsSeller.repositoryInterfaces.CategoriesMongoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:application-test.properties")
@ActiveProfiles("test")
public class CategoriesServiceIntegrationTest {
	
	@Autowired
	private CategoriesService categoriesService;
	
	@Autowired
	private CategoriesMongoRepository categoriesRepository;
	
	@SuppressWarnings("serial")
	@Before
	public final void setUp()  {
		UUID id1 = UUID.fromString("6aa5ac20-e20e-4e34-873a-8283904fda39");
		UUID id2 = UUID.fromString("228c132f-5355-4f90-8fac-9cdaa8d7e869");
		UUID id3 = UUID.fromString("d5a96313-d390-4862-94a0-8c6770e82c83");
		UUID id4 = UUID.fromString("c0fa08f3-057e-474a-af54-3dc976f3e084");
		List<String> layout = new ArrayList<String>() {
			{
				add("aaaaa");
				add("a_p_a"); 
                add("a__pa"); 
                add("ap__a"); 
				add("aaaaa");
			}
		};
		Category c1 = new Category();
		c1.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("_p___"); 
	                add("_____"); 
	                add("___p_"); 
					add("_____");
				}
			}
		);
		c1.setParterCapacities(new ArrayList<Integer>() {
			{
				add(100);
				add(null);
				add(100);
			}
		});
		
		
		Category c2 = new Category();
		c2.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("_____"); 
	                add("___p_"); 
	                add("_____"); 
					add("_____");
				}
			}
		);
		c2.setParterCapacities(new ArrayList<Integer>() {
			{
				add(null);
				add(200);
				add(null);
			}
		});
		Category c3 = new Category();
		c3.setSeats(new ArrayList<String>(){
				{
					add("aaaaa");
					add("a___a"); 
	                add("a___a"); 
	                add("a___a"); 
					add("aaaaa");
				}
			}
		);
		
		List<Category> categories = new ArrayList<Category>() {
			{
				add(c1);
				add(c2);
				add(c3);
			}
		};
		
		List<Integer> parterCapacities = new ArrayList<Integer>() {
			{
				add(100);
				add(200);
				add(100);
			}
		};
		
		List<Categories> listCategories = new ArrayList<Categories>();
		listCategories.add(new Categories(id1, layout, parterCapacities, categories));
		listCategories.add(new Categories(id2, layout, parterCapacities, categories));
		listCategories.add(new Categories(id3, layout, parterCapacities, categories));
		listCategories.add(new Categories(id4, layout, parterCapacities, categories));
		
		for(Categories cat : listCategories) {
			categoriesRepository.save(cat);
		}
    }

	@After
	public final void tearDown() {
		UUID id1 = UUID.fromString("6aa5ac20-e20e-4e34-873a-8283904fda39");
		UUID id2 = UUID.fromString("228c132f-5355-4f90-8fac-9cdaa8d7e869");
		UUID id3 = UUID.fromString("d5a96313-d390-4862-94a0-8c6770e82c83");
		UUID id4 = UUID.fromString("c0fa08f3-057e-474a-af54-3dc976f3e084");
		List<UUID> ids = new ArrayList<UUID>();
		ids.add(id1);
		ids.add(id2);
		ids.add(id3);
		ids.add(id4);
		
		for(UUID id : ids) {
			categoriesRepository.deleteById(id);
		}
	}
	
	@Test
	public void getAll() {
		Iterable<Categories> categories = categoriesService.getAll();
		assertTrue(StreamSupport.stream(categories.spliterator(), false).count() >= 4);
	}
	
	@Test
	public void getById() {
		UUID id = UUID.fromString("6aa5ac20-e20e-4e34-873a-8283904fda39");
		Categories categories = categoriesService.getById(id);
		assertEquals(categories.getId(), id);
	}
	
	@Test
	public void post() {
		UUID id = UUID.fromString("6aa5ac20-e20e-4e34-873a-8283904fda39");
		Categories categories = categoriesService.getById(id);
		NewCategories newCat = new NewCategories(categories.getLayout(), categories.getParterCapacities(), categories.getCategories());
		UUID newId = null;

		newId  = categoriesService.post(newCat);
		
		assertNotNull(newId);
		Categories newCategories = categoriesService.getById(newId);
		assertEquals(newCategories.getId(), newId);
		
		categoriesRepository.deleteById(newId);

	}

	@Test
	public void delete(){
		UUID id = UUID.fromString("6aa5ac20-e20e-4e34-873a-8283904fda39");

		categoriesService.delete(id);

		assertFalse(categoriesRepository.findById(id).isPresent());
	}
}
