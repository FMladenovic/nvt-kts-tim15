package com.tim15.ticketsSeller.serviceImplementations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.StreamSupport;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.JsonIOException;
import com.tim15.ticketsSeller.DTO.INewCategories;
import com.tim15.ticketsSeller.DTOImplementations.NewCategories;
import com.tim15.ticketsSeller.domain.Categories;
import com.tim15.ticketsSeller.domain.Category;
import com.tim15.ticketsSeller.repositoryInterfaces.CategoriesMongoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CategoriesServiceTest {
	
	@MockBean
	private CategoriesMongoRepository categoriesRepository;
	
	@Autowired
	private CategoriesService categoriesService;
	
	
	private ArrayList<Categories> setTest(){
		ArrayList<Categories> test = new ArrayList<Categories>();
		test.add(new Categories());
		test.add(new Categories());
		test.add(new Categories());
		test.add(new Categories());
		test.add(new Categories());
		return test;
	}
	
	@Test
	public void getAll_whenCategoriesRepositoryDoesntHaveEntityToReturn_returns0Entities() {
		when(categoriesRepository.findAll()).thenReturn(new ArrayList<Categories>());

		Iterable<Categories> categories = categoriesService.getAll();
		
		assertEquals(StreamSupport.stream(categories.spliterator(), false).count(), 0);
	}
	
	@Test
	public void getAll_whenCategoriesRepositoryHasEntityToReturn_returnsAllEntities() {
		List<Categories> test = this.setTest();
		when(categoriesRepository.findAll()).thenReturn(test);

		Iterable<Categories> categories = categoriesService.getAll();
		
		assertEquals(StreamSupport.stream(categories.spliterator(), false).count(), test.size());
	}

	
	@Test 
	public void getById_whenCategoriesRepositoryDoesntHaveEntityWithGivenId_returnsNull() {
		UUID id = UUID.randomUUID();
		when(categoriesRepository.findById(id)).thenReturn(Optional.empty());	
		
		Categories categories = categoriesService.getById(id);
		
		assertNull(categories);
    }
	
	@Test 
	public void getById_whenCategoriesRepositoryHasEntityWithGivenId_returnsEntity() {
		Categories testCategories = new Categories();
		UUID id = UUID.randomUUID();
		testCategories.setId(id);
		Optional<Categories> test = Optional.of(testCategories);
		
		
		when(categoriesRepository.findById(id)).thenReturn(test);	
		
		Categories categories = categoriesService.getById(id);
		
		assertNotNull(categories);
		assertEquals(categories.getId(), id);
    }
	
	@Test
	public void post_whenParamIsNull_throwsIllegalArgumentException() {
		assertThrows(IllegalArgumentException.class, ()->categoriesService.post(null));
	}
	
	@Test
	@SuppressWarnings("serial")
	public void post_whenParamIsValid_returnsId() throws JsonIOException, IOException {

		INewCategories newCategories = mock(NewCategories.class);
		List<String> layout = new ArrayList<String>() {
			{
				add("aaaaa");
				add("a_p_a"); 
                add("a__pa"); 
                add("ap__a"); 
				add("aaaaa");
			}
		};
		Category c1 = new Category();
		c1.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("_p___"); 
	                add("_____"); 
	                add("___p_"); 
					add("_____");
				}
			}
		);
		c1.setParterCapacities(new ArrayList<Integer>() {
			{
				add(100);
				add(null);
				add(100);
			}
		});
		
		
		Category c2 = new Category();
		c2.setSeats(new ArrayList<String>(){
				{
					add("_____");
					add("_____"); 
	                add("___p_"); 
	                add("_____"); 
					add("_____");
				}
			}
		);
		c2.setParterCapacities(new ArrayList<Integer>() {
			{
				add(null);
				add(200);
				add(null);
			}
		});
		Category c3 = new Category();
		c3.setSeats(new ArrayList<String>(){
				{
					add("aaaaa");
					add("a___a"); 
	                add("a___a"); 
	                add("a___a"); 
					add("aaaaa");
				}
			}
		);
		
		List<Category> categories = new ArrayList<Category>() {
			{
				add(c1);
				add(c2);
				add(c3);
			}
		};
		
		List<Integer> parterCapacities = new ArrayList<Integer>() {
			{
				add(100);
				add(200);
				add(100);
			}
		};
		
		
        when(newCategories.getLayout()).thenReturn(layout);
        when(newCategories.getParterCapacities()).thenReturn(parterCapacities);
        when(newCategories.getCategories()).thenReturn(categories);
        UUID id = null;

		id = categoriesService.post(newCategories);
		
        verify(categoriesRepository, times(1)).save(isA(Categories.class));
        assertNotNull(id);
	
	}
	
	
}
