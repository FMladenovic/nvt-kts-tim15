package com.tim15.ticketsSeller.serviceImplementations;

import com.tim15.ticketsSeller.DTOImplementations.NewUser;
import com.tim15.ticketsSeller.DTOImplementations.UpdateUser;
import com.tim15.ticketsSeller.DTOImplementations.UserCredentials;
import com.tim15.ticketsSeller.DTOImplementations.UserDetails;
import com.tim15.ticketsSeller.domain.User;
import com.tim15.ticketsSeller.enumeration.Role;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidLoginException;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidPasswordException;
import com.tim15.ticketsSeller.exception.customExceptions.InvalidTokenException;
import com.tim15.ticketsSeller.repositoryInterfaces.UserRepository;
import com.tim15.ticketsSeller.security.jwt.JwtUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
public class UserServiceIntegrationTest {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtUtil jwtUtil;

    private NewUser mockNewUser;

    private MultipartFile mockMultipartFile;

    private Authentication mockAuthentication;

    private UpdateUser mockUpdateUser;

    private User mockLoggedUser;

    private UserCredentials mockUserCredentials;

    private Set<String> initialFiles;

    @Before
    public void setUp() throws Exception {
        mockNewUser = new NewUser("username", "password", "password", "name",
                "surname", "email@user.user");

        Path imagePath = Paths.get("src\\test\\resources\\static\\images\\user.png");
        mockMultipartFile = new MockMultipartFile("user", "user.png", "", Files.readAllBytes(imagePath));

        mockLoggedUser = userRepository.findByUsername("admin").orElseThrow(EntityNotFoundException::new);
        mockAuthentication = new UsernamePasswordAuthenticationToken(mockLoggedUser, Collections.singletonList(mockLoggedUser.getRole()));

        mockUpdateUser = new UpdateUser();
        mockUpdateUser.setOldPassword("admin");

        mockUserCredentials = new UserCredentials("admin", "admin");

        initialFiles = getFiles();
    }

    @After
    public void tearDown() throws Exception {
        getFiles().forEach(path -> {
            File file = new File(path);
            if (file.exists() && !initialFiles.contains(path)) {
                file.delete();
            }
        });

        //set updated user to original state
        userRepository.save(mockLoggedUser);
    }

    private Set<String> getFiles() throws IOException {
        Set<String> allFiles = new HashSet<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get("src\\test\\resources\\static\\images\\"))) {
            stream.forEach(path -> {
                if (!Files.isDirectory(path)) {
                    allFiles.add(path.toString());
                }
            });
        }

        return allFiles;
    }

    @Test
    public void login_ValidRequest_ShouldLoginUser() {

        String token = userService.login(mockUserCredentials);

        assertNotNull(token);
        assertTrue(token.contains("Bearer"));
    }

    @Test(expected = ConstraintViolationException.class)
    public void login_PasswordIsNotProvided_ShouldThrowException() {
        mockUserCredentials.setPassword(null);

        userService.login(mockUserCredentials);
    }

    @Test(expected = ConstraintViolationException.class)
    public void login_UsernameIsNotProvided_ShouldThrowException() {
        mockUserCredentials.setUsername(null);

        userService.login(mockUserCredentials);
    }

    @Test(expected = InvalidLoginException.class)
    public void login_BadCredentials_ShouldThrowException() {
        mockUserCredentials.setUsername("bad username");

        userService.login(mockUserCredentials);
    }

    @Test
    public void registerUser_ValidUserAndValidImage_ShouldSaveUser() {

        NewUser registeredUser = userService.registerUser(mockNewUser, mockMultipartFile);

        int sizeOfImages = 0;
        try {
            sizeOfImages = getFiles().size();
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertThat(registeredUser, hasProperty("email", is(mockNewUser.getEmail())));
        assertThat(registeredUser, hasProperty("password", is(mockNewUser.getPassword())));
        assertThat(registeredUser, hasProperty("username", is(mockNewUser.getUsername())));
        assertThat(registeredUser, hasProperty("name", is(mockNewUser.getName())));
        assertThat(registeredUser, hasProperty("surname", is(mockNewUser.getSurname())));

        User user = userRepository.findByUsername(registeredUser.getUsername()).orElseThrow(EntityNotFoundException::new);
        //saved to database
        assertNotNull(user);
        //added profile image
        assertNotNull(user.getImagePath());
        assertEquals(sizeOfImages, initialFiles.size() + 1);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_BlankUsername_ShouldThrowConstraintViolationException() {
        mockNewUser.setUsername("");

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_BlankPassword_ShouldThrowConstraintViolationException() {
        mockNewUser.setPassword("");

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_ShortPassword_ShouldThrowConstraintViolationException() {
        mockNewUser.setPassword("pass");

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_BlankConfirmPassword_ShouldThrowConstraintViolationException() {
        mockNewUser.setConfirmPassword("");

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_ShortConfirmPassword_ShouldThrowConstraintViolationException() {
        mockNewUser.setConfirmPassword("pass");

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_DifferentPasswords_ShouldThrowConstraintViolationException() {
        mockNewUser.setConfirmPassword("different password");

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_BlankName_ShouldThrowConstraintViolationException() {
        mockNewUser.setName("");

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_BlankSurname_ShouldThrowConstraintViolationException() {
        mockNewUser.setSurname("");

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_BlankEmail_ShouldThrowConstraintViolationException() {
        mockNewUser.setEmail("");

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

    @Test(expected = ConstraintViolationException.class)
    public void registerUser_BadFormedEmail_ShouldThrowConstraintViolationException() {
        mockNewUser.setEmail("email.example");

        userService.registerUser(mockNewUser, mockMultipartFile);
    }

//    @Test
//    public void confirmUserAccount_ValidRequest_ShouldConfirmUser() {
//
//        assertFalse(userRepository.findByUsername("new user").orElseThrow(EntityNotFoundException::new).getEnabled());
//
//        userService.confirmUserAccount("token");
//
//        assertTrue(userRepository.findByUsername("new user").orElseThrow(EntityNotFoundException::new).getEnabled());
//    }

    @Test(expected = EntityNotFoundException.class)
    public void confirmUserAccount_TokenDoesNotExist_ShouldThrowException() {
        userService.confirmUserAccount("Token does not exist");
    }

    @Test(expected = IllegalArgumentException.class)
    public void confirmUserAccount_UserNull_ShouldThrowException() {
        userService.confirmUserAccount("invalid token");
    }

    @Test(expected = InvalidTokenException.class)
    public void confirmUserAccount_TokenExpired_ShouldThrowException() {
        userService.confirmUserAccount("expired token");
    }

    @Test
    public void updateUser_UpdateName_ShouldUpdateUser() {
        String newName = "New Name";
        assertNotEquals(newName, userRepository.findByUsername("admin").orElseThrow(EntityNotFoundException::new).getName());
        mockUpdateUser.setName(newName);

        UpdateUser updatedUser = userService.updateUser(mockUpdateUser, null, mockAuthentication);

        assertThat(updatedUser, hasProperty("name", is(mockUpdateUser.getName())));
        assertThat(userRepository.findByUsername("admin").orElseThrow(EntityNotFoundException::new).getName(), is(equalTo(newName)));
    }

    @Test
    public void updateUser_UpdateSurname_ShouldUpdateUser() {
        String newSurname = "New Surname";
        assertNotEquals(newSurname, userRepository.findByUsername("admin").orElseThrow(EntityNotFoundException::new).getSurname());

        mockUpdateUser.setSurname(newSurname);

        UpdateUser updatedUser = userService.updateUser(mockUpdateUser, null, mockAuthentication);

        assertThat(updatedUser, hasProperty("surname", is(mockUpdateUser.getSurname())));
        assertThat(userRepository.findByUsername("admin").orElseThrow(EntityNotFoundException::new).getSurname(), is(equalTo(newSurname)));
    }

    @Test
    public void updateUser_UpdatePassword_ShouldUpdateUser() {
        String newPassword = "new password";
        assertNotEquals(newPassword, userRepository.findByUsername("admin").orElseThrow(EntityNotFoundException::new).getPassword());


        mockUpdateUser.setPassword(newPassword);
        mockUpdateUser.setConfirmPassword(newPassword);

        UpdateUser updatedUser = userService.updateUser(mockUpdateUser, null, mockAuthentication);

        assertThat(updatedUser, hasProperty("password", is(mockUpdateUser.getPassword())));
        assertThat(userRepository.findByUsername("admin").orElseThrow(EntityNotFoundException::new).getPassword(), is(equalTo(newPassword)));
    }

    @Test
    public void updateUser_UpdateImage_ShouldUpdateUser() {
        assertTrue(userRepository.findByUsername("admin").orElseThrow(EntityNotFoundException::new).getImagePath().contains("admin"));

        userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);

        assertTrue(userRepository.findByUsername("admin").orElseThrow(EntityNotFoundException::new).getImagePath().contains(mockMultipartFile.getName()));
    }

    @Test
    public void updateUser_UpdateAll_ShouldUpdateUser() {
        String newValue = "new value";
        mockUpdateUser.setName(newValue);
        mockUpdateUser.setSurname(newValue);
        mockUpdateUser.setPassword(newValue);
        mockUpdateUser.setConfirmPassword(newValue);

        User userBeforeUpdate = userRepository.findByUsername("admin").orElseThrow(EntityNotFoundException::new);
        assertTrue(userBeforeUpdate.getImagePath().contains("admin"));

        UpdateUser updatedUser = userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);

        User userAfterUpdate = userRepository.findByUsername("admin").orElseThrow(EntityNotFoundException::new);

        assertNotEquals(userBeforeUpdate, userAfterUpdate);
        assertEquals(updatedUser.getName(), userAfterUpdate.getName());
        assertEquals(updatedUser.getSurname(), userAfterUpdate.getSurname());
        assertEquals(updatedUser.getPassword(), userAfterUpdate.getPassword());
        assertTrue(userAfterUpdate.getImagePath().contains(mockMultipartFile.getName()));
    }

    @Test(expected = AccessDeniedException.class)
    public void updateUser_UserNotLoggedIn_ShouldThrowAccessDeniedException() {
        userService.updateUser(mockUpdateUser, mockMultipartFile, null);
    }

    @Test(expected = InvalidPasswordException.class)
    public void updateUser_OldPasswordIsNotValid_ShouldThrowInvalidPasswordException() {
        mockUpdateUser.setOldPassword("invalid password");

        userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);
    }

    /*validation for data in update user request*/

    @Test(expected = ConstraintViolationException.class)
    public void updateUser_ShortPassword_ShouldThrowConstraintViolationException() {
        mockUpdateUser.setPassword("pass");

        userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);
    }

    @Test(expected = ConstraintViolationException.class)
    public void updateUser_ShortConfirmPassword_ShouldThrowConstraintViolationException() {
        mockUpdateUser.setConfirmPassword("pass");

        userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);
    }

    @Test(expected = ConstraintViolationException.class)
    public void updateUser_InvalidConfirmPassword_ShouldThrowConstraintViolationException() {
        mockUpdateUser.setPassword("new password");
        mockUpdateUser.setConfirmPassword("wrong confirm password");

        userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);
    }

    @Test(expected = ConstraintViolationException.class)
    public void updateUser_BlankOldPassword_ShouldThrowConstraintViolationException() {
        mockUpdateUser.setOldPassword("");

        userService.updateUser(mockUpdateUser, mockMultipartFile, mockAuthentication);
    }

    @Test
    public void getLoggedUser_ValidRequest_ShouldReturnLoggedUser() {

        UserDetails userDetails = userService.getLoggedUser(mockAuthentication);

        assertEquals(userDetails.getEmail(), ((User) mockAuthentication.getPrincipal()).getEmail());
        assertEquals(userDetails.getUsername(), ((User) mockAuthentication.getPrincipal()).getUsername());
        assertEquals(userDetails.getName(), ((User) mockAuthentication.getPrincipal()).getName());
        assertEquals(userDetails.getSurname(), ((User) mockAuthentication.getPrincipal()).getSurname());

    }

    @Test(expected = AccessDeniedException.class)
    public void getLoggedUser_UserDoesNotExist_ShouldThrowException() {
        User mockInvalidUser = new User();
        mockInvalidUser.setUsername("user does not exist in db");

        Authentication mockAuthentication =
                new UsernamePasswordAuthenticationToken(mockInvalidUser, Collections.singletonList(Role.ROLE_USER));

        userService.getLoggedUser(mockAuthentication);
    }

    @Test(expected = AccessDeniedException.class)
    public void getLoggedUser_UserIsNotLoggedIn_ShouldThrowException() {
        Authentication mockAuthentication =
                new UsernamePasswordAuthenticationToken("anonymous", "anonymous", null);

        userService.getLoggedUser(mockAuthentication);
    }
}
