package com.tim15.ticketsSeller.serviceImplementations;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.StreamSupport;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.tim15.ticketsSeller.DTOImplementations.NewLocation;
import com.tim15.ticketsSeller.domain.Location;
import com.tim15.ticketsSeller.exception.customExceptions.ForeignKeyException;
import com.tim15.ticketsSeller.exception.customExceptions.UniqueViolationException;
import com.tim15.ticketsSeller.repositoryInterfaces.LocationRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:application-test.properties")
@ActiveProfiles("test")
public class LocationServiceIntegrationTest {
	
	@Autowired
	private LocationService locationService;
	
	@Autowired
	private LocationRepository locationRepository;
	
	@Before
	public final void setUp() {
		UUID id1 = UUID.fromString("0fbde643-18ca-458f-b7ce-092d061cbd58");
		UUID id2 = UUID.fromString("e8b6162d-d404-47d4-a8bf-0a20ef60e0c9");
		
		Location location1 = new Location(id1, "Location1", 0);
		Location location2 = new Location(id2, "Location2", 0);
		List<Location> locations = new ArrayList<Location>();
		locations.add(location1);
		locations.add(location2);
		locationRepository.saveAll(locations);
	}
	
	@After
	public final void tearDown() {
		locationRepository.deleteAll();
	}
	
	@Test
	public void getAll() {
		Iterable<Location> locations = locationService.getAll();
		assertTrue(StreamSupport.stream(locations.spliterator(), false).count() >= 2);
	}

	@Test
	public void getById() {
		UUID id = UUID.fromString("0fbde643-18ca-458f-b7ce-092d061cbd58");
		Location location = locationService.getById(id);
		assertEquals(location.getId(), id);
	}
	
	@Test
	public void post() throws IllegalArgumentException, NoSuchElementException, UniqueViolationException {
		String name = "NewName";
		int configurationId = 0;
		boolean active = true;
		NewLocation newLocation = new NewLocation(name, configurationId, active);
		UUID id = locationService.post(newLocation);
		Location location = locationService.getById(id);
		assertEquals(location.getId(), id);
		assertEquals(location.getName(), name);
		assertEquals(location.getConfigurationId(), configurationId);
		assertEquals(location.isActive(), active);
	}

	@Test
	public void put() throws IllegalArgumentException, NoSuchElementException, UniqueViolationException {
		UUID id = UUID.fromString("0fbde643-18ca-458f-b7ce-092d061cbd58");
		String newName = "some other name";
		int newConfigurationId = 0;
		boolean newActive = true;
		NewLocation newLocation = new NewLocation(newName, newConfigurationId, newActive);
		
		locationService.put(id, newLocation);
		Location location = locationService.getById(id);
		assertEquals(location.getId(), id);
		assertEquals(location.getName(), newName);
		assertEquals(location.getConfigurationId(), newConfigurationId);
		assertEquals(location.isActive(), newActive);
	}
	
	public void delete() throws ForeignKeyException {
		UUID id = UUID.fromString("0fbde643-18ca-458f-b7ce-092d061cbd58");
		locationService.delete(id);
		Location location = locationService.getById(id);
		assertNull(location);
	}
}
