insert into user(id, name, surname, username, password, email, role, image_path, is_enabled)
values ('faeec839-9df4-4e58-83f7-048783a7052d', 'admin', 'admin', 'admin', 'admin', 'admin@ex.ex',
'ROLE_ADMIN', 'src\\test\\resources\\static\\images\\admin.png', 1);

insert into user(id, name, surname, username, password, email, role, image_path, is_enabled)
values ('d55134db-a3ea-40ad-9d50-b6c2354a1e81', 'user', 'user', 'user', 'user', 'user@ex.ex',
'ROLE_USER', 'src\\test\\resources\\static\\images\\user.png', 1);

insert into user(id, name, surname, username, password, email, role, image_path, is_enabled)
values ('faeec839-9df4-4e58-83f7-048783a70523', 'new user', 'new user', 'new user', 'password1', 'newuser@ex.ex',
'ROLE_USER', 'src\\test\\resources\\static\\images\\user.png', 0);

insert into user(id, name, surname, username, password, email, role, image_path, is_enabled)
values ('faeec839-9df4-4e58-83f7-048783a70524', 'new user 2', 'new user 2', 'new user 2', 'password2', 'newuser2@ex.ex',
'ROLE_USER', 'src\\test\\resources\\static\\images\\user.png', 0);

insert into confirmation_token(id, token, created_date, user_id)
values('faeec839-9df4-4e58-83f7-048783a705278', 'token', '2050-12-12', 'faeec839-9df4-4e58-83f7-048783a70523');

insert into confirmation_token(id, token, created_date, user_id)
values('faeec839-9df4-4e58-83f7-048783a705277', 'expired token', '2018-12-12', 'faeec839-9df4-4e58-83f7-048783a70524');

--for test case where user does not exist
insert into confirmation_token(id, token, created_date, user_id)
values('faeec839-9df4-4e58-83f7-048783a705275', 'invalid token', '2050-12-12', null);